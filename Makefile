.PHONY: debug tests clean release configure_debug configure_release vim
ROOT_DIR:=$(shell pwd)

all: debug
	./build/OpenBirch

release: configure_release
	cmake -B build -DCMAKE_BUILD_TYPE=Release
	cmake --build build -j 12 -- OpenBirch

debug: configure_debug
	cmake --build build -j 12 -- OpenBirch
	ln -sf '$(ROOT_DIR)/build/compile_commands.json' '$(ROOT_DIR)/compile_commands.json'

tests:
	cmake --build build -j 12 -- tests
	ln -sf '$(ROOT_DIR)/build/compile_commands.json' '$(ROOT_DIR)/compile_commands.json'
	./build/tests/tests --skip-benchmarks

bench:
	cmake --build build -j 12 -- tests
	ln -sf '$(ROOT_DIR)/build/compile_commands.json' '$(ROOT_DIR)/compile_commands.json'
	./build/tests/tests

web: /tmp/emsdk lib/emscripten/fmt lib/emscripten/gmp-6.3.0
	rm -rf build/*
	cd /tmp/emsdk; source ./emsdk_env.sh; cd -;\
	cmake -DCMAKE_TOOLCHAIN_FILE=../Emscripten.cmake -B build;
	cp -r std/ build/
	cd /tmp/emsdk; source ./emsdk_env.sh; cd -;\
	rm -rf build/CMakeFiles/OpenBirch.dir/emsdk/;\
	cmake --build build -j 12 -- OpenBirch;

/tmp/emsdk:
	mkdir -p lib/emscripten
	cd /tmp;\
	git clone https://github.com/emscripten-core/emsdk.git;\
	cd emsdk;\
	./emsdk install latest;\
	./emsdk activate latest;

lib/emscripten/fmt:
	cd /tmp/emsdk; source ./emsdk_env.sh; cd -;\
	cd lib/emscripten/;\
	git clone https://github.com/fmtlib/fmt.git;\
	cd fmt;\
	mkdir build;\
	cd build;\
	emcmake cmake ..;\
	emmake make;

lib/emscripten/gmp-6.3.0:
	cd /tmp/emsdk; source ./emsdk_env.sh; cd -;\
	cd lib/emscripten/;\
	wget https://gmplib.org/download/gmp/gmp-6.3.0.tar.xz;\
	tar xf gmp-6.3.0.tar.xz;\
	cd gmp-6.3.0;\
	emconfigure ./configure --disable-shared --enable-static --build=x86_64-linux-gnu --enable-cxx --disable-assembly --host none;\
	emmake make;

# debug:
# 	mkdir -p build/ && \
# 	cd build/ && \
# 	cmake -DCMAKE_BUILD_TYPE=Debug -S .. -B . -G Ninja && \
# 	ln -sf '$(ROOT_DIR)/build/compile_commands.json' '$(ROOT_DIR)/compile_commands.json' && \
# 	make OpenBirch -j 12
#
# tests:
# 	mkdir -p build/ && \
# 	cd build/ && \
# 	cmake -DCMAKE_BUILD_TYPE=Debug -S .. -B . -G Ninja && \
# 	ln -sf '$(ROOT_DIR)/build/compile_commands.json' '$(ROOT_DIR)/compile_commands.json' && \
# 	make tests -j 12
# 	./build/tests/tests

docs: clean_docs git-update
	doxygen Doxyfile

clean_docs:
	rm -rf docs/

git-update:
	git submodule sync
	git submodule update --init --recursive

clean:
	# Clean old files
	rm -rf build/
	rm -rf docs/

configure_debug:
	# Create build directory
	mkdir -p build
	# Set up cmake build files
	cmake -B build -S . -G Ninja -DCMAKE_BUILD_TYPE=Debug

configure_release:
	# Create build directory
	mkdir -p build
	# Set up cmake build files
	cmake -B build -S . -G Ninja -DCMAKE_BUILD_TYPE=Release

gdb: debug
	gdb -ex run build/OpenBirch

vim:
	mkdir -p ~/.config/nvim/syntax
	cp obd.vim ~/.config/nvim/syntax
	# Auto select syntax based on extension
	mkdir -p ~/.config/nvim/ftdetect/
	echo "au BufRead,BufNewFile *.obd set filetype=obd" > ~/.config/nvim/ftdetect/obd.vim

valgrind: 
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes ./build/OpenBirch 
