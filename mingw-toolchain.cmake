# Specify the target system
set(CMAKE_SYSTEM_NAME Windows)

# Specify the cross compiler
set(CMAKE_C_COMPILER x86_64-w64-mingw32-gcc)
set(CMAKE_CXX_COMPILER x86_64-w64-mingw32-g++)

# Specify the target environment
set(CMAKE_FIND_ROOT_PATH /usr/x86_64-w64-mingw32)

# Adjust the include and link directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# Specify paths for GMP
set(GMP_INCLUDE_DIR /usr/x86_64-w64-mingw32/include)
set(GMP_LIBRARY /usr/x86_64-w64-mingw32/lib/libgmp.a)
