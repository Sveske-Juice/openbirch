#ifndef TESTS_OPTS_H
#define TESTS_OPTS_H

// Enable or disable compilation of different tests
#define TEST_LEXER 1
#define TEST_PARSER 1
#define TEST_INTERPRETER 1
#define TEST_REWRITER 1

#define TEST_DIFF 0

#endif
