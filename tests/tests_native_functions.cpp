#include "lexer/token.h"
#include "parser/expression.h"
#include "runtime/abstract_function.h"
#include "runtime/native_functions.h"

#include <catch2/catch_test_macros.hpp>
#include <memory>
#include <vector>
#include <sstream>

namespace {
std::stringstream stream;

TEST_CASE("Native Functions") {
    // Redirect std::cout to custom stream
    std::cout.rdbuf(stream.rdbuf());

    SECTION("Print") {
        auto lambda = std::make_unique<LiteralExpression>(Token(IDENTIFIER, 0, 5, "print"), "print");
        auto param = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 1, "2"), LiteralValue(2));
        std::vector<std::unique_ptr<Expression>> params;
        params.push_back(std::move(param));
        auto callExpr = std::make_unique<CallExpression>(std::move(lambda), std::move(params));

        FunctionReturn res;
        Interpreter interpreter;
        CallContext callCtx(*callExpr->obtainLambdaName(), interpreter, callExpr->obtainParameters());
        REQUIRE_NOTHROW(res = __native_print(callCtx));
        REQUIRE(res.rc == SUCCESS);
        REQUIRE(res.expr == nullptr);
        REQUIRE(stream.str() == "2\n");
    }

    SECTION("One argument") {
        auto lambda = std::make_unique<LiteralExpression>(Token(IDENTIFIER, 0, 3, "sin"), "sin");
        auto param = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 1, "0"), LiteralValue(0));
        std::vector<std::unique_ptr<Expression>> params;
        params.push_back(std::move(param));
        auto callExpr = std::make_unique<CallExpression>(std::move(lambda), std::move(params));

        FunctionReturn res;
        Interpreter interpreter;
        CallContext callCtx(*callExpr->obtainLambdaName(), interpreter, callExpr->obtainParameters());
        REQUIRE_NOTHROW(res = __native_sin(callCtx));
        REQUIRE(res.rc == SUCCESS);
        REQUIRE(res.expr);
        REQUIRE(res.expr->expressionType() == EXPR_NUMBER);
        REQUIRE(static_cast<LiteralExpression *>(res.expr)->value().number_value == 0);
    }

    SECTION("Two arguments") {
        auto lambda = std::make_unique<LiteralExpression>(Token(IDENTIFIER, 0, 5, "nroot"), "nroot");
        auto param1 = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 1, "27"), LiteralValue(27));
        auto param2 = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 1, "3"), LiteralValue(3));
        std::vector<std::unique_ptr<Expression>> params;
        params.push_back(std::move(param1));
        params.push_back(std::move(param2));
        auto callExpr = std::make_unique<CallExpression>(std::move(lambda), std::move(params));

        FunctionReturn res;
        Interpreter interpreter;
        CallContext callCtx(*callExpr->obtainLambdaName(), interpreter, callExpr->obtainParameters());
        REQUIRE_NOTHROW(res = __native_nroot(callCtx));
        REQUIRE(res.rc == SUCCESS);
        REQUIRE(res.expr);
        REQUIRE(res.expr->expressionType() == EXPR_NUMBER);
        REQUIRE(static_cast<LiteralExpression *>(res.expr)->value().number_value == 3);
    }


    SECTION("Call arity error") {
        auto lambda = std::make_unique<LiteralExpression>(Token(IDENTIFIER, 0, 5, "nroot"), "nroot");
        auto param1 = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 1, "27"), LiteralValue(27));
        auto param2 = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 1, "3"), LiteralValue(3));
        std::vector<std::unique_ptr<Expression>> params;
        params.push_back(std::move(param1));
        params.push_back(std::move(param2));
        auto callExpr = std::make_unique<CallExpression>(std::move(lambda), std::move(params));

        FunctionReturn res;
        Interpreter interpreter;
        CallContext callCtx(*callExpr->obtainLambdaName(), interpreter, callExpr->obtainParameters());
        REQUIRE_NOTHROW(res = __native_nroot(callCtx));
        REQUIRE(res.rc == SUCCESS);
        REQUIRE(res.expr);
        REQUIRE(res.expr->expressionType() == EXPR_NUMBER);
        REQUIRE(static_cast<LiteralExpression *>(res.expr)->value().number_value == 3);
    }
}
}
