#include "lexer/token.h"
#include "parser/expression.h"
#include <catch2/catch_test_macros.hpp>
#include <memory>

TEST_CASE("Empty vec") {
    VectorExpression vExpr{Token(LSQUARE, 0, 1, "["), {}};

    REQUIRE(vExpr.size() == 0);
}

TEST_CASE("One value") {
    auto vec = std::vector<std::unique_ptr<Expression>>();
    auto val = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 0, ""), LiteralValue{8});
    vec.push_back(val->clone());
    VectorExpression vExpr{Token(LSQUARE, 0, 1, "["), std::move(vec)};

    REQUIRE(vExpr.size() == 1);
    REQUIRE(val->equal(vExpr.get_const(0)));
}

TEST_CASE("Multiple values") {
    auto vec = std::vector<std::unique_ptr<Expression>>();
    for (int i = 0; i < 69; i++) {
        auto val = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 0, ""), LiteralValue(i));
        vec.push_back(std::move(val));
    }
    VectorExpression vExpr{Token(LSQUARE, 0, 1, "["), std::move(vec)};

    REQUIRE(vExpr.size() == 69);
}


TEST_CASE("Vector set") {
    auto vec = std::vector<std::unique_ptr<Expression>>();
    auto val = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 0, ""), LiteralValue{8});
    vec.push_back(val->clone());
    VectorExpression vExpr{Token(LSQUARE, 0, 1, "["), std::move(vec)};

    auto newVal = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 0, ""), LiteralValue{69});
    vExpr.set(0, newVal->clone());

    REQUIRE(vExpr.size() == 1);
    REQUIRE(newVal->equal(vExpr.get_const(0)));
}

TEST_CASE("Vector clone") {
    auto vec = std::vector<std::unique_ptr<Expression>>();
    for (int i = 0; i < 69; i++) {
        auto val = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 0, ""), LiteralValue(i));
        vec.push_back(std::move(val));
    }
    VectorExpression vExpr{Token(LSQUARE, 0, 1, "["), std::move(vec)};

    auto tmp = vExpr.clone();
    auto vExpr2 = reinterpret_cast<VectorExpression*>(tmp.get());

    REQUIRE(vExpr.size() == 69);
    REQUIRE(vExpr2->size() == 69);
    REQUIRE(vExpr.equal(*vExpr2));
}
