#include "lexer/token.h"
#include "parser/expression.h"
#include <catch2/catch_test_macros.hpp>
#include <memory>

TEST_CASE("Ast Printer Test", "[AST Printer]") {
    SECTION("Literals") {
        LiteralExpression l(Token(NUMBER, 0, 0, ""), LiteralValue{69});
        CHECK(l.value().number_value == 69);
    }

    SECTION("Tree Structure Verification") {
        std::unique_ptr<Expression> l1 = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 0, ""), LiteralValue{69});
        std::unique_ptr<Expression> l2 = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 0, ""), LiteralValue{55});
        std::unique_ptr<Expression> l3 = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 0, ""), LiteralValue{2});
        std::unique_ptr<Expression> opInner = std::make_unique<BinaryExpression>(std::move(l2), Token(TokenType::STAR, 0, 1, "*"), std::move(l3));
        BinaryExpression op(std::move(l1), Token(TokenType::PLUS, 0, 1, "+"), std::move(opInner));

        CHECK(op.op().type == PLUS);
        REQUIRE(op.left().expressionType() == EXPR_NUMBER);
        CHECK(static_cast<LiteralExpression*>(&op.left())->value().number_value == 69);
        REQUIRE(op.right().expressionType() == EXPR_MULTIPLICATION);
        CHECK(static_cast<BinaryExpression*>(&op.right())->op().type == STAR);
        CHECK(static_cast<LiteralExpression*>(&static_cast<BinaryExpression*>(&op.right())->left())->value().number_value == 55);
        CHECK(static_cast<LiteralExpression*>(&static_cast<BinaryExpression*>(&op.right())->right())->value().number_value == 2);
    }
}
