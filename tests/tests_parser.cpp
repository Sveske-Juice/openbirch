#include "tests_opts.h"
#include "lexer/token.h"
#include "parser/ast_printer.h"
#include "parser/expression.h"
#include "parser/parser.h"
#include "parser/parser_errors.h"
#include "parser/statement.h"

#include <catch2/catch_test_macros.hpp>
#include <memory>

#if TEST_PARSER

TEST_CASE("Parser Literals", "[Parser:Literals]") {
    // TODO: should not rely on the printer to verify the results.

    SECTION("numbers") {
        AstPrinter printer;
        std::vector<Token> lexemes = {
            Token(TokenType::NUMBER, 0, 1, "69"),
            Token(TokenType::SEMICOLON, 0, 1, ";"),
            Token(TokenType::END_OF_FILE, 0, 1, "\0"),
        };
        Parser parser{lexemes};
        auto statement = std::move(parser.parse()[0]);
        Expression& expression = dynamic_cast<ExpressionStatement *>(statement.get())->expression();
        std::string out = printer.print(expression);

        CHECK(out == "69");
    }

    SECTION("strings") {
        AstPrinter printer;
        std::string str = "MY PERFECT STRING";
        std::vector<Token> lexemes = {
            Token(TokenType::STRING, 0, 1, str),
            Token(TokenType::SEMICOLON, 0, 1, ";"),
            Token(TokenType::END_OF_FILE, 0, 1, "\0"),
        };
        Parser parser{lexemes};
        auto statement = std::move(parser.parse()[0]);
        Expression& expression = dynamic_cast<ExpressionStatement *>(statement.get())->expression();
        std::string out = printer.print(expression);

        CHECK(out == str);
    }
}

TEST_CASE("Parser Unary", "[Parser:Unary]") {

    SECTION("Single Unary 1") {
        AstPrinter printer;
        std::vector<Token> lexemes = {
            Token(TokenType::MINUS, 0, 1, "-"),
            Token(TokenType::NUMBER, 0, 1, "69"),
            Token(TokenType::SEMICOLON, 0, 1, ";"),
            Token(TokenType::END_OF_FILE, 0, 1, "\0"),
        };
        Parser parser{lexemes};
        auto statement = std::move(parser.parse()[0]);
        Expression& expression = dynamic_cast<ExpressionStatement *>(statement.get())->expression();
        std::string out = printer.print(expression);

        CHECK(out == "-69");
    }

    SECTION("Single Unary 2") {
        AstPrinter printer;
        std::vector<Token> lexemes = {
            Token(TokenType::NOT, 0, 3, "not"),
            Token(TokenType::TRUE, 0, 4, "true"),
            Token(TokenType::SEMICOLON, 0, 1, ";"),
            Token(TokenType::END_OF_FILE, 0, 1, "\0"),
        };
        Parser parser{lexemes};
        auto statement = std::move(parser.parse()[0]);
        Expression& expression = dynamic_cast<ExpressionStatement *>(statement.get())->expression();
        std::string out = printer.print(expression);

        CHECK(out == "not true");
    }

    SECTION("Multiple Unary") {
        for (int i = 1; i < 69; i++) {
            AstPrinter printer;
            std::vector<Token> lexemes;
            for (int j = 0; j < i; j++) {
                lexemes.push_back(Token(TokenType::NOT, 0, 3, "not"));
            }
            lexemes.push_back(Token(TokenType::TRUE, 0, 1, "true"));
            lexemes.push_back(Token(TokenType::SEMICOLON, 0, 1, ";"));
            lexemes.push_back(Token(TokenType::END_OF_FILE, 0, 1, "\0"));

            Parser parser{lexemes};
            auto statement = std::move(parser.parse()[0]);
            Expression& expression = dynamic_cast<ExpressionStatement *>(statement.get())->expression();
            std::string out = printer.print(expression);

            std::string correct;
            for (int j = 0; j < i; j++) {
                correct += "not ";
            }
            correct += "true";

            CHECK(out == correct);
        }
        for (int i = 1; i < 69; i++) {
            AstPrinter printer;
            std::vector<Token> lexemes;
            for (int j = 0; j < i; j++) {
                lexemes.push_back(Token(TokenType::MINUS, 0, 1, "-"));
            }
            lexemes.push_back(Token(TokenType::NUMBER, 0, 1, "69"));
            lexemes.push_back(Token(TokenType::SEMICOLON, 0, 1, ";"));
            lexemes.push_back(Token(TokenType::END_OF_FILE, 0, 1, "\0"));

            Parser parser{lexemes};
            auto statement = std::move(parser.parse()[0]);
            Expression& expression = dynamic_cast<ExpressionStatement *>(statement.get())->expression();
            std::string out = printer.print(expression);

            std::string correct;
            for (int j = 0; j < i; j++) {
                correct += '-';
            }
            correct += "69";

            CHECK(out == correct);
        }
    }
}

TEST_CASE("Parser Binary", "[Parser:Binary]") {
    SECTION("Plus") {
        AstPrinter printer;
        std::vector<Token> lexemes = {
            Token(TokenType::NUMBER, 0, 1, "69"),
            Token(TokenType::PLUS, 0, 1, "+"),
            Token(TokenType::NUMBER, 0, 1, "420"),
            Token(TokenType::SEMICOLON, 0, 1, ";"),
            Token(TokenType::END_OF_FILE, 0, 1, "\0"),
        };
        Parser parser{lexemes};
        auto statement = std::move(parser.parse()[0]);
        Expression& expression = dynamic_cast<ExpressionStatement *>(statement.get())->expression();
        std::string out = printer.print(expression);

        CHECK(out == "(69+420)");
    }
    SECTION("Minus") {
        AstPrinter printer;
        std::vector<Token> lexemes = {
            Token(TokenType::NUMBER, 0, 1, "69"),
            Token(TokenType::MINUS, 0, 1, "-"),
            Token(TokenType::NUMBER, 0, 1, "420"),
            Token(TokenType::SEMICOLON, 0, 1, ";"),
            Token(TokenType::END_OF_FILE, 0, 1, "\0"),
        };
        Parser parser{lexemes};
        auto statement = std::move(parser.parse()[0]);
        Expression& expression = dynamic_cast<ExpressionStatement *>(statement.get())->expression();
        std::string out = printer.print(expression);

        CHECK(out == "(69-420)");
    }
    SECTION("Mult") {
        AstPrinter printer;
        std::vector<Token> lexemes = {
            Token(TokenType::NUMBER, 0, 1, "69"),
            Token(TokenType::STAR, 0, 1, "*"),
            Token(TokenType::NUMBER, 0, 1, "420"),
            Token(TokenType::SEMICOLON, 0, 1, ";"),
            Token(TokenType::END_OF_FILE, 0, 1, "\0"),
        };
        Parser parser{lexemes};
        auto statement = std::move(parser.parse()[0]);
        Expression& expression = dynamic_cast<ExpressionStatement *>(statement.get())->expression();
        std::string out = printer.print(expression);

        CHECK(out == "(69*420)");
    }
    SECTION("Divide") {
        AstPrinter printer;
        std::vector<Token> lexemes = {
            Token(TokenType::NUMBER, 0, 1, "69"),
            Token(TokenType::SLASH, 0, 1, "/"),
            Token(TokenType::NUMBER, 0, 1, "420"),
            Token(TokenType::SEMICOLON, 0, 1, ";"),
            Token(TokenType::END_OF_FILE, 0, 1, "\0"),
        };
        Parser parser{lexemes};
        auto statement = std::move(parser.parse()[0]);
        Expression& expression = dynamic_cast<ExpressionStatement *>(statement.get())->expression();
        std::string out = printer.print(expression);

        CHECK(out == "(69/420)");
    }
}

TEST_CASE("Parser Exceptions", "[Parser:Exceptions]") {
    SECTION("Unexpected Token") {
        std::vector<Token> lexemes = {
            Token(TokenType::PLUS, 0, 1, "+"),
            Token(TokenType::SEMICOLON, 0, 1, ";"),
            Token(TokenType::END_OF_FILE, 0, 1, "\0"),
        };
        Parser parser{lexemes};
        REQUIRE_THROWS_AS(parser.parse(), UnexpectedToken);
    }
    SECTION("Missing Closing Bracket") {
        std::vector<Token> lexemes = {
            Token(TokenType::LPAREN, 0, 1, "("),
            Token(TokenType::SEMICOLON, 0, 1, ";"),
            Token(TokenType::END_OF_FILE, 0, 1, "\0"),
        };
        Parser parser{lexemes};

        // For some reason if i use CHECK_THROWS_AS here catch2 throws an std::exception
        // maybe this is a bug with catch2?
        try {
            parser.parse();
            FAIL();
        } catch(const ParserException& e){
            SUCCEED();
        }
    }
}

TEST_CASE("Map Expressions") {
    SECTION("Empty map") {
        std::vector<Token> lexemes = {
            Token(LCURLY, 0, 1, "{"),
            Token(RCURLY, 0, 1, "}"),
            Token(TokenType::END_OF_FILE, 0, 1, "\0"),
        };
        Parser parser{lexemes};
        std::unique_ptr<Statement> statement;
        REQUIRE_NOTHROW(statement = std::move(parser.parse()[0]));
        Expression& expression = dynamic_cast<ExpressionStatement *>(statement.get())->expression();
        REQUIRE(expression.expressionType() == EXPR_MAP);
    }

    SECTION("Single pair map") {
        std::vector<Token> lexemes = {
            Token(LCURLY, 0, 1, "{"),
            Token(STRING, 0, 3, "sin"),
            Token(COLON_EQUAL, 0, 2, ":="),
            Token(STRING, 0, 3, "cos"),
            Token(RCURLY, 0, 1, "}"),
            Token(TokenType::END_OF_FILE, 0, 1, "\0"),
        };
        Parser parser{lexemes};
        std::unique_ptr<Statement> statement;
        REQUIRE_NOTHROW(statement = std::move(parser.parse()[0]));
        Expression& expression = dynamic_cast<ExpressionStatement *>(statement.get())->expression();
        REQUIRE(expression.expressionType() == EXPR_MAP);

        auto &map = static_cast<const MapExpression &>(expression);
        REQUIRE(map.mapReference().size() == 1);
        const auto &key = *map.mapReference().begin()->first;
        const auto &value = *map.mapReference().begin()->second;
        auto expectedKey = std::make_unique<LiteralExpression>(Token(STRING, 0, 0, ""), "sin");
        auto expectedValue = std::make_unique<LiteralExpression>(Token(STRING, 0, 0, ""), "cos");
        REQUIRE(key.equal(*expectedKey));
        REQUIRE(value.equal(*expectedValue));
    }

    SECTION("Multi pair map") {
        // sin -> cos
        // cos -> -sin
        // 69 -> 420 (just for testing)
        HashMap expectedMap;
        expectedMap[std::make_unique<LiteralExpression>(Token(STRING,0,0,""),"sin")] = std::make_unique<LiteralExpression>(Token(STRING,0,0,""),"cos");
        auto minusSin = std::make_unique<UnaryExpression>(Token(MINUS,0,1,"-"), std::make_unique<LiteralExpression>(Token(STRING,0,0,""),"sin"));
        expectedMap[std::make_unique<LiteralExpression>(Token(STRING,0,0,""),"cos")] = minusSin->clone();
        expectedMap[std::make_unique<LiteralExpression>(Token(NUMBER,0,0,""),LiteralValue{.number_value=69})] = std::make_unique<LiteralExpression>(Token(NUMBER,0,0,""),LiteralValue{.number_value=420});
        AstPrinter printer;

        auto expectedMapExpr = std::make_unique<MapExpression>(Token(LCURLY,0,0,""), std::move(expectedMap));

        std::vector<Token> lexemes = {
            Token(LCURLY, 0, 1, "{"),
            Token(STRING, 0, 3, "sin"),
            Token(COLON_EQUAL, 0, 2, ":="),
            Token(STRING, 0, 3, "cos"),
            Token(COMMA, 0, 1, ","),

            Token(STRING, 0, 3, "cos"),
            Token(COLON_EQUAL, 0, 2, ":="),
            Token(MINUS, 0, 1, "-"),
            Token(STRING, 0, 3, "sin"),
            Token(COMMA, 0, 1, ","),

            Token(NUMBER, 0, 3, "69"),
            Token(COLON_EQUAL, 0, 2, ":="),
            Token(NUMBER, 0, 3, "420"),

            Token(RCURLY, 0, 1, "}"),
            Token(TokenType::END_OF_FILE, 0, 1, "\0"),
        };

        Parser parser{lexemes};
        std::unique_ptr<Statement> statement;
        REQUIRE_NOTHROW(statement = std::move(parser.parse()[0]));
        Expression& expression = dynamic_cast<ExpressionStatement *>(statement.get())->expression();
        REQUIRE(expression.expressionType() == EXPR_MAP);

        auto &map = static_cast<const MapExpression &>(expression);
        REQUIRE(map.mapReference().size() == expectedMapExpr->mapReference().size());

        REQUIRE(expectedMapExpr->equal(map));
    }
}


#endif
