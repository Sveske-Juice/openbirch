#include "catch2/benchmark/catch_benchmark.hpp"
#include "lexer/lexer.h"
#include "parser/parser.h"
#include "runtime/interpreter.h"

#include <catch2/catch_test_macros.hpp>
#include <fstream>

TEST_CASE("Recursive Fibonacci") {
    std::fstream ifs("examples/fibonacci.obd");
    std::string content((std::istreambuf_iterator<char>(ifs)),
                        (std::istreambuf_iterator<char>()));

    Lexer lexer{content};
    Parser parser{lexer.tokenize()};
    Interpreter interpreter{parser.parse()};

    BENCHMARK_ADVANCED("Fib(30)")(Catch::Benchmark::Chronometer meter) {
        meter.measure([&interpreter] { return interpreter.interpret(); });
    };
}
