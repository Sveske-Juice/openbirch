#include "tests_opts.h"
#include "lexer/token.h"
#include "parser/expression.h"
#include "parser/statement.h"
#include "runtime/interpreter.h"

#include <catch2/catch_test_macros.hpp>
#include <memory>

#if TEST_INTERPRETER

TEST_CASE("Intepreter Literal tests", "[Intepreter:Literal]") {
    // Primitive
    SECTION("True") {
        LiteralValue value;
        value.boolean_value = true;
        std::unique_ptr<Expression> l =
            std::make_unique<LiteralExpression>(Token(TokenType::TRUE, 0, 0, ""), value);
        std::unique_ptr<Statement> s =
            std::make_unique<ExpressionStatement>(std::move(l));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_BOOLEAN);
        CHECK(res.value()->toString() == "true");
    }
    SECTION("False") {
        LiteralValue value;
        value.boolean_value = false;
        std::unique_ptr<Expression> l =
            std::make_unique<LiteralExpression>(Token(TokenType::FALSE, 0, 0, ""), value);
        std::unique_ptr<Statement> s =
            std::make_unique<ExpressionStatement>(std::move(l));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_BOOLEAN);
        CHECK(res.value()->toString() == "false");
    }
    SECTION("Number") {
        LiteralValue value;
        value.number_value = 69;
        std::unique_ptr<Expression> l =
            std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), value);
        std::unique_ptr<Statement> s =
            std::make_unique<ExpressionStatement>(std::move(l));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_NUMBER);
        CHECK(std::stod(res.value()->toString()) == 69);
    }

    // Object literals
    SECTION("Strings") {
        std::unique_ptr<Expression> l =
            std::make_unique<LiteralExpression>(Token(TokenType::STRING, 0, 0, ""), "HEHEHEAW");
        std::unique_ptr<Statement> s =
            std::make_unique<ExpressionStatement>(std::move(l));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_STRING);
        CHECK(res.value()->toString() == "HEHEHEAW");
    }
}

TEST_CASE("Interpreter Unary Operations", "[Interpreter:Unary]") {
    SECTION("Negate") {
        LiteralValue value;
        value.number_value = 69;
        std::unique_ptr<LiteralExpression> l =
            std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), value);
        std::unique_ptr<Expression> u = std::make_unique<UnaryExpression>(
            Token(TokenType::MINUS, 0, 1, "-"), std::move(l));

        std::unique_ptr<Statement> s =
            std::make_unique<ExpressionStatement>(std::move(u));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_NUMBER);
        CHECK(reinterpret_cast<LiteralExpression*>(res.value().get())->value().number_value == -69.0);
    }

    SECTION("Chained negates") {
        for (int i = 1; i < 69; i++) {
            LiteralValue value;
            value.number_value = 69;
            auto l =
                std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), value);
            auto uFinal =
                std::make_unique<UnaryExpression>(
                    Token(TokenType::MINUS, 0, 1, "-"), std::move(l));
            for (int j = 0; j < i; j++) {
                uFinal = std::make_unique<UnaryExpression>(
                    Token(TokenType::MINUS, 0, 1, "-"), std::move(uFinal));
            }
            std::unique_ptr<Statement> s =
                std::make_unique<ExpressionStatement>(std::move(uFinal));
            Interpreter interpreter(std::move(s));
            auto res = interpreter.interpret();
            double correct = i % 2 == 0 ? -69.0 : 69.0;
            REQUIRE(res.value()->expressionType() == EXPR_NUMBER);
            CHECK(reinterpret_cast<LiteralExpression*>(res.value().get())->value().number_value == correct);
        }
    }

    SECTION("NOT1") {
        LiteralValue value;
        value.boolean_value = true;
        std::unique_ptr<LiteralExpression> l =
            std::make_unique<LiteralExpression>(Token(TokenType::TRUE, 0, 0, ""), value);
        std::unique_ptr<Expression> u = std::make_unique<UnaryExpression>(
            Token(TokenType::NOT, 0, 3, "not"), std::move(l));

        std::unique_ptr<Statement> s =
            std::make_unique<ExpressionStatement>(std::move(u));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_BOOLEAN);
        CHECK(reinterpret_cast<LiteralExpression*>(res.value().get())->value().boolean_value == false);
    }

    SECTION("NOT2") {
        LiteralValue value;
        value.boolean_value = false;
        std::unique_ptr<LiteralExpression> l =
            std::make_unique<LiteralExpression>(Token(TokenType::FALSE, 0, 0, ""), value);
        std::unique_ptr<Expression> u = std::make_unique<UnaryExpression>(
            Token(TokenType::NOT, 0, 3, "not"), std::move(l));

        std::unique_ptr<Statement> s =
            std::make_unique<ExpressionStatement>(std::move(u));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_BOOLEAN);
        CHECK(reinterpret_cast<LiteralExpression*>(res.value().get())->value().boolean_value == true);
    }

    SECTION("Chained NUTS") {
        for (int i = 1; i < 69; i++) {
            LiteralValue value;
            value.boolean_value = true;
            std::unique_ptr<LiteralExpression> l =
                std::make_unique<LiteralExpression>(Token(TokenType::TRUE, 0, 0, ""), value);
            std::unique_ptr<Expression> uFinal =
                std::make_unique<UnaryExpression>(
                    Token(TokenType::NOT, 0, 3, "not"), std::move(l));
            for (int j = 0; j < i; j++) {
                uFinal = std::make_unique<UnaryExpression>(
                    Token(TokenType::NOT, 0, 3, "not"), std::move(uFinal));
            }
            std::unique_ptr<Statement> s =
                std::make_unique<ExpressionStatement>(std::move(uFinal));
            Interpreter interpreter(std::move(s));
            auto res = interpreter.interpret();
            bool correct = i % 2 == 0 ? false : true;
            CHECK(res.value()->expressionType() == EXPR_BOOLEAN);
        CHECK(reinterpret_cast<LiteralExpression*>(res.value().get())->value().boolean_value == correct);
        }
    }
}

TEST_CASE("Interpreter Binary Operations", "[Interpreter:Binary]") {
    SECTION("Plus") {
        LiteralValue value;
        value.number_value = 69;
        std::unique_ptr<LiteralExpression> l =
            std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), value);
        value.number_value = 420;
        std::unique_ptr<LiteralExpression> r =
            std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), value);
        std::unique_ptr<Expression> bin = std::make_unique<BinaryExpression>(
            std::move(l), Token(TokenType::PLUS, 0, 1, "+"), std::move(r));

        std::unique_ptr<Statement> s =
            std::make_unique<ExpressionStatement>(std::move(bin));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        REQUIRE(res.value()->expressionType() == EXPR_NUMBER);
        CHECK(reinterpret_cast<LiteralExpression*>(res.value().get())->value().number_value == 489.0);
    }

    SECTION("Minus") {
        LiteralValue value;
        value.number_value = 69;
        std::unique_ptr<LiteralExpression> l =
            std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), value);
        value.number_value = 420;
        std::unique_ptr<LiteralExpression> r =
            std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), value);
        std::unique_ptr<Expression> bin = std::make_unique<BinaryExpression>(
            std::move(l), Token(TokenType::MINUS, 0, 1, "-"), std::move(r));

        std::unique_ptr<Statement> s =
            std::make_unique<ExpressionStatement>(std::move(bin));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        REQUIRE(res.value()->expressionType() == EXPR_NUMBER);
        CHECK(reinterpret_cast<LiteralExpression*>(res.value().get())->value().number_value == -351.0);
    }

    SECTION("Multiplication") {
        LiteralValue value;
        value.number_value = 69;
        std::unique_ptr<LiteralExpression> l =
            std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), value);
        value.number_value = 420;
        std::unique_ptr<LiteralExpression> r =
            std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), value);
        std::unique_ptr<Expression> bin = std::make_unique<BinaryExpression>(
            std::move(l), Token(TokenType::STAR, 0, 1, "*"), std::move(r));

        std::unique_ptr<Statement> s =
            std::make_unique<ExpressionStatement>(std::move(bin));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        REQUIRE(res.value()->expressionType() == EXPR_NUMBER);
        CHECK(reinterpret_cast<LiteralExpression*>(res.value().get())->value().number_value == 28980.0);
    }

    SECTION("Division") {
        LiteralValue value;
        value.number_value = 69;
        std::unique_ptr<LiteralExpression> l =
            std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), value);
        value.number_value = 420;
        std::unique_ptr<LiteralExpression> r =
            std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), value);
        std::unique_ptr<Expression> bin = std::make_unique<BinaryExpression>(
            std::move(l), Token(TokenType::SLASH, 0, 1, "/"), std::move(r));

        std::unique_ptr<Statement> s =
            std::make_unique<ExpressionStatement>(std::move(bin));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        REQUIRE(res.value()->expressionType() == EXPR_NUMBER);
    }

    SECTION("String Concat") {
        std::unique_ptr<LiteralExpression> l =
            std::make_unique<LiteralExpression>(Token(TokenType::STRING, 0, 0, ""), "Hello ");
        std::unique_ptr<LiteralExpression> r =
            std::make_unique<LiteralExpression>(Token(TokenType::STRING, 0, 0, ""), "World~");
        std::unique_ptr<Expression> bin = std::make_unique<BinaryExpression>(
            std::move(l), Token(TokenType::PLUS, 0, 1, "+"), std::move(r));

        std::unique_ptr<Statement> s =
            std::make_unique<ExpressionStatement>(std::move(bin));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_STRING);
        CHECK(res.value()->toString() == "Hello World~");
    }
}

TEST_CASE("Logical operators") {
    SECTION("AND(false,false)") {
        auto l = std::make_unique<LiteralExpression>(Token(FALSE, 0, 0, ""), LiteralValue{.boolean_value=false});
        auto r = std::make_unique<LiteralExpression>(Token(FALSE, 0, 0, ""), LiteralValue{.boolean_value=false});
        auto bin = std::make_unique<BinaryExpression>(
            std::move(l), Token(AND, 0, 0, ""), std::move(r));
        auto s = std::make_unique<ExpressionStatement>(std::move(bin));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_BOOLEAN);
        CHECK(static_cast<const LiteralExpression &>(*res.value()).value().boolean_value == false);
    }
    SECTION("AND(false,true)") {
        auto l = std::make_unique<LiteralExpression>(Token(FALSE, 0, 0, ""), LiteralValue{.boolean_value = false});
        auto r = std::make_unique<LiteralExpression>(Token(TRUE, 0, 0, ""), LiteralValue{.boolean_value = true});
        auto bin = std::make_unique<BinaryExpression>(
            std::move(l), Token(AND, 0, 0, ""), std::move(r));
        auto s = std::make_unique<ExpressionStatement>(std::move(bin));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_BOOLEAN);
        CHECK(static_cast<const LiteralExpression &>(*res.value()).value().boolean_value == false);
    }
    SECTION("AND(true,false)") {
        auto l = std::make_unique<LiteralExpression>(Token(TRUE, 0, 0, ""), LiteralValue{.boolean_value = true});
        auto r = std::make_unique<LiteralExpression>(Token(FALSE, 0, 0, ""), LiteralValue{.boolean_value = false});
        auto bin = std::make_unique<BinaryExpression>(
            std::move(l), Token(AND, 0, 0, ""), std::move(r));
        auto s = std::make_unique<ExpressionStatement>(std::move(bin));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_BOOLEAN);
        CHECK(static_cast<const LiteralExpression &>(*res.value()).value().boolean_value == false);
    }
    SECTION("AND(true,true)") {
        LiteralValue val;
        val.boolean_value = true;
        auto l = std::make_unique<LiteralExpression>(Token(TRUE, 0, 0, ""), LiteralValue{.boolean_value = true});
        auto r = std::make_unique<LiteralExpression>(Token(TRUE, 0, 0, ""), LiteralValue{.boolean_value = true});
        auto bin = std::make_unique<BinaryExpression>(
            std::move(l), Token(AND, 0, 0, ""), std::move(r));
        auto s = std::make_unique<ExpressionStatement>(std::move(bin));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_BOOLEAN);
        CHECK(static_cast<const LiteralExpression &>(*res.value()).value().boolean_value == true);
    }

    SECTION("OR(false,false)") {
        auto l = std::make_unique<LiteralExpression>(Token(FALSE, 0, 0, ""), LiteralValue{.boolean_value=false});
        auto r = std::make_unique<LiteralExpression>(Token(FALSE, 0, 0, ""), LiteralValue{.boolean_value=false});
        auto bin = std::make_unique<BinaryExpression>(
            std::move(l), Token(OR, 0, 0, ""), std::move(r));
        auto s = std::make_unique<ExpressionStatement>(std::move(bin));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_BOOLEAN);
        CHECK(static_cast<const LiteralExpression &>(*res.value()).value().boolean_value == false);
    }
    SECTION("OR(false,true)") {
        auto l = std::make_unique<LiteralExpression>(Token(FALSE, 0, 0, ""), LiteralValue{.boolean_value = false});
        auto r = std::make_unique<LiteralExpression>(Token(TRUE, 0, 0, ""), LiteralValue{.boolean_value = true});
        auto bin = std::make_unique<BinaryExpression>(
            std::move(l), Token(OR, 0, 0, ""), std::move(r));
        auto s = std::make_unique<ExpressionStatement>(std::move(bin));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_BOOLEAN);
        CHECK(static_cast<const LiteralExpression &>(*res.value()).value().boolean_value == true);
    }
    SECTION("OR(true,false)") {
        auto l = std::make_unique<LiteralExpression>(Token(TRUE, 0, 0, ""), LiteralValue{.boolean_value = true});
        auto r = std::make_unique<LiteralExpression>(Token(FALSE, 0, 0, ""), LiteralValue{.boolean_value = false});
        auto bin = std::make_unique<BinaryExpression>(
            std::move(l), Token(OR, 0, 0, ""), std::move(r));
        auto s = std::make_unique<ExpressionStatement>(std::move(bin));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_BOOLEAN);
        CHECK(static_cast<const LiteralExpression &>(*res.value()).value().boolean_value == true);
    }
    SECTION("OR(true,true)") {
        LiteralValue val;
        val.boolean_value = true;
        auto l = std::make_unique<LiteralExpression>(Token(TRUE, 0, 0, ""), LiteralValue{.boolean_value = true});
        auto r = std::make_unique<LiteralExpression>(Token(TRUE, 0, 0, ""), LiteralValue{.boolean_value = true});
        auto bin = std::make_unique<BinaryExpression>(
            std::move(l), Token(OR, 0, 0, ""), std::move(r));
        auto s = std::make_unique<ExpressionStatement>(std::move(bin));
        Interpreter interpreter(std::move(s));
        auto res = interpreter.interpret();
        CHECK(res.value()->expressionType() == EXPR_BOOLEAN);
        CHECK(static_cast<const LiteralExpression &>(*res.value()).value().boolean_value == true);
    }
}

TEST_CASE("Hashmap") {
    auto key = std::make_unique<LiteralExpression>(Token(STRING, 0,0,""), "sin");
    auto value = std::make_unique<LiteralExpression>(Token(STRING, 0,0,""), "cos");
    HashMap hashMap;
    hashMap[std::move(key)] = std::move(value);

    auto mapExpr = std::make_unique<MapExpression>(Token(TRUE,0,0,""), std::move(hashMap));
    std::unique_ptr<Statement> stmt = std::make_unique<ExpressionStatement>(std::move(mapExpr));
    Interpreter interpreter{std::move(stmt)};
    
    std::optional<std::unique_ptr<Expression>> res;
    REQUIRE_NOTHROW(res = interpreter.interpret());
}

#endif
