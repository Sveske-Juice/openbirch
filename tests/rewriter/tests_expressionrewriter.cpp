#include "tests_opts.h"
#include "lexer/lexer.h"
#include "lexer/token.h"
#include "parser/ast_printer.h"
#include "parser/expression.h"
#include "parser/parser.h"
#include "runtime/expressionrewriter.h"
#include "runtime/interpreter.h"
#include "runtime/lambda_function.h"

#include <catch2/catch_test_macros.hpp>
#include <memory>

#if TEST_REWRITER

#define MATCH_CONSTANT "proc constant(expr) if (type(expr)=NUMBER) return true; end return false; end"
#define MATCH_NOT_CONSTANT "proc notconstant(expr) if (type(expr)=NUMBER) return false; end return true; end"

#define MATCH_IDENTIFIER "proc identifier(expr) if (type(expr)=IDENTIFIER) return true; end return false; end"
#define MATCH_NOT_IDENTIFIER "proc notidentifier(expr) if (type(expr)=IDENTIFIER) return false; end return true; end"

#define MATCH_FUNCTION "proc function(expr) if (type(expr)=FUNCTION) return true; end return false; end"
#define MATCH_NOT_FUNCTION "proc notfunction(expr) if (type(expr)=FUNCTION) return false; end return true; end"

#define INTERPRET_PREDICATE(input) Lexer lexer{input}; Parser parser{lexer.tokenize()}; auto ast = parser.parse(); Interpreter interpreter(std::move(ast)); interpreter.interpret()

/* NODE REPLACEMENT */
TEST_CASE("Replace node in ast") {
    SECTION("Replace root node") {
        // Replace + in (a+0) with a
        auto a = std::make_unique<LiteralExpression>(Token(IDENTIFIER, 0, 0, ""), "a");
        auto s0 = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 0, ""), LiteralValue(0));
        auto splus = std::make_unique<BinaryExpression>(std::move(a), Token(PLUS, 0, 1, "+"), std::move(s0));

        auto replacement = std::make_unique<LiteralExpression>(Token(IDENTIFIER, 0, 0, ""), "a");
        auto res = ExpressionRewriter::replaceNodeInAst(*splus.get(), std::move(replacement));
        REQUIRE(res);
        REQUIRE(res->expressionType() == EXPR_IDENTIFIER);
        REQUIRE(res->toString() == "a");
    }
    SECTION("Replace leaf node") {
        // Replace a in (a+0) with 0 (not mathmatically correct, just a test)
        auto a = std::make_unique<LiteralExpression>(Token(IDENTIFIER, 0, 0, ""), "a");
        auto s0 = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 0, ""), LiteralValue(0));
        auto splus = std::make_unique<BinaryExpression>(std::move(a), Token(PLUS, 0, 1, "+"), std::move(s0));

        auto replacement = std::make_unique<LiteralExpression>(Token(NUMBER, 0, 0, ""), LiteralValue(0));
        auto res = ExpressionRewriter::replaceNodeInAst(splus->left(), std::move(replacement));

        // source should still be intact
        REQUIRE(splus);

        REQUIRE(!res); // Should not have returned anything, but instead edit src
        REQUIRE(splus->left().expressionType() == EXPR_NUMBER);
        REQUIRE(splus->left().toString() == "0");
    }
}

// TODO: dont rely on printer to verify results
TEST_CASE("Arithmetic axioms", "[ExpreRewriter:Peano]") {
    SECTION("Addition") {
        AstPrinter printer;
        // Pattern
        // A + 0
        auto pA = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
        auto p0 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});
        auto patternRhs = std::make_unique<BinaryExpression>(
            std::move(pA), Token(TokenType::PLUS, 0, 1, "+"), std::move(p0));

        // Pattern
        // 0 + A
        auto pA1 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
        auto p02 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});
        auto patternLhs= std::make_unique<BinaryExpression>(
            std::move(p02), Token(TokenType::PLUS, 0, 1, "+"), std::move(pA1));

        // Replacement
        // A
        auto replacement = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");

        // Concrete tests to test the rule/pattern
        SECTION("Constant plus 0 rhs") {
            // 5 + 0 -> 5
            auto s5 =
                std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{5});
            auto s0 =
                std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});
            auto src = std::make_unique<BinaryExpression>(
                std::move(s5), Token(TokenType::PLUS, 0, 1, "+"),
                std::move(s0));
            auto result = ExpressionRewriter::substitute(std::move(src), std::move(patternRhs), std::move(replacement));
            CHECK(printer.print(*result.rewrittenExpr.get()) == "5");
        }

        SECTION("Expression plus 0 rhs") {
            // (5+2)+0 -> (5+2)
            auto s5 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{5});
            auto s2 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{2});
            auto splus1 = std::make_unique<BinaryExpression>(std::move(s5), Token(PLUS,0,1,"+"), std::move(s2));
            auto s0 =
                std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});
            auto sPlus2 = std::make_unique<BinaryExpression>(
                std::move(splus1), Token(TokenType::PLUS, 0, 1, "+"),
                std::move(s0));
            auto result = ExpressionRewriter::substitute(std::move(sPlus2), std::move(patternRhs), std::move(replacement));
            CHECK(printer.print(*result.rewrittenExpr.get()) == "(5+2)");
        }

        SECTION("Expresion plus 0 lhs") {
            // 0+(5+2) -> (5+2)
            auto s5 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{5});
            auto s2 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{2});
            auto splus1 = std::make_unique<BinaryExpression>(std::move(s5), Token(PLUS,0,1,"+"), std::move(s2));
            auto s0 =
                std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});
            auto sPlus2 = std::make_unique<BinaryExpression>(
                std::move(s0), Token(TokenType::PLUS, 0, 1, "+"),
                std::move(splus1));
            auto result = ExpressionRewriter::substitute(std::move(sPlus2), std::move(patternLhs), std::move(replacement));
            CHECK(printer.print(*result.rewrittenExpr.get()) == "(5+2)");
        }

        SECTION("Zero in subexpression lhs") {
            // (0+2)+5 -> 2+5
            auto s5 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{5});
            auto s2 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{2});
            auto s0 =
                std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});
            auto splus1 = std::make_unique<BinaryExpression>(std::move(s0), Token(PLUS,0,1,"+"), std::move(s2));
            auto sPlus2 = std::make_unique<BinaryExpression>(
                std::move(splus1), Token(TokenType::PLUS, 0, 1, "+"),
                std::move(s5));
            auto result = ExpressionRewriter::substitute(std::move(sPlus2), std::move(patternLhs), std::move(replacement));
            CHECK(printer.print(*result.rewrittenExpr.get()) == "(2+5)");
        }

        SECTION("Zero in subexpression lhs 2") {
            // 5+(0+2)
            auto s5 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{5});
            auto s2 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{2});
            auto s0 =
                std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});
            auto splus1 = std::make_unique<BinaryExpression>(std::move(s0), Token(PLUS,0,1,"+"), std::move(s2));
            auto sPlus2 = std::make_unique<BinaryExpression>(
                std::move(s5), Token(TokenType::PLUS, 0, 1, "+"),
                std::move(splus1));
            auto result = ExpressionRewriter::substitute(std::move(sPlus2), std::move(patternLhs), std::move(replacement));
            CHECK(printer.print(*result.rewrittenExpr.get()) == "(5+2)");
        }
    }
    SECTION("Subtraction") {
        AstPrinter printer;
        // Pattern
        // A - 0
        auto pA = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
        auto p0 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});
        auto pattern = std::make_unique<BinaryExpression>(
            std::move(pA), Token(TokenType::MINUS, 0, 1, "-"), std::move(p0));

        // Replacement
        // A
        auto replacement = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");

        // Concrete tests to test the rule/pattern
        SECTION("Sub1") {
            // 5 - 0 -> 5
            auto s5 =
                std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{5});
            auto s0 =
                std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});
            auto src = std::make_unique<BinaryExpression>(
                std::move(s5), Token(TokenType::MINUS, 0, 1, "-"),
                std::move(s0));
            auto result = ExpressionRewriter::substitute(std::move(src), std::move(pattern), std::move(replacement));
            CHECK(printer.print(*result.rewrittenExpr.get()) == "5");
        }
    }
    SECTION("Multiplication") {
        AstPrinter printer;
        // Pattern
        // A * 0
        auto pA = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
        auto p0 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});
        auto pattern = std::make_unique<BinaryExpression>(
            std::move(pA), Token(TokenType::STAR, 0, 1, "*"), std::move(p0));

        // Replacement
        // 0
        auto replacement = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});

        // Concrete tests to test the rule/pattern
        SECTION("Mul1") {
            // 5 * 0 -> 0
            auto s5 =
                std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{5});
            auto s0 =
                std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});
            auto src = std::make_unique<BinaryExpression>(
                std::move(s5), Token(TokenType::STAR, 0, 1, "*"),
                std::move(s0));
            auto result = ExpressionRewriter::substitute(std::move(src), std::move(pattern), std::move(replacement));
            CHECK(printer.print(*result.rewrittenExpr.get()) == "0");
        }
    }
    SECTION("Division") {
        AstPrinter printer;
        // Pattern
        // 0 / A
        auto pA = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});
        auto p0 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
        auto pattern = std::make_unique<BinaryExpression>(
            std::move(pA), Token(TokenType::SLASH, 0, 1, "/"), std::move(p0));

        // Replacement
        // 0
        auto replacement = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});

        // Concrete tests to test the rule/pattern
        SECTION("Div1") {
            // 0 / 5 -> 0
            auto s5 =
                std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{5});
            auto s0 =
                std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});
            auto src = std::make_unique<BinaryExpression>(
                std::move(s0), Token(TokenType::SLASH, 0, 1, "/"),
                std::move(s5));
            auto result = ExpressionRewriter::substitute(std::move(src), std::move(pattern), std::move(replacement));
            CHECK(printer.print(*result.rewrittenExpr.get()) == "0");
        }
    }
}

TEST_CASE("Expression Rewriter variable replacements",
          "[ExprRewriter:VarReplace]") {
    // Pattern
    // A + B
    std::unique_ptr<LiteralExpression> pA =
        std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
    std::unique_ptr<LiteralExpression> pB =
        std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "b");
    std::unique_ptr<Expression> pattern = std::make_unique<BinaryExpression>(
        std::move(pA), Token(TokenType::PLUS, 0, 1, "+"), std::move(pB));

    // Replacement
    // B + A
    auto rA = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "b");
    auto rB = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
    auto replacement = std::make_unique<BinaryExpression>(
        std::move(rA), Token(TokenType::PLUS, 0, 1, "+"), std::move(rB));

    // Source
    // (4+6)/2
    auto s4 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue(4));
    auto s6 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue(6));
    auto plusOp = std::make_unique<BinaryExpression>(
        std::move(s4), Token(TokenType::PLUS, 0, 1, "+"), std::move(s6));

    auto s2 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue(2));
    auto source = std::make_unique<BinaryExpression>(
        std::move(plusOp), Token(TokenType::SLASH, 0, 1, "/"), std::move(s2));

    // Applied Source
    // B + A
    AstPrinter printer;
    auto applied = ExpressionRewriter::substitute(
        std::move(source), std::move(pattern), std::move(replacement));
    CHECK(printer.print(*applied.rewrittenExpr.get()) == "((6+4)/2)");
}

TEST_CASE("Square", "[ExprRewriter:Square]") {
    // Pattern
    //  ("A" + "B")*("A" + "B")
    auto a1 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
    auto b1 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "b");
    auto plus1 = std::make_unique<BinaryExpression>(std::move(a1), Token(PLUS, 0, 1, "+"), std::move(b1));

    auto a2 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
    auto b2 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "b");
    auto plus2 = std::make_unique<BinaryExpression>(std::move(a2), Token(PLUS, 0, 1, "+"), std::move(b2));

    auto mult = std::make_unique<BinaryExpression>(std::move(plus1), Token(STAR, 0, 1, "*"), std::move(plus2));

    // Replacement
    // "A"*"A" + "B"*"B" + 2*"A"*"B"
    auto ra1 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
    auto ra2 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
    auto rMultA = std::make_unique<BinaryExpression>(std::move(ra1), Token(STAR, 0, 1, "*"), std::move(ra2));

    auto rb1 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "b");
    auto rb2 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "b");
    auto rMultB = std::make_unique<BinaryExpression>(std::move(rb1), Token(STAR, 0, 1, "*"), std::move(rb2));
    auto rabPlus = std::make_unique<BinaryExpression>(std::move(rMultA), Token(PLUS, 0, 1, "+"), std::move(rMultB));

    auto r2 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{2});
    auto ra3 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
    auto rb3 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "b");

    auto rAB = std::make_unique<BinaryExpression>(std::move(ra3), Token(STAR, 0, 1, "*"), std::move(rb3));
    auto rMult = std::make_unique<BinaryExpression>(std::move(r2), Token(STAR, 0, 1, "*"), std::move(rAB));
    auto replacement = std::make_unique<BinaryExpression>(std::move(rMult), Token(PLUS, 0, 1, "+"), std::move(rabPlus));

    // Source
    // (69+420)*(69+420)
    auto l1 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{69});
    auto l2 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{420});
    auto sPlus1 = std::make_unique<BinaryExpression>(std::move(l1), Token(PLUS, 0, 1, "+"), std::move(l2));

    auto re1 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{69});
    auto re2 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{420});
    auto sPlus2 = std::make_unique<BinaryExpression>(std::move(re1), Token(PLUS, 0, 1, "+"), std::move(re2));

    auto sMult = std::make_unique<BinaryExpression>(std::move(sPlus1), Token(STAR, 0, 1, "*"), std::move(sPlus2));

    // Applied source
    // "A"*"A" + "B"*"B" + 2*"A"*"B"
    AstPrinter printer;
    auto applied = ExpressionRewriter::substitute(std::move(sMult), std::move(mult), std::move(replacement));
    CHECK(printer.print(*applied.rewrittenExpr.get()) == "((2*(69*420))+((69*69)+(420*420)))");
}

TEST_CASE("Match identifier") {
    AstPrinter printer;
    // Pattern
    // x+x
    auto x1 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "x");
    auto x2 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "x");
    auto xMult = std::make_unique<BinaryExpression>(std::move(x1), Token(TokenType::PLUS, 0, 1, "+"), std::move(x2));

    // Replacement
    // 2*x
    auto con2 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{2});
    auto x3 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "x");
    auto rMult = std::make_unique<BinaryExpression>(std::move(con2), Token(STAR, 0, 1, "*"), std::move(x3));
    SECTION("Identifiers should not match constant") {
        // Source
        // 5 + 5
        auto con51 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{5});
        auto con52 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{5});
        auto sPlus1 = std::make_unique<BinaryExpression>(std::move(con51), Token(PLUS, 0, 1, "+"), std::move(con52));

        INTERPRET_PREDICATE(MATCH_IDENTIFIER);
        std::unordered_map<std::string, std::string> matchMap = {
            {"x", "identifier"}
        };

        auto applied = ExpressionRewriter::substitute(std::move(sPlus1), std::move(xMult), std::move(rMult), matchMap, interpreter);
        CHECK(printer.print(*applied.rewrittenExpr.get()) == "(5+5)");
    }

    SECTION("Identifier should match identifier") {
        // Source
        // x + x
        auto sx1 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "x");
        auto sx2 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "x");
        auto sPlus1 = std::make_unique<BinaryExpression>(std::move(sx1), Token(PLUS, 0, 1, "+"), std::move(sx2));

        INTERPRET_PREDICATE(MATCH_IDENTIFIER);
        std::unordered_map<std::string, std::string> matchMap = {
            {"x", "identifier"}
        };

        auto applied = ExpressionRewriter::substitute(std::move(sPlus1), std::move(xMult), std::move(rMult), matchMap, interpreter);
        CHECK(printer.print(*applied.rewrittenExpr.get()) == "(2*x)");
    }

    SECTION("Identifiers should not match not identifier") {
        // Source
        // x + x
        auto sx1 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "x");
        auto sx2 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "x");
        auto sPlus1 = std::make_unique<BinaryExpression>(std::move(sx1), Token(PLUS, 0, 1, "+"), std::move(sx2));

        INTERPRET_PREDICATE(MATCH_NOT_IDENTIFIER);
        std::unordered_map<std::string, std::string> matchMap = {
            {"x", "notidentifier"}
        };

        auto applied = ExpressionRewriter::substitute(std::move(sPlus1), std::move(xMult), std::move(rMult), matchMap, interpreter);
        CHECK(printer.print(*applied.rewrittenExpr.get()) == "(x+x)");
    }

    SECTION("Function matching") {
        auto sf = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "f");
        auto r = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue(69));

        INTERPRET_PREDICATE(MATCH_FUNCTION);
        std::unordered_map<std::string, std::string> matchMap = {
            {"f", "function"}
        };

        auto res1 = ExpressionRewriter::substitute(sf->clone(), sf->clone(), r->clone(), matchMap, interpreter);
        CHECK(res1.rewrittenExpr->toString() == "f"); // Not rewritten

        // Define function f
        std::vector<std::string> names {"f"};
        interpreter.addDefinition("f", std::make_unique<LambdaFunction>(std::make_unique<LambdaExpression>(names, std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue(420)))), false);

        auto res2 = ExpressionRewriter::substitute(sf->clone(), sf->clone(), r->clone(), matchMap, interpreter);
        CHECK(res2.rewrittenExpr->toString() == "69"); // function is now defined so should be rewritten
    }
}

TEST_CASE("Match constant") {
    AstPrinter printer;
    // Pattern
    // x+x
    auto x1 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "x");
    auto x2 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "x");
    auto xMult = std::make_unique<BinaryExpression>(std::move(x1), Token(TokenType::PLUS, 0, 1, "+"), std::move(x2));

    // Replacement
    // 2*x
    auto con2 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{2});
    auto x3 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "x");
    auto rMult = std::make_unique<BinaryExpression>(std::move(con2), Token(STAR, 0, 1, "*"), std::move(x3));

    SECTION("Constant should match constants") {
        // Source
        // 5 + 5
        auto s1 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{5});
        auto s2 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{5});
        auto sPlus1 = std::make_unique<BinaryExpression>(std::move(s1), Token(PLUS, 0, 1, "+"), std::move(s2));

        INTERPRET_PREDICATE(MATCH_CONSTANT);
        MatchMap matchMap = {
            {"x", "constant"}
        };

        auto applied = ExpressionRewriter::substitute(std::move(sPlus1), std::move(xMult), std::move(rMult), matchMap, interpreter);
        CHECK(printer.print(*applied.rewrittenExpr.get()) == "(2*5)");
    }

    SECTION("Constant should not match variables") {
        // Source
        // x + x
        auto s1 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "x");
        auto s2 = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "x");
        auto sPlus1 = std::make_unique<BinaryExpression>(std::move(s1), Token(PLUS, 0, 1, "+"), std::move(s2));

        INTERPRET_PREDICATE(MATCH_CONSTANT);
        std::unordered_map<std::string, std::string> matchMap = {
            {"x", "constant"}
        };

        auto applied = ExpressionRewriter::substitute(std::move(sPlus1), std::move(xMult), std::move(rMult), matchMap, interpreter);
        CHECK(printer.print(*applied.rewrittenExpr.get()) == "(x+x)");
    }

    SECTION("Constant should not match not constant") {
        // Source
        // 5 + 5
        auto s1 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{5});
        auto s2 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{5});
        auto sPlus1 = std::make_unique<BinaryExpression>(std::move(s1), Token(PLUS, 0, 1, "+"), std::move(s2));

        INTERPRET_PREDICATE(MATCH_NOT_CONSTANT);
        std::unordered_map<std::string, std::string> matchMap = {
            {"x", "notconstant"}
        };

        auto applied = ExpressionRewriter::substitute(std::move(sPlus1), std::move(xMult), std::move(rMult), matchMap, interpreter);
        CHECK(printer.print(*applied.rewrittenExpr.get()) == "(5+5)");
    }
}

TEST_CASE("All constraint") {
    AstPrinter printer;

    SECTION("All constraint should match subexpressions") {
        // Pattern
        // 0 / a
        auto p0 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});
        auto pa = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
        auto pDiv = std::make_unique<BinaryExpression>(std::move(p0), Token(TokenType::SLASH, 0, 1, "/"), std::move(pa));

        // Replacement
        // 0
        auto r0 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});

        // Source
        // 0 / (69+420+24)
        auto s1 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{0});
        auto s2 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{69});
        auto s3 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{420});
        auto s4 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{24});
        auto sPlus = std::make_unique<BinaryExpression>(std::move(s2), Token(PLUS, 0, 1, "+"), std::move(s3));
        auto sPlus1 = std::make_unique<BinaryExpression>(std::move(sPlus), Token(PLUS, 0, 1, "+"), std::move(s4));
        auto sDiv = std::make_unique<BinaryExpression>(std::move(s1), Token(SLASH, 0, 1, "/"), std::move(sPlus1));

        // No matchmap = all identifiers are all match constrainted
        auto applied = ExpressionRewriter::substitute(std::move(sDiv), std::move(pDiv), std::move(r0), {});
        CHECK(printer.print(*applied.rewrittenExpr.get()) == "0");
    }

    SECTION("All constraints2") {
        // Pattern
        // a * 1
        auto pa = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
        auto p1 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{1});
        auto pMult = std::make_unique<BinaryExpression>(std::move(pa), Token(TokenType::STAR, 0, 1, "*"), std::move(p1));

        // Replacement
        // a
        auto ra = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");

        // Source
        // (9+69+4) * 1
        auto s1 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{9});
        auto s2 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{69});
        auto s3 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{4});
        auto s4 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{1});
        auto sPlus = std::make_unique<BinaryExpression>(std::move(s1), Token(PLUS, 0, 1, "+"), std::move(s2));
        auto sPlus1 = std::make_unique<BinaryExpression>(std::move(sPlus), Token(PLUS, 0, 1, "+"), std::move(s3));
        auto sMult = std::make_unique<BinaryExpression>(std::move(sPlus1), Token(STAR, 0, 1, "*"), std::move(s4));

        // No matchmap = all identifiers are all match constrainted
        auto applied = ExpressionRewriter::substitute(std::move(sMult), std::move(pMult), std::move(ra), {});
        CHECK(printer.print(*applied.rewrittenExpr.get()) == "((9+69)+4)");

    }
}

TEST_CASE("Multi match replacements") {
    AstPrinter printer;

    SECTION("Multi subexpression matches") {
        // Pattern
        // a*1
        auto pa = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
        auto p1 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue{1});
        auto pMult = std::make_unique<BinaryExpression>(std::move(pa), Token(TokenType::STAR, 0, 1, "*"), std::move(p1));

        // Replacement
        // a
        auto ra = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");

        // Expected
        // (69*1) / (420*1) -> 69 / 420
        auto s69 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue(69));
        auto s11 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue(1));
        auto sMult1 = std::make_unique<BinaryExpression>(std::move(s69), Token(STAR,0,1,"*"), std::move(s11));

        auto s420 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue(420));
        auto s12 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue(1));
        auto sMult2 = std::make_unique<BinaryExpression>(std::move(s420), Token(STAR,0,1,"*"), std::move(s12));

        auto sDiv = std::make_unique<BinaryExpression>(std::move(sMult1), Token(SLASH,0,1,"/"), std::move(sMult2));

        auto applied = ExpressionRewriter::substitute(std::move(sDiv), std::move(pMult), std::move(ra));
        CHECK(printer.print(*applied.rewrittenExpr.get()) == "(69/420)");
    }

    SECTION("Multi subexpression matches 2") {
        // Pattern
        // a+b
        auto pa = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
        auto pb = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "b");
        auto pPlus = std::make_unique<BinaryExpression>(std::move(pa), Token(TokenType::PLUS, 0, 1, "+"), std::move(pb));

        // Replacement
        // b+a
        auto ra = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
        auto rb = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "b");
        auto rPlus = std::make_unique<BinaryExpression>(std::move(rb), Token(PLUS,0,1,"+"), std::move(ra));

        // Expected
        // (5+2) * (69+420) -> (2+5) * (420+69)
        auto s5 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue(5));
        auto s2 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue(2));
        auto sPlus1 = std::make_unique<BinaryExpression>(std::move(s5), Token(PLUS,0,1,"+"), std::move(s2));

        auto s69 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue(69));
        auto s420 = std::make_unique<LiteralExpression>(Token(TokenType::NUMBER, 0, 0, ""), LiteralValue(420));
        auto sPlus2 = std::make_unique<BinaryExpression>(std::move(s69), Token(PLUS,0,1,"+"), std::move(s420));

        auto sMult = std::make_unique<BinaryExpression>(std::move(sPlus1), Token(STAR,0,1,"*"), std::move(sPlus2));

        auto applied = ExpressionRewriter::substitute(std::move(sMult), std::move(pPlus), std::move(rPlus));
        CHECK(printer.print(*applied.rewrittenExpr.get()) == "((2+5)*(420+69))");
    }

}

// Relies on the rewriter to substitute multiple pattern matches
TEST_CASE("Reduction") {
    AstPrinter printer;
    SECTION("Reduce negates") {
        // Pattern
        // --a
        auto pa = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");
        auto pnot1 = std::make_unique<UnaryExpression>(Token(MINUS,0,1,"-"), std::move(pa));
        auto pnot2 = std::make_unique<UnaryExpression>(Token(MINUS,0,1,"-"), std::move(pnot1));

        // Replacement
        // a
        auto ra = std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "a");

        for (int i = 1; i < 69; i++) {
            auto l =
                std::make_unique<LiteralExpression>(Token(TokenType::IDENTIFIER, 0, 0, ""), "x");
            auto uFinal =
                std::make_unique<UnaryExpression>(
                    Token(TokenType::MINUS, 0, 1, "-"), std::move(l));
            for (int j = 0; j < i; j++) {
                uFinal = std::make_unique<UnaryExpression>(
                    Token(TokenType::MINUS, 0, 1, "-"), std::move(uFinal));
            }

            Interpreter interpreter;
            MatchMap matchMap;
            auto applied = ExpressionRewriter::substitute(std::move(uFinal), pnot2->clone(), ra->clone(), matchMap, interpreter, true, true);
            CHECK(printer.print(*applied.rewrittenExpr.get()) == (i % 2 == 0 ? "-x" : "x"));
        }
    }
}

#endif
