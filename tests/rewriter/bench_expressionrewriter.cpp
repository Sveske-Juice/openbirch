#include "lexer/lexer.h"
#include "parser/parser.h"
#include "runtime/expressionrewriter.h"
#include "runtime/interpreter.h"

#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

TEST_CASE("Negation Reduction") {
    auto pa = std::make_unique<LiteralExpression>(Token(IDENTIFIER, 0, 0, ""), "a");
    auto pnot1 = std::make_unique<UnaryExpression>(Token(MINUS,0,1,"-"), std::move(pa));
    auto pnot2 = std::make_unique<UnaryExpression>(Token(MINUS,0,1,"-"), std::move(pnot1));

    auto ra = std::make_unique<LiteralExpression>(Token(IDENTIFIER, 0, 0, ""), "a");

    BENCHMARK_ADVANCED("Reduce double negate")(Catch::Benchmark::Chronometer meter) {
        std::string src(2, '-');
        Lexer lexer{src + "1"};
        Parser parser{lexer.tokenize()};
        auto stmts = parser.parse();
        Expression& ast = static_cast<ExpressionStatement *>(stmts[0].get())->expression();
        Interpreter interpreter;
        MatchMap matchMap;
        meter.measure([&ast, &pnot2, &ra, &interpreter, &matchMap] { ExpressionRewriter::substitute(ast.clone(), pnot2->clone(), ra->clone(), matchMap, interpreter, true, true); });
    };

    BENCHMARK_ADVANCED("Reduce negates 10")(Catch::Benchmark::Chronometer meter) {
        std::string src(10, '-');
        Lexer lexer{src + "1"};
        Parser parser{lexer.tokenize()};
        auto stmts = parser.parse();
        Expression& ast = static_cast<ExpressionStatement *>(stmts[0].get())->expression();
        Interpreter interpreter;
        MatchMap matchMap;
        meter.measure([&ast, &pnot2, &ra, &interpreter, &matchMap] { ExpressionRewriter::substitute(ast.clone(), pnot2->clone(), ra->clone(), matchMap, interpreter, true, true); });
    };

    BENCHMARK_ADVANCED("Reduce negates 300")(Catch::Benchmark::Chronometer meter) {
        std::string src(300, '-');
        Lexer lexer{src + "1"};
        Parser parser{lexer.tokenize()};
        auto stmts = parser.parse();
        Expression& ast = static_cast<ExpressionStatement *>(stmts[0].get())->expression();
        Interpreter interpreter;
        MatchMap matchMap;
        meter.measure([&ast, &pnot2, &ra, &interpreter, &matchMap] { ExpressionRewriter::substitute(ast.clone(), pnot2->clone(), ra->clone(), matchMap, interpreter, true, true); });
    };
}
