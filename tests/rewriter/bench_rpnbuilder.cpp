#include "lexer/lexer.h"
#include "parser/parser.h"
#include "parser/statement.h"
#include "runtime/expressionrewriter.h"

#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

TEST_CASE("RPN Expression Builder") {
    BENCHMARK_ADVANCED("RPNBuilder Negate 10")(Catch::Benchmark::Chronometer meter) {
        std::string src(10, '-');
        Lexer lexer{src + "1"};
        Parser parser{lexer.tokenize()};
        auto stmts = parser.parse();
        Expression& ast = static_cast<ExpressionStatement *>(stmts[0].get())->expression();
        RPNExpressionBuilder builder;

        meter.measure([&ast, &builder] { return builder.buildRPNQueue(ast); });
    };

    BENCHMARK_ADVANCED("RPNBuilder Negate 100")(Catch::Benchmark::Chronometer meter) {
        std::string src(100, '-');
        Lexer lexer{src + "1"};
        Parser parser{lexer.tokenize()};
        auto stmts = parser.parse();
        Expression& ast = static_cast<ExpressionStatement *>(stmts[0].get())->expression();
        RPNExpressionBuilder builder;

        meter.measure([&ast, &builder] { return builder.buildRPNQueue(ast); });
    };

    BENCHMARK_ADVANCED("RPNBuilder Negate 1000")(Catch::Benchmark::Chronometer meter) {
        std::string src(1000, '-');
        Lexer lexer{src + "1"};
        Parser parser{lexer.tokenize()};
        auto stmts = parser.parse();
        Expression& ast = static_cast<ExpressionStatement *>(stmts[0].get())->expression();
        RPNExpressionBuilder builder;

        meter.measure([&ast, &builder] { return builder.buildRPNQueue(ast); });
    };

    BENCHMARK_ADVANCED("RPNBuilder Negate 10000")(Catch::Benchmark::Chronometer meter) {
        std::string src(10000, '-');
        Lexer lexer{src + "1"};
        Parser parser{lexer.tokenize()};
        auto stmts = parser.parse();
        Expression& ast = static_cast<ExpressionStatement *>(stmts[0].get())->expression();
        RPNExpressionBuilder builder;

        meter.measure([&ast, &builder] { return builder.buildRPNQueue(ast); });
    };
}
