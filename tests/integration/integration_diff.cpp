#include <catch2/catch_test_macros.hpp>
#include <memory>

#include "tests_opts.h"
#include "lexer/lexer.h"
#include "parser/ast_printer.h"
#include "parser/expression.h"
#include "parser/parser.h"
#include "runtime/interpreter.h"

#if TEST_DIFF

TEST_CASE("Diff std") {
    std::string input = "using \"std/core.obd\";";

    SECTION("Load std") {
        Lexer lexer{input};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        
        REQUIRE_NOTHROW(interpreter.interpret());
    }

    SECTION("Power rule (x)") {
        Lexer lexer{input + "d/dx x;"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        REQUIRE(res.value()->expressionType() == EXPR_NUMBER);
        REQUIRE(static_cast<LiteralExpression*>(res.value().get())->value().number_value == 1);
    }

    SECTION("Power rule (x^2)") {
        Lexer lexer{input + "d/dx x^2;"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "(2*x)");
    }

    SECTION("Power rule 69*x^2") {
        Lexer lexer{input + "d/dx (69*x^2);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "(138*x)");
    }

    SECTION("Sum rule 69+x") {
        Lexer lexer{input + "d/dx (69 + x);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "1");
    }

    SECTION("Sum rule x^2+x") {
        Lexer lexer{input + "d/dx (x^2 + x);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "((2*x)+1)");
    }

    SECTION("Difference rule 69-x") {
        Lexer lexer{input + "d/dx (69 - x);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "-1");
    }

    SECTION("Difference rule x^2-x") {
        Lexer lexer{input + "d/dx (x^2 - x);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "((2*x)-1)");
    }

    SECTION("Product rule x*2x") {
        Lexer lexer{input + "d/dx (x * 2*x);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "(4*x)");
    }

    SECTION("Product rule (2*x^4)*x^3") {
        Lexer lexer{input + "d/dx ((2 * x^4) * x^3);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "(((8*(x^3))*(x^3))+(6*(x^6)))");
    }

    SECTION("Quotient rule 69/x") {
        Lexer lexer{input + "d/dx (69 / x);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "(-69/(x^2))");
    }

    SECTION("Quotient rule (69*x)/x") {
        Lexer lexer{input + "d/dx ((69 * x) / x);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "(((69*x)-(69*x))/(x^2))");
    }

    SECTION("Quotient rule (69*x^5)/(4*x^3)") {
        Lexer lexer{input + "d/dx ((69 * x^5) / (4 * x^3));"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "(((1380*(x^7))-(828*(x^7)))/(16*((x^3)^2)))");
    }
}

TEST_CASE("Trig derivatives") {
    std::string input = "using \"std/core.obd\";";

    SECTION("Sine derivative") {
        Lexer lexer{input + "d/dx sin(x)"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "cos(x)");
    }

    SECTION("Cosine derivative") {
        Lexer lexer{input + "d/dx cos(x)"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "-sin(x)");
    }

    SECTION("Tangent derivative") {
        Lexer lexer{input + "d/dx tan(x)"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "(sec(x)^2)");
    }
}

TEST_CASE("Custom functions") {
    SECTION("x") {
        Lexer lexer{"f(x):=x;d/dx f;"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "1");
    }

    SECTION("sum") {
        Lexer lexer{"f(x):=x;g(x):=2*x;d/dx f+g;"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "3");
    }

    SECTION("product") {
        Lexer lexer{"f(x):=x^2;g(x):=3*x^3;d/dx f*g;"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        AstPrinter printer;
        REQUIRE(printer.print(*res.value().get())== "(((2*x)*(3*(x^3)))+((9*(x^2))*(x^2)))");
    }
}
TEST_CASE("Respected variables") {
    SECTION("Respected variable depth 1") {
        Lexer lexer{"d/dx y;"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        REQUIRE(res.value()->expressionType() == EXPR_DIFFERENTIATE);
    }

    SECTION("Respected variable depth 2") {
        Lexer lexer{"d/dx sin(y);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
        REQUIRE(res.has_value());
        REQUIRE(res.value()->expressionType() == EXPR_DIFFERENTIATE);
    }
}


TEST_CASE("General Diff") {
    SECTION("d/dx sin(x^2)*x") {
        Lexer lexer{"d/dx sin(x^2)*x"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        auto res = interpreter.interpret();
    }
}

#endif
