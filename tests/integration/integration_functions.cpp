#include <catch2/catch_test_macros.hpp>
#include <memory>

#include "lexer/lexer.h"
#include "parser/ast_printer.h"
#include "parser/expression.h"
#include "parser/parser.h"
#include "parser/statement.h"
#include "runtime/interpreter.h"

TEST_CASE("Function declaration", "[Identity function]") {
    Lexer lexer{"f(x):=x;"};
    Parser parser{lexer.tokenize()};
    auto ast = parser.parse();
    Interpreter interpreter{std::move(ast)};

    REQUIRE_NOTHROW(interpreter.interpret());
}

TEST_CASE("Identifiers dont match functions") {
    Lexer lexer{"x:=2;x"};
    Parser parser{lexer.tokenize()};
    auto ast = parser.parse();
    Interpreter interpreter{std::move(ast)};

    REQUIRE_NOTHROW(interpreter.interpret());
}

TEST_CASE("Function calling", "[Identity function]") {
    Lexer lexer{"f(x):=x;f(2)"};
    Parser parser{lexer.tokenize()};
    auto ast = parser.parse();
    Interpreter interpreter{std::move(ast)};

    auto result = interpreter.interpret();
    REQUIRE(result.has_value());
    REQUIRE(result.value()->expressionType() == EXPR_NUMBER);
    REQUIRE(static_cast<LiteralExpression*>(result.value().get())->value().number_value == 2);
}

TEST_CASE("Function calling, call with constant") {
    Lexer lexer{"f(x):=2*x;f(2)"};
    Parser parser{lexer.tokenize()};
    auto ast = parser.parse();
    Interpreter interpreter{std::move(ast)};

    auto result = interpreter.interpret();
    REQUIRE(result.has_value());

    // Manually eval
    interpreter = Interpreter{std::make_unique<ExpressionStatement>(std::unique_ptr<Expression>(result.value().get()))};
    result = interpreter.interpret();

    REQUIRE(result.value()->expressionType() == EXPR_NUMBER);
    REQUIRE(static_cast<LiteralExpression*>(result.value().get())->value().number_value == 4);
}

TEST_CASE("Function calling call with variable") {
    Lexer lexer{"f(x):=2*x;f(a);"};
    Parser parser{lexer.tokenize()};
    auto ast = parser.parse();
    Interpreter interpreter{std::move(ast)};

    auto result = interpreter.interpret();
    REQUIRE(result.has_value());
    REQUIRE(result.value()->toString() == "(2*a)");
}

TEST_CASE("Function calling, call with same variable as arg") {
    Lexer lexer{"f(x):=x;f(x);"};
    Parser parser{lexer.tokenize()};
    auto ast = parser.parse();
    Interpreter interpreter{std::move(ast)};

    auto result = interpreter.interpret();
    REQUIRE(result.has_value());
    REQUIRE(result.value()->expressionType() == EXPR_IDENTIFIER);
    REQUIRE(strcmp(static_cast<LiteralExpression*>(result.value().get())->value().string_value, "x") == 0);
}

/* MULTI VARIABLE FUNCTIONS */
TEST_CASE("Multivariable Functions") {
    AstPrinter printer;
    SECTION("Multivar func 1") {
        Lexer lexer{"f(a,b):=a*b;f(69,420);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};

        auto result = interpreter.interpret();
        REQUIRE(result.has_value());
        REQUIRE(printer.print(*result.value().get()) == "28980");
    }
    SECTION("Multivar func 2") {
        Lexer lexer{"f(a,b,c,d,e,f,g,h):=a*b*c*d*e*f*g*h;f(1,2,3,4,5,6,7,8);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};

        auto result = interpreter.interpret();
        REQUIRE(result.has_value());
        REQUIRE(printer.print(*result.value().get()) == "40320");
    }
}

/* VECTOR FUNCTIONS */
TEST_CASE("Vector functions") {
    AstPrinter printer;
    SECTION("VectorFunc identity") {
        Lexer lexer{"f(x):=[x];f(69);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};

        auto result = interpreter.interpret();
        REQUIRE(result.has_value());
        REQUIRE(result.value()->expressionType() == EXPR_VECTOR);
        REQUIRE(printer.print(*result.value().get()) == "[69]");
    }
    SECTION("VectorFunc 2") {
        Lexer lexer{"f(x):=[x, cos(x), x];f(0);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};

        auto result = interpreter.interpret();
        REQUIRE(result.has_value());
        REQUIRE(result.value()->expressionType() == EXPR_VECTOR);
        REQUIRE(printer.print(*result.value().get()) == "[0, 1, 0]");
    }
    SECTION("VectorFunc 3") {
        Lexer lexer{"f(x):=[x, 2 * x, nroot(27, x)];f(3);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};

        auto result = interpreter.interpret();
        REQUIRE(result.has_value());
        REQUIRE(printer.print(*result.value().get()) == "[3, 6, 3]");
    }
}

TEST_CASE("Multivariable vector function") {
    AstPrinter printer;
    SECTION("MultivarVec func 1") {
        Lexer lexer{"f(x,y,z):=[x, 2 * y, nroot(z, 7)];f(69, 420, 128);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};

        auto result = interpreter.interpret();
        REQUIRE(result.has_value());
        REQUIRE(printer.print(*result.value().get()) == "[69, 840, 2]");
    }
}
