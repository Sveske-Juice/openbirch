#include "lexer/lexer.h"
#include "parser/parser.h"
#include "runtime/interpreter.h"
#include "runtime/runtime_errors.h"

#include <catch2/catch_test_macros.hpp>
#include <iostream>

namespace {
std::stringstream stream;
TEST_CASE("Native Functions integration") {
    // Redirect std::cout to custom stream
    std::cout.rdbuf(stream.rdbuf());

    // Test no crash of functions returning void
    SECTION("Void return") {
        Lexer lexer{"print(\"Hello World!\");"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        std::optional<std::unique_ptr<Expression>> res;
        REQUIRE_NOTHROW(res = interpreter.interpret());
    }

    // Should throw if more arguments provided than specified by the function
    SECTION("Call arity error") {
        Lexer lexer{"sin(2, 5);"};
        Parser parser{lexer.tokenize()};
        auto ast = parser.parse();
        Interpreter interpreter{std::move(ast)};
        try {
            interpreter.interpret();
            FAIL();
        } catch(const CallArity& e) {
            SUCCEED();
        }
    }
}
}
