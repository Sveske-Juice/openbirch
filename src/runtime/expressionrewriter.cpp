#include "runtime/expressionrewriter.h"
#include "lexer/token.h"
#include "parser/expression.h"
#include "parser/iexpression_visitor.h"
#include "runtime/abstract_function.h"
#include "runtime/interpreter.h"
#include "runtime/procedure_functions.h"
#include "runtime/runtime_errors.h"

#include <cassert>
#include <cstddef>
#include <fmt/core.h>
#include <iostream>
#include <memory>
#include <utility>
#include <variant>

// debug rewriter
#define RDEBUG 0

#define NO_MATCH 0
#define MATCH 1

void printVec(std::deque<Expression *> &vec) {
    if (vec.size() == 0)
        return;
    if (vec.size() == 1) {
        std::cout << "{" << vec[0]->toString() << "}" << std::endl;
        return;
    }
    std::cout << "{ " << vec[0]->toString();
    for (size_t i = 1; i < vec.size() - 1; i++) {
        std::cout << ", " << vec[i]->toString();
    }
    std::cout << ", " << vec.back()->toString() << " }" << std::endl;
}

RewriteResult
ExpressionRewriter::substitute(std::unique_ptr<Expression> rootSource,
                               std::unique_ptr<Expression> pattern,
                               std::unique_ptr<Expression> replacement_,
                               bool substituteAll, bool recursiveSubstitution) {
    Interpreter interpreter;
    MatchMap matchMap;
    return substitute(std::move(rootSource), std::move(pattern),
                      std::move(replacement_), matchMap, interpreter,
                      substituteAll, recursiveSubstitution);
}

RewriteResult ExpressionRewriter::substitute(
    std::unique_ptr<Expression> rootSource, std::unique_ptr<Expression> pattern,
    std::unique_ptr<Expression> replacement, const MatchMap &matchMap,
    Interpreter &interpreter, bool substituteAll, bool recursiveSubstitution) {

    SymbolMap symbolMap;
    bool matched{false};

#if RDEBUG
    std::cout << "Applying rule. Pattern: " << pattern->toString()
              << ", Source: " << rootSource->toString()
              << ", Replacement: " << replacement->toString() << std::endl;
#endif
    RPNExpressionBuilder rpnBuilder;

    // Create reverse polish notation sequence for the pattern to search for
    ExpressionDeque patternPostfix = rpnBuilder.buildRPNQueue(*pattern.get());

#if RDEBUG
    std::cout << "Pattern RPN:";
    printVec(this->patternPostfix);
#endif

    std::unique_ptr<Expression> oldSource{nullptr};
    do {
        ExpressionDeque sourcePostfix =
            rpnBuilder.buildRPNQueue(*rootSource.get());

        oldSource = rootSource->clone();

#if RDEBUG
        std::cout << "Source RPN:";
        printVec(this->sourcePostfix);
#endif

        // Check and substitute while there is still source nodes that haven't
        // been visited
        do {
            Expression *replacementNode =
                checkForMatch(patternPostfix, sourcePostfix, symbolMap,
                              matchMap, interpreter);

            // No substitution needed when no matches are found
            if (!replacementNode)
                break;

            matched = true;
            // Since resolving the replacement modifies it, we need to store the
            // old for potential replacement resolution later for other pattern
            // matches
            auto replacementCopy = replacement->clone();

            // Resolve all identifiers in replacement to the corresponding
            // expressions found in source
            ExpressionDeque replacementPostfix =
                rpnBuilder.buildRPNQueue(*replacement.get());

            for (Expression *node : replacementPostfix) {
                auto newReplacement =
                    resolveReplacement(node, symbolMap);
                if (newReplacement)
                    replacement = std::move(newReplacement);
            }

            std::unique_ptr<Expression> finalReplacement = replaceNodeInAst(
                *replacementNode, std::move(replacement), &symbolMap);
            replacement = std::move(replacementCopy);
            symbolMap.clear();

            if (finalReplacement) {
                printVec(sourcePostfix);
                assert(
                    sourcePostfix.size() == 0 &&
                    "All source nodes should've been consumed if source wasn't "
                    "modified");
                rootSource = std::move(finalReplacement);
                break;
            }

            if (!substituteAll)
                break;

        } while (sourcePostfix.size() > 0);

        if (!recursiveSubstitution)
            break;

        // Now that the root have been modified we need to make sure that
        // potential old reference to the root's parent are discarded, because
        // this is the new root
        rootSource->parent = nullptr;
    } while (!rootSource->equal(*oldSource.get()));

    RewriteResult result;
    result.rewrittenExpr = std::move(rootSource);
    result.matched = matched;
    return result;
}

int ExpressionRewriter::matchPart(const Expression &sourceExpr,
                                  ExpressionDeque &sourcePostfix,
                                  const MatchMap &matchMap,
                                  SymbolMap &symbolMap,
                                  const Expression &patternExpr,
                                  Interpreter &interpreter) {
    switch (patternExpr.expressionType()) {
    case EXPR_NUMBER: {
        if (sourceExpr.expressionType() != patternExpr.expressionType())
            return NO_MATCH;

        const auto &patternLiteral =
            static_cast<const LiteralExpression &>(patternExpr);
        const auto &sourceLiteral =
            static_cast<const LiteralExpression &>(sourceExpr);

        if (patternLiteral.value().number_value !=
            sourceLiteral.value().number_value)
            return NO_MATCH;

        return MATCH;
    }

    case EXPR_PLUS:
    case EXPR_MINUS:
    case EXPR_MULTIPLICATION:
    case EXPR_DIVISION:
    case EXPR_MODULUS:
    case EXPR_NEGATE:
    case EXPR_LOGIC_NEGATE:
    case EXPR_EQUALS:
    case EXPR_NOT_EQUALS:
    case EXPR_GREATER:
    case EXPR_LESS:
    case EXPR_LOGIC_OR:
    case EXPR_LOGIC_AND:
    case EXPR_EXPONENT: {
        if (sourceExpr.expressionType() != patternExpr.expressionType())
            return NO_MATCH;

        return MATCH;
    }
    case EXPR_IDENTIFIER: {
        const auto &patternSymbol =
            static_cast<const LiteralExpression &>(patternExpr);

        // Guard statement to check for the identifier match type (variable,
        // constant or all)
        auto matchIt = matchMap.find(patternSymbol.value().string_value);
        bool matchAll = matchIt == matchMap.end();

        if (!matchAll) {
            auto definition = interpreter.getDefinition(matchIt->second);
            if (!definition.has_value())
                throw NotDefined(
                    Token(VOID, 0, 0, ""),
                    fmt::format("Procedure '{}' used as predicate is undefined",
                                matchIt->second));

            if (std::holds_alternative<std::unique_ptr<Expression>>(
                    *definition.value())) // Contains a regular value, like 3.
                throw TypeMismatch(Token(VOID, 0, 0, ""),
                                   "Predicate '{}' is not a procedure");

            auto &function = std::get<std::unique_ptr<AbstractFunction>>(
                *definition.value());

            if (function->type() != PROCEDURE_FUNCTION)
                throw TypeMismatch(Token(VOID, 0, 0, ""),
                                   "Predicate '{}' is not a procedure");

            auto &procedure = static_cast<ProcedureFunction &>(*function);
            ParameterList parameters;
            parameters.push_back(sourceExpr.clone());

            CallContext callCtx(*patternSymbol.clone(), interpreter,
                                std::move(parameters));

            auto returned = procedure.call(callCtx);

            if (returned.expr) {
                if (returned.expr->expressionType() != EXPR_BOOLEAN) {
                    delete returned.expr;
                    throw TypeMismatch(Token(VOID, 0, 0, ""),
                                       "Predicate '{}' did not return a value "
                                       "of type 'BOOLEAN'");
                }

                std::unique_ptr<LiteralExpression> result{
                    static_cast<LiteralExpression *>(returned.expr)};

                if (!result->value().boolean_value)
                    return NO_MATCH;

            } else {
                throw TypeMismatch(Token(VOID, 0, 0, ""),
                                   "Predicate '{}' did not return a value");
            }
        }

        auto symbolIt = symbolMap.find(patternSymbol.value().string_value);

        // Already defined. So verify that the stored value is the same as
        // this, because otherwise X*X will trigger for any multiplication
        // not just squares.
        if (symbolIt != symbolMap.end()) {
            if (!symbolIt->second->equal(sourceExpr))
                return NO_MATCH;
        }
#if RDEBUG
        std::cout << "setting " << patternSymbol.value().string_value
                  << &patternSymbol << " = " << sourceExpr.toString()
                  << &sourceExpr << std::endl;
#endif
        symbolMap[patternSymbol.value().string_value] =
            std::addressof(sourceExpr);

        return sourceExpr.nodeCount();
    }

    case EXPR_DIFFERENTIATE: {
        if (sourceExpr.expressionType() != patternExpr.expressionType())
            return NO_MATCH;

        const auto &sourceBinary =
            static_cast<const BinaryExpression &>(sourceExpr);

        // Map the variable to differentiate with respect to,
        // prefixed with d to symbol map. So we can replace
        // the variable with the correct variable instead of x
        auto value = sourceBinary.left().clone();
        // FIXME: potential memory leak
        symbolMap["dx"] = value.release();
        return MATCH;
    }

    case EXPR_CALL: {
        if (sourceExpr.expressionType() != patternExpr.expressionType())
            return NO_MATCH;

        const auto &sourceCall =
            static_cast<const CallExpression &>(sourceExpr);
        const auto &patternCall =
            static_cast<const CallExpression &>(patternExpr);
        if (sourceCall.lambdaName().value().string_value !=
            patternCall.lambdaName().value().string_value)
            return NO_MATCH;

        return MATCH;
    }

    case EXPR_VECTOR: {
        if (sourceExpr.expressionType() != patternExpr.expressionType())
            return NO_MATCH;

        const auto &sourceVector =
            static_cast<const VectorExpression &>(sourceExpr);
        const auto &patternVector =
            static_cast<const VectorExpression &>(patternExpr);

        if (sourceVector.size() != patternVector.size())
            return NO_MATCH;

        for (size_t i = 0; i < sourceVector.size(); i++) {
            if (!matchPart(sourceVector.get_const(i), sourcePostfix, matchMap,
                           symbolMap, patternVector.get_const(i),
                           interpreter)) {
                printf("Source vector %s does NOT match %s",
                       sourceVector.toString().c_str(),
                       patternVector.toString().c_str());
                return NO_MATCH;
            }
        }
        return MATCH;
    }

    default:
        throw UnimplementedLogic(
            0, 0,
            fmt::format(
                "ExpressionRewriter unimplemented for expression type '{}'",
                ExpressionTypeToString[patternExpr.expressionType()]));
    }
}

Expression *ExpressionRewriter::checkForMatch(ExpressionDeque &patternPostfix,
                                              ExpressionDeque &sourcePostfix,
                                              SymbolMap &symbolMap,
                                              const MatchMap &matchMap,
                                              Interpreter &interpreter) {
    if (sourcePostfix.size() < patternPostfix.size())
        return nullptr;

    while (sourcePostfix.size() > 0) {
        Expression *sourceNode = sourcePostfix.front();
        int totalMatches{0};
        int matches{0};
        for (Expression *patternExpr : patternPostfix) {
#if RDEBUG
            std::cout << "CHECK " << patternExpr->toString() << " == "
                      << (**(sourcePostfix.begin() + totalMatches)).toString()
                      << std::endl;
#endif
            matches = matchPart(**(sourcePostfix.begin() + totalMatches),
                                sourcePostfix, matchMap, symbolMap,
                                *patternExpr, interpreter);
            if (!matches)
                break;

#if RDEBUG
            std::cout << "MATCH (" << matches << ")" << std::endl;
#endif
            totalMatches += matches;
        }

        if (matches) {
            for (int i = 0; i < totalMatches; i++) {
                sourcePostfix.pop_front();
            }
            return sourceNode;
        }

        sourcePostfix.pop_front();
        symbolMap.clear();
    }
    return nullptr;
}

std::unique_ptr<Expression> ExpressionRewriter::resolveReplacement(
    Expression *node, SymbolMap &symbolMap) {
    const Expression *correspondingExpression{nullptr};

    // Since we replace the source with the replacement.
    // the replacement AST's tokens have location info
    // about where they were lexed in their respective
    // .obd file. But this info is wrong when a new expression
    // is created, so set it to -1 to indicate no location info.
    node->tokenReference().sourceOffset = -1;

    if (node->expressionType() != EXPR_IDENTIFIER)
        return nullptr;

    const auto &nodeStr = static_cast<const LiteralExpression &>(*node);
    auto correspondingExpressionIt =
        symbolMap.find(nodeStr.value().string_value);

    // Identifier wasn't resolved to any expression, therefore just
    // let the identifier be
    if (correspondingExpressionIt == symbolMap.cend())
        return nullptr;

    correspondingExpression = correspondingExpressionIt->second;
#if RDEBUG
    std::cout << nodeStr.toString() << " mapped to "
              << correspondingExpression->toString() << std::endl;
#endif

    std::unique_ptr<Expression> replaced =
        replaceNodeInAst(*node, correspondingExpression->clone(), &symbolMap);

    return replaced;
}

std::unique_ptr<Expression>
ExpressionRewriter::replaceNodeInAst(Expression &node,
                                     std::unique_ptr<Expression> replacement,
                                     SymbolMap *symbolMap) {
    Expression *parentNode = node.parent;
#if RDEBUG
    std::cout << "Replacing " << node.toString() << " with "
              << replacement->toString() << std::endl;
#endif
    if (!parentNode) {
        replacement->parent = nullptr;
        return replacement;
    }

    switch (parentNode->expressionType()) {
    case EXPR_DIFFERENTIATE: {
        auto &parent = static_cast<BinaryExpression &>(*parentNode);

        auto it = symbolMap->find("dx");
        if (it != symbolMap->end()) {
            Expression *expr = const_cast<Expression *>(it->second);
            parent.setLeft(std::unique_ptr<Expression>(expr));
            symbolMap->erase(it);
        }

        // Replace expression to differentiate (guranteed to be rhs of
        // binaray expr)
        parent.setRight(std::move(replacement));
        break;
    }

    case EXPR_PLUS:
    case EXPR_MINUS:
    case EXPR_MULTIPLICATION:
    case EXPR_DIVISION:
    case EXPR_MODULUS:
    case EXPR_EQUALS:
    case EXPR_EXPONENT: {
        auto &parent = static_cast<BinaryExpression &>(*parentNode);

        // Figure out which side of parent the node is on
        bool rhs = &parent.right() == &node ? true : false;
        if (rhs)
            parent.setRight(std::move(replacement));
        else
            parent.setLeft(std::move(replacement));

        break;
    }
    case EXPR_NEGATE:
    case EXPR_LOGIC_NEGATE: {
        auto &parent = static_cast<UnaryExpression &>(*parentNode);

        parent.setOperand(std::move(replacement));
        break;
    }
    case EXPR_CALL: {
        auto &parent = static_cast<CallExpression &>(*parentNode);
        int parameterIdx = -1;
        for (size_t i = 0; i < parent.parameters().size(); i++) {
            if (parent.parameters()[i].get() == &node) {
                parameterIdx = i;
                break;
            }
        }
        assert(parameterIdx != -1 && "Could not determine which parameter in "
                                     "the function call to replace");
        parent.setParameter(parameterIdx, std::move(replacement));
        break;
    }
    case EXPR_VECTOR: {
        auto &parent = static_cast<VectorExpression &>(*parentNode);
        int parameterIdx = -1;
        for (size_t i = 0; i < parent.size(); i++) {
            if (&parent.get_const(i) == &node) {
                parameterIdx = i;
                break;
            }
        }
        assert(parameterIdx != -1 && "Could not determine which index in "
                                     "the vector to replace");
        parent.set(parameterIdx, std::move(replacement));
        break;
    }

    default:
        throw UnimplementedLogic(
            0, 0,
            fmt::format("ExpressionRewriter::replaceNodeInAst unimplemented "
                        "for expression type '{}'",
                        ExpressionTypeToString[parentNode->expressionType()]));
    }
    return nullptr;
}

ExpressionDeque RPNExpressionBuilder::buildRPNQueue(Expression &root) {
    this->rpn.clear();
    root.accept(*this);
    return rpn;
}

void RPNExpressionBuilder::visitLiteralExpression(
    LiteralExpression &expression) {
    rpn.push_back(std::addressof(expression));
}
void RPNExpressionBuilder::visitVectorExpression(VectorExpression &expression) {
    rpn.push_back(std::addressof(expression));
    for (size_t i = 0; i < expression.size(); i++) {
        expression.get(i).accept(*this);
    }
}

void RPNExpressionBuilder::visitMapExpression(MapExpression &expression) {
    rpn.push_back(std::addressof(expression));
    for (auto &[key, value] : expression.mapReference()) {
        key->accept(*this);
        value->accept(*this);
    }
}

void RPNExpressionBuilder::visitLambdaExpression(LambdaExpression &expression) {
    expression.body().accept(*this);
}
void RPNExpressionBuilder::visitCallExpression(CallExpression &expression) {
    rpn.push_back(std::addressof(expression));
    for (size_t i = 0; i < expression.parameters().size(); i++) {
        expression.parameters()[i]->accept(*this);
    }
}
void RPNExpressionBuilder::visitBinaryExpression(BinaryExpression &expression) {
    rpn.push_back(std::addressof(expression));

    // Don't push the variable with respect to on the stack
    if (expression.expressionType() != EXPR_DIFFERENTIATE)
        expression.left().accept(*this);

    expression.right().accept(*this);
}
void RPNExpressionBuilder::visitUnaryExpression(UnaryExpression &expression) {
    rpn.push_back(std::addressof(expression));
    expression.operand().accept(*this);
}
