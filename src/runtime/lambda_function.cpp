#include "runtime/lambda_function.h"
#include "runtime/interpreter.h"
#include "runtime/expressionrewriter.h"
#include "runtime/runtime_errors.h"

#include <cstddef>

FunctionReturn
LambdaFunction::call(CallContext& callCtx) {
  auto lambdaBody = lambdaExpression_->body().clone();

  auto boundVariables = lambdaExpression_->boundVariables();

  callCtx.interpreter.pushScope();
  size_t i = 0;
  for (auto &variable : boundVariables) {
      callCtx.interpreter.addDefinition(variable, std::move(callCtx.parameterList[i++]),
              true);
  }
  lambdaBody->accept(callCtx.interpreter);
  auto res = std::move(callCtx.interpreter.workingStack.top());
  callCtx.interpreter.workingStack.pop();
  callCtx.interpreter.popScope();

  return { SUCCESS, res.release() };
}
