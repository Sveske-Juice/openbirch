#include "runtime/interpreter.h"
#include "lexer/lexer.h"
#include "lexer/lexer_errors.h"
#include "lexer/token.h"
#include "parser/ast_printer.h"
#include "parser/expression.h"
#include "parser/parser.h"
#include "parser/parser_errors.h"
#include "parser/statement.h"
#include "runtime/abstract_function.h"
#include "runtime/expressionrewriter.h"
#include "runtime/lambda_function.h"
#include "runtime/native_function.h"
#include "runtime/native_functions.h"
#include "runtime/procedure_functions.h"
#include "runtime/runtime_errors.h"

#include <cassert>
#include <cmath>
#include <cstddef>
#include <cstring>
#include <filesystem>
#include <fmt/core.h>
#include <fstream>
#include <iostream>
#include <memory>
#include <variant>
#include <vector>

Interpreter::Interpreter() { setupEnvironment(); }

Interpreter::Interpreter(std::vector<std::unique_ptr<Statement>> _statements)
    : statements(std::move(_statements)) {
    setupEnvironment();
}

Interpreter::Interpreter(std::unique_ptr<Statement> _statement) {
    statements.push_back(std::move(_statement));
    setupEnvironment();
}

void Interpreter::setupEnvironment() {
    resetEnvironment();
    addNativeFunctions();
    addConstants();
    importStandardLibrary();
}

void Interpreter::addNativeFunctions() {
    // Register native functions
    addDefinition("print", std::make_unique<NativeFunction>(1, __native_print),
                  false);
    addDefinition("type", std::make_unique<NativeFunction>(1, __native_type, false, false),
                  false);
    addDefinition("contains", std::make_unique<NativeFunction>(2, __native_contains, false, false),
                  false);
    addDefinition("lhs", std::make_unique<NativeFunction>(1, __native_lhs, false, false),
                  false);
    addDefinition("rhs", std::make_unique<NativeFunction>(1, __native_rhs, false, false),
                  false);
    addDefinition("operand", std::make_unique<NativeFunction>(1, __native_operand, false, false),
                  false);
    addDefinition("index", std::make_unique<NativeFunction>(2, __native_index, false, false),
                  false);
    addDefinition("getfunc", std::make_unique<NativeFunction>(1, __native_get_func, false, false),
                  false);
    addDefinition("params", std::make_unique<NativeFunction>(2, __native_get_params, false, false),
                  false);
    addDefinition("replace", std::make_unique<NativeFunction>(3, __native_replace, false, false),
                  false);
    addDefinition("eval", std::make_unique<NativeFunction>(1, __native_eval, false, false),
                  false);

    addDefinition("sin", std::make_unique<NativeFunction>(1, __native_sin),
                  false);
    addDefinition("cos", std::make_unique<NativeFunction>(1, __native_cos),
                  false);
    addDefinition("tan", std::make_unique<NativeFunction>(1, __native_tan),
                  false);
    addDefinition("asin", std::make_unique<NativeFunction>(1, __native_asin),
                  false);
    addDefinition("acos", std::make_unique<NativeFunction>(1, __native_acos),
                  false);
    addDefinition("atan", std::make_unique<NativeFunction>(1, __native_atan),
                  false);
    addDefinition("cot", std::make_unique<NativeFunction>(1, __native_cot),
                  false);
    addDefinition("sec", std::make_unique<NativeFunction>(1, __native_sec),
                  false);
    addDefinition("csc", std::make_unique<NativeFunction>(1, __native_csc),
                  false);

    addDefinition("sqrt", std::make_unique<NativeFunction>(1, __native_sqrt),
                  false);
    addDefinition("cbrt", std::make_unique<NativeFunction>(1, __native_cbrt),
                  false);
    addDefinition("nroot", std::make_unique<NativeFunction>(2, __native_nroot),
                  false);

    addDefinition(
        "ln", std::make_unique<NativeFunction>(1, __native_natural_log), false);
    addDefinition("log", std::make_unique<NativeFunction>(2, __native_log),
                  false);
    addDefinition(
        "dot",
        std::make_unique<NativeFunction>(2, __native_vector_dotproduct, true),
        false);

    addDefinition("isolate",
                  std::make_unique<NativeFunction>(2, __native_isolate), false);

    addDefinition("autorun_add",
                  std::make_unique<NativeFunction>(1, __native_autorun_add),
                  false);
    addDefinition("autorun_list",
                  std::make_unique<NativeFunction>(0, __native_autorun_list),
                  false);
    addDefinition("autorun_set",
                  std::make_unique<NativeFunction>(1, __native_autorun_set),
                  false);
}

void Interpreter::addConstants() {
    // Add constant values
    addDefinition("pi",
                  std::make_unique<LiteralExpression>(Token(NUMBER, 0, 0, "pi"),
                                                      LiteralValue(M_PI)),
                  false);
    addDefinition("e",
                  std::make_unique<LiteralExpression>(Token(NUMBER, 0, 0, "e"),
                                                      LiteralValue(M_E)),
                  false);
}

void printvec(const std::vector<Token>& vec) {
    std::cout << "[";
    for (const auto &ele : vec) {
        std::cout << TokenTypesToString[ele.type] << ", ";
    }
    std::cout << "]" << std::endl;
}

bool Interpreter::importStandardLibrary() {
    // TODO: Replace with system-dependant data path for standard library
    auto source = "using \"std/core.obd\";";

    Lexer lexer(source);

    Parser parser(lexer.tokenize());

    try {
        parser.parse().at(0)->accept(*this);
    } catch (RuntimeException &e) {
        printf("Failed to import standard library: %s", e.what(source).c_str());
        return false;
    }
    return true;
}

void Interpreter::setIgnoreAssignments(bool ignore) {
    ignoreAssignments = ignore;
}

std::optional<std::unique_ptr<Expression>>
Interpreter::interpret(std::vector<std::unique_ptr<Statement>> statements_) {
    statements = std::move(statements_);
    return interpret();
}

std::optional<std::unique_ptr<Expression>> Interpreter::interpret() {
    // Clear from last use
    while (workingStack.size() > 0) {
        workingStack.pop();
    }

    for (size_t i = 0; i < statements.size(); i++) {
        statements[i]->accept(*this);
    }

    // Statements might not produce a value
    if (workingStack.empty())
        return {};

    return std::move(workingStack.top());
}

void Interpreter::visitExpressionStatement(ExpressionStatement &statement) {
    // auto rule = getRule(std::string{AUTO_APPLY_RULE});
    statement.expression().accept(*this);

    if (autorun.empty() || applyingAutoRule || workingStack.empty())
        return;

    std::unique_ptr<Expression> currentValue = std::move(workingStack.top());
    workingStack.pop();

    for (auto &name : autorun) {
        auto definition = getDefinition(name);

        if (!definition.has_value()) {
            std::cerr << std::endl
                      << "Autorun function '" << name << "' not found"
                      << std::endl;
            continue;
        }

        if (std::holds_alternative<std::unique_ptr<Expression>>(
                *definition.value())) // Contains a regular value, like 3.
            throw TypeMismatch(Token(VOID, 0, 0, ""),
                               "Autorun procedure '{}' is not a procedure");

        auto &function =
            std::get<std::unique_ptr<AbstractFunction>>(*definition.value());

        if (function->type() != PROCEDURE_FUNCTION)
            throw TypeMismatch(Token(VOID, 0, 0, ""),
                               "Autorun procedure '{}' is not a procedure");

        auto procedure = static_cast<ProcedureFunction *>(function.get());
        std::vector<std::unique_ptr<Expression>> parameters{};
        parameters.push_back(std::move(currentValue));

        applyingAutoRule = true;
        CallContext callCtx(statement.expression(), *this,
                            std::move(parameters));
        try {
            FunctionReturn returned = procedure->call(callCtx);

            if (returned.expr) {
                std::unique_ptr<Expression> result{returned.expr};
                workingStack.push(std::move(result));
            } else {
                workingStack.push(std::make_unique<LiteralExpression>(
                    Token(VOID, 0, 0, ""), LiteralValue(0)));
            }
        } catch (const std::exception &e) {
            applyingAutoRule = false;
            throw e;
        }
        applyingAutoRule = false;

        currentValue = std::move(workingStack.top());
        workingStack.pop();
    }

    workingStack.push(std::move(currentValue));
}

void Interpreter::visitDeclaration(DeclarationStatement &statement) {
    if (ignoreAssignments)
        return;

    auto value = statement.value().clone();

    // Get the value of identifiers, if any
    if (value->expressionType() == EXPR_IDENTIFIER) {
        value->accept(*this);
        value = std::move(workingStack.top());
        workingStack.pop();
    }

    if (value->expressionType() == EXPR_LAMBDA) {
        auto pointer_value = value.release();
        LambdaExpression *pointer =
            static_cast<LambdaExpression *>(pointer_value);

        auto function = std::make_unique<LambdaFunction>(
            std::unique_ptr<LambdaExpression>(pointer));

        addDefinition(statement.variableName(), std::move(function), true);
        return;
    }

    value->accept(*this);
    value = std::move(workingStack.top());
    workingStack.pop();

    addDefinition(statement.variableName(), std::move(value), false);
}

void Interpreter::visitIfStatement(IfStatement &statement) {
    auto condition = statement.ifCondition();

    // Eval condition
    condition->accept(*this);
    auto value = std::move(workingStack.top());
    workingStack.pop();

    if (value->expressionType() != EXPR_BOOLEAN) {
        throw TypeMismatch(statement.token(),
                           "If condition evaluated to non-boolean value");
    }

    const LiteralExpression &boolValue =
        static_cast<const LiteralExpression &>(*value);
    if (boolValue.value().boolean_value) { // Run main if branch
        CallCodeBlock(statement.ifBranch());
    } else { // Run potential else if branches
        const auto &elifBranches = statement.elifBranches();
        for (const auto &[condition, block] : elifBranches) {
            // Eval condition
            condition->accept(*this);
            auto value = std::move(workingStack.top());
            workingStack.pop();
            if (value->expressionType() != EXPR_BOOLEAN) {
                throw TypeMismatch(
                    condition->tokenReference(),
                    "Else if condition evaluated to non-boolean value");
            }
            const LiteralExpression &boolValue =
                static_cast<const LiteralExpression &>(*value);
            if (boolValue.value().boolean_value) { // Run elif branch
                CallCodeBlock(block);
                // We don't want to check conditions for other elif statements
                // when we find one condition that is true
                return;
            }
        }
        // If we get here, the main if condition and all elif conditions were
        // false, so exec else branch
        CallCodeBlock(statement.elseBranch());
    }
}

void Interpreter::CallCodeBlock(const CodeBlock &codeblock) {
    pushScope();
    try {
        for (auto &statement : codeblock) {
            statement->accept(*this);
        }
        popScope();
    } catch (ReturnValue &r) {
        popScope();
        throw std::move(r);
    }
}

void Interpreter::visitProcedure(ProcedureStatement &statement) {
    // Dont want evaluation showcase to define new procedures
    if (ignoreAssignments)
        return;

    std::string name = statement.name().literal;
    std::vector<std::string> arguments;
    for (Token argument : statement.arguments()) {
        arguments.push_back(argument.literal);
    }

    std::vector<std::unique_ptr<Statement>> body = statement.move_body();

    auto procedure =
        std::make_unique<ProcedureFunction>(name, arguments, std::move(body));

    addDefinition(name, std::move(procedure), false);
}

void Interpreter::visitReturn(ReturnStatement &statement) {
    statement.returnValue().accept(*this);
    auto value = std::move(workingStack.top());
    workingStack.pop();

    if (environmentStack.size() == 1) {
        // TODO: warn user that they are returning in global space?
        workingStack.push(std::move(value));
        return;
    }
    throw ReturnValue(std::move(value));
}

void Interpreter::visitImportStatement(ImportStatement &statement) {
    // Open file stream to path given by expression and interpret that file
    statement.expression().accept(*this);

    auto &value = workingStack.top();
    if (value->expressionType() != EXPR_STRING)
        throw TypeMismatch(statement.token(),
                           "Import statement requires a string path");

    // Save current path
    auto current_path = std::filesystem::current_path();

    // Evaluate path to char*
    std::filesystem::path path = std::filesystem::absolute(
        reinterpret_cast<LiteralExpression *>(value.get())->toString());

    // Open path
    std::fstream ifs(path, std::ios_base::in);

    // Check if everything is okie dokie
    if (ifs.fail()) {
        throw ImportError(statement.token(),
                          fmt::format("Could not import file '{}': {}",
                                      path.string(), strerror(errno)));
    }

    // cd to current path to ensure nested imports look in the correct folder
    std::filesystem::current_path(path.parent_path());

    // Read contents to string
    std::string content((std::istreambuf_iterator<char>(ifs)),
                        (std::istreambuf_iterator<char>()));

    workingStack.pop();

    size_t stacksize = workingStack.size();

    // Lex and parse
    try {
        Lexer lexer(content);
        Parser parser(lexer.tokenize());
        std::vector<std::unique_ptr<Statement>> imported_statements =
            parser.parse();

        // Evaluate file
        for (auto &imported_statement : imported_statements) {
            imported_statement.get()->accept(*this);
        }

    } catch (const LexerException &le) {
        std::string err_msg = "Lexer error when importing: " + le.what(content);
        throw ImportError(statement.token(), err_msg);
    } catch (const ParserException &pe) {
        std::string err_msg =
            "Parsing error when importing: " + pe.what(content);
        throw ImportError(statement.token(), err_msg);
    } catch (const ImportError &ie) {
        std::string err_msg =
            "Import error in imported file: " + ie.what(content);
        throw ImportError(statement.token(), err_msg);
    }

    assert(workingStack.size() >= stacksize &&
           "Somehow, the imported file used more values from the stack than it "
           "created :)");

    // Clean up after import
    while (workingStack.size() > stacksize)
        workingStack.pop();

    // Return to correct path afterwards
    std::filesystem::current_path(current_path);
}

void Interpreter::visitLiteralExpression(LiteralExpression &expression) {
    switch (expression.type()) {
    case IDENTIFIER: {
        auto definition = getDefinition(expression.value().string_value);

        if (!definition.has_value())
            break;

        if (std::holds_alternative<std::unique_ptr<Expression>>(
                *definition.value())) {
            // Contains a regular expression
            workingStack.push(
                std::get<std::unique_ptr<Expression>>(*definition.value())
                    ->clone());
            return;
        } else {
            auto &function = std::get<std::unique_ptr<AbstractFunction>>(
                *definition.value());

            if (function->type() == LAMBDA_FUNCTION) {
                const auto &lambdaFunction =
                    static_cast<const LambdaFunction &>(*function);
                workingStack.push(lambdaFunction.lambdaExpression().clone());
                return;
            }
        }
    }

    default:
        break;
    }

    workingStack.push(expression.clone());
}

std::unique_ptr<Expression> Interpreter::resolve(const Expression& expr) const {
    if (expr.expressionType() != EXPR_IDENTIFIER)
        return expr.clone();

    const auto &exprLit = AS_LITERALEXPR(expr);
    auto def = getDefinition(exprLit.value().string_value);
    if (!def.has_value())
        return expr.clone();

    if (std::holds_alternative<std::unique_ptr<AbstractFunction>>(*def.value()))
        return expr.clone();

    return std::get<std::unique_ptr<Expression>>(*def.value())->clone();
}

// FIXME: this code is actually disgusting...
void Interpreter::visitCallExpression(CallExpression &expression) {
    const LiteralExpression &lambdaName = expression.lambdaName();
    const ParameterList &parameters = expression.parameters();

    auto function = getDefinition(lambdaName.value().string_value);
    if (!function.has_value()) {
        workingStack.push(expression.clone());
        return;
    }


    if (!std::holds_alternative<std::unique_ptr<AbstractFunction>>(
            *function.value())) {

        auto &parameter = *std::get<std::unique_ptr<Expression>>(*function.value());

        if (parameter.expressionType() == EXPR_IDENTIFIER) {
            const auto &exprLit = AS_LITERALEXPR(parameter);
            auto def = getDefinition(exprLit.value().string_value);
            if (!def.has_value())
                throw TypeMismatch(
                        expression.tokenReference(),
                        fmt::format("Cannot call {} because it is not defined", lambdaName.value().string_value));

            if (std::holds_alternative<std::unique_ptr<AbstractFunction>>(*def.value())) {
                function = def;
            }
        }
        else if (parameter.expressionType() == EXPR_LAMBDA) {

            auto &lambdaExpr = static_cast<LambdaExpression &>(parameter);
            LambdaExpression* lambdaExprClone = static_cast<LambdaExpression*>(lambdaExpr.clone().release());
            auto lambdaFunc = std::make_unique<LambdaFunction>(std::unique_ptr<LambdaExpression>(lambdaExprClone));

            ParameterList resolvedParameters;
            resolvedParameters.reserve(parameters.size());
            for (const auto &param : parameters) {
                param->parent = nullptr;
                resolvedParameters.push_back(resolve(*param));
            }

            CallContext callCtx(*lambdaExprClone, *this, std::move(resolvedParameters));
            auto returned = lambdaFunc->call(callCtx);
            if (returned.expr) {
                std::unique_ptr<Expression> result{returned.expr};
                switch (returned.rc) {
                    case ReturnCode::SUCCESS: {
                        result->accept(*this);
                        break;
                    }
                    default:
                        throw UnimplementedLogic(lambdaExprClone->tokenReference(), ":(");
                }
            }
            return;
        }
        else {
            throw TypeMismatch(
                    expression.tokenReference(),
                    fmt::format("Cannot call {} because it is a variable and not a function", lambdaName.value().string_value));
        }
    }

    AbstractFunction &abstractFunction =
        *std::get<std::unique_ptr<AbstractFunction>>(*function.value());

    ParameterList resolvedParameters;
    resolvedParameters.reserve(parameters.size());

    // Decide wether or not to evaluate/resolve parameters to function call
    if (abstractFunction.evalParams) {
        for (const auto &param : parameters) {
            param->parent = nullptr;
            param->accept(*this);
            resolvedParameters.push_back(std::move(workingStack.top()));
            workingStack.pop();
        }
    } else {
        for (const auto &param : parameters) {
            param->parent = nullptr;
            resolvedParameters.push_back(resolve(*param));
        }
    }
    assert(resolvedParameters.size() == parameters.size());

    FunctionReturn returned;
    CallContext callCtx(lambdaName, *this, std::move(resolvedParameters));

    returned = abstractFunction.call(callCtx);

    if (returned.rc == AUTORUN_LIST) {
        ParameterList list;
        list.reserve(autorun.size());

        for (auto &name : autorun) {
            auto value = std::make_unique<LiteralExpression>(
                Token(STRING, 0, 0, name),
                LiteralExpression::stringToLiteral(name));

            list.push_back(std::move(value));
        }

        workingStack.push(std::make_unique<VectorExpression>(
            Token(VOID, 0, 0, ""), std::move(list)));
        return;
    }

    if (returned.expr) {
        std::unique_ptr<Expression> result{returned.expr};
        switch (returned.rc) {
        case ReturnCode::SUCCESS: {
            if (abstractFunction.evalAfter) {
                result->accept(*this);
                result = std::move(workingStack.top());
                workingStack.pop();
            }
            break;
        }

        case ReturnCode::AUTORUN_ADD:
            autorun.push_back(result->toString());
            return;
        case ReturnCode::AUTORUN_SET: {
            autorun.clear();

            std::unique_ptr<VectorExpression> vector{
                static_cast<VectorExpression *>(result.release())};

            autorun.reserve(vector->size());

            for (size_t i = 0; i < vector->size(); i++) {
                autorun.push_back(vector->get(i).toString());
            }
            return;
        }

        default:
            break;
        }
        workingStack.push(std::move(result));
    }
    // If a function returns null, we just create a null literal
    else {
        workingStack.push(std::make_unique<LiteralExpression>(
            Token(VOID, 0, 0, ""), LiteralValue(0)));
    }
}

void Interpreter::visitLambdaExpression(LambdaExpression &expression) {
    workingStack.push(expression.clone());
}

void Interpreter::visitVectorExpression(VectorExpression &expression) {
    for (size_t i = 0; i < expression.size(); i++) {
        expression.get(i).accept(*this);
        expression.set(i, std::move(workingStack.top()));
        workingStack.pop();
    }

    workingStack.push(
        std::make_unique<VectorExpression>(std::move(expression)));
}

void Interpreter::visitMapExpression(MapExpression &expression) {
    return workingStack.push(expression.clone());
}

void Interpreter::visitUnaryExpression(UnaryExpression &expression) {
    // Eval operand
    expression.operand().accept(*this);
    std::unique_ptr<Expression> operand = std::move(workingStack.top());
    workingStack.pop();
    const Token &op = expression.op();

    switch (op.type) {
    case TokenType::MINUS:
        // We calculate numbers here
        if (operand->expressionType() == EXPR_NUMBER) {
            double value =
                -(reinterpret_cast<LiteralExpression *>(operand.get())
                      ->value()
                      .number_value);
            LiteralValue literalValue;
            literalValue.number_value = value;

            auto negated = std::make_unique<LiteralExpression>(
                Token(NUMBER, op.sourceOffset, 1,
                      std::to_string(literalValue.number_value)),
                literalValue);

            workingStack.push(std::move(negated));
            break;
        }

        // For other types we return a new negate with the evaluated operand
        {
            auto negated =
                std::make_unique<UnaryExpression>(op, std::move(operand));
            workingStack.push(std::move(negated));
        }
        break;

    case TokenType::NOT: {
        // For other types we throw an error
        if (operand->expressionType() != EXPR_BOOLEAN)
            throw TypeMismatch(
                op, "Type Mismatch, because can't NOT a non-boolean value");

        // Get value of boolean literal
        bool value = !(reinterpret_cast<LiteralExpression *>(operand.get())
                           ->value()
                           .boolean_value);
        LiteralValue literalValue;
        literalValue.boolean_value = value;

        // Construct negative literal that is the opposite of value
        auto notted = std::make_unique<LiteralExpression>(
            Token(value ? TokenType::TRUE : TokenType::FALSE, 0, 0, "maybe"),
            literalValue);

        workingStack.push(std::move(notted));
        break;
    }

    case TokenType::SINGLE_QUOTE: {
        auto diffed = std::make_unique<UnaryExpression>(op, std::move(operand));
        workingStack.push(std::move(diffed));
        break;
    }

    default:
        throw UnimplementedLogic(expression.op(),
                                 "Interpreter doesn't handle unary operator");
    }
}

void Interpreter::visitBinaryExpression(BinaryExpression &expression) {
    // Eval left
    expression.left().accept(*this);
    auto l = std::move(workingStack.top());
    workingStack.pop();

    // Eval right
    expression.right().accept(*this);
    auto r = std::move(workingStack.top());
    workingStack.pop();

    const Token &op = expression.op();

    switch (expression.expressionType()) {
    case EXPR_PLUS: {
        if (l->expressionType() == EXPR_NUMBER &&
            r->expressionType() == EXPR_NUMBER) {
            double value =
                static_cast<LiteralExpression *>(l.get())
                    ->value()
                    .number_value +
                static_cast<LiteralExpression *>(r.get())->value().number_value;

            LiteralValue literalValue;
            literalValue.number_value = value;

            workingStack.push(std::make_unique<LiteralExpression>(
                Token(TokenType::NUMBER, op.sourceOffset, 1,
                      std::to_string(literalValue.number_value)),
                literalValue));
            return;
            // String concat
        } else if (l->expressionType() == EXPR_STRING &&
                   r->expressionType() == EXPR_STRING) {
            std::string value = l->toString() + r->toString();

            workingStack.push(std::make_unique<LiteralExpression>(
                Token(TokenType::STRING, op.sourceOffset, value.length(),
                      value),
                value));
            return;
        } else if (l->expressionType() == EXPR_STRING ||
                   r->expressionType() == EXPR_STRING) {
            AstPrinter printer;
            std::string value = printer.print(*l) + printer.print(*r);
            workingStack.push(std::make_unique<LiteralExpression>(
                Token(TokenType::STRING, op.sourceOffset, value.length(),
                      value),
                value));
            return;

            // TODO: str + other types
        } else if (l->expressionType() == EXPR_VECTOR &&
                   r->expressionType() == EXPR_VECTOR) {
            auto lVec = static_cast<VectorExpression *>(l.get());
            auto rVec = static_cast<VectorExpression *>(r.get());

            if (lVec->size() != rVec->size())
                break;

            std::vector<std::unique_ptr<Expression>> values;
            values.reserve(lVec->size());
            for (size_t i = 0; i < lVec->size(); i++) {
                auto bin = std::make_unique<BinaryExpression>(
                    std::unique_ptr<Expression>(lVec->obtain(i)),
                    Token(PLUS, 0, 1, "+"),
                    std::unique_ptr<Expression>(rVec->obtain(i)));
                values.push_back(std::move(bin));
            }
            workingStack.push(std::make_unique<VectorExpression>(
                lVec->tokenReference(), std::move(values)));
            return;
        }
        break;
    }
    case EXPR_MINUS: {
        if (l->expressionType() == EXPR_NUMBER &&
            r->expressionType() == EXPR_NUMBER) {
            double value =
                static_cast<LiteralExpression *>(l.get())
                    ->value()
                    .number_value -
                static_cast<LiteralExpression *>(r.get())->value().number_value;

            LiteralValue literalValue;
            literalValue.number_value = value;

            workingStack.push(std::make_unique<LiteralExpression>(
                Token(TokenType::NUMBER, op.sourceOffset, 1,
                      std::to_string(literalValue.number_value)),
                literalValue));
            return;
        } else if (l->expressionType() == EXPR_VECTOR &&
                   r->expressionType() == EXPR_VECTOR) {
            auto lVec = static_cast<VectorExpression *>(l.get());
            auto rVec = static_cast<VectorExpression *>(r.get());

            if (lVec->size() != rVec->size())
                break;

            std::vector<std::unique_ptr<Expression>> values;
            values.reserve(lVec->size());
            for (size_t i = 0; i < lVec->size(); i++) {
                auto bin = std::make_unique<BinaryExpression>(
                    std::unique_ptr<Expression>(lVec->obtain(i)),
                    Token(MINUS, 0, 1, "-"),
                    std::unique_ptr<Expression>(rVec->obtain(i)));
                values.push_back(std::move(bin));
            }
            workingStack.push(std::make_unique<VectorExpression>(
                lVec->tokenReference(), std::move(values)));
            return;
        }
        break;
    }
    case EXPR_MULTIPLICATION: {
        if (l->expressionType() == EXPR_NUMBER &&
            r->expressionType() == EXPR_NUMBER) {
            double value =
                static_cast<LiteralExpression *>(l.get())
                    ->value()
                    .number_value *
                static_cast<LiteralExpression *>(r.get())->value().number_value;

            LiteralValue literalValue;
            literalValue.number_value = value;

            workingStack.push(std::make_unique<LiteralExpression>(
                Token(TokenType::NUMBER, op.sourceOffset, 1,
                      std::to_string(literalValue.number_value)),
                literalValue));
            return;
        }
        // Scaling lhs with rhs
        else if (l->expressionType() == EXPR_VECTOR &&
                 r->expressionType() != EXPR_VECTOR) {
            // Canonical form must ensure that scalers are moved to rhs, so
            // should never need to deal with this
            assert(false && "Scaler on lhs of vector. Not canonical form");
        }
        // Scaling rhs with lhs
        else if (l->expressionType() != EXPR_VECTOR &&
                 r->expressionType() == EXPR_VECTOR) {
            auto rVec = static_cast<VectorExpression *>(r.get());

            std::vector<std::unique_ptr<Expression>> values;
            values.reserve(rVec->size());

            for (size_t i = 0; i < rVec->size(); i++) {
                values.push_back(std::make_unique<BinaryExpression>(
                    std::unique_ptr<Expression>(rVec->obtain(i)),
                    Token(STAR, 0, 1, "*"), l->clone()));
            }
            workingStack.push(std::make_unique<VectorExpression>(
                rVec->tokenReference(), std::move(values)));
            return;
        }
        break;
    }
    case EXPR_DIVISION: {
        if (l->expressionType() == EXPR_NUMBER &&
            r->expressionType() == EXPR_NUMBER) {
            // TODO: check for division by zero
            double value =
                static_cast<LiteralExpression *>(l.get())
                    ->value()
                    .number_value /
                static_cast<LiteralExpression *>(r.get())->value().number_value;

            LiteralValue literalValue;
            literalValue.number_value = value;

            workingStack.push(std::make_unique<LiteralExpression>(
                Token(TokenType::NUMBER, op.sourceOffset, 1,
                      std::to_string(literalValue.number_value)),
                literalValue));
            return;
        } else if (l->expressionType() == EXPR_VECTOR &&
                   r->expressionType() == EXPR_VECTOR) {
        } else if (l->expressionType() == EXPR_VECTOR) {
            auto lVec = static_cast<VectorExpression *>(l.get());

            std::vector<std::unique_ptr<Expression>> values;
            values.reserve(lVec->size());

            for (size_t i = 0; i < lVec->size(); i++) {
                values.push_back(std::make_unique<BinaryExpression>(
                    std::unique_ptr<Expression>(lVec->obtain(i)),
                    Token(SLASH, 0, 1, "/"), r->clone()));
            }
            workingStack.push(std::make_unique<VectorExpression>(
                lVec->tokenReference(), std::move(values)));
            return;
        }
        break;
    }
    case EXPR_MODULUS: {
        if (l->expressionType() == EXPR_NUMBER &&
            r->expressionType() == EXPR_NUMBER) {
            double value = std::fmod(
                static_cast<LiteralExpression *>(l.get())->value().number_value,
                static_cast<LiteralExpression *>(r.get())
                    ->value()
                    .number_value);

            LiteralValue literalValue;
            literalValue.number_value = value;

            workingStack.push(std::make_unique<LiteralExpression>(
                Token(TokenType::NUMBER, op.sourceOffset, 1,
                      std::to_string(literalValue.number_value)),
                literalValue));
            return;
        }
        break;
    }
    case EXPR_NOT_EQUALS:
    case EXPR_EQUALS: {
        bool inverted = expression.expressionType() == EXPR_NOT_EQUALS;
        bool value = l->equal(*r);
        /* std::cout << "Comparison of " << l->toString() << " == " << r->toString() << " : "<< value << std::endl; */
        if (inverted)
            value = !value;
        workingStack.push(std::make_unique<LiteralExpression>(
            Token(value ? TokenType::TRUE : TokenType::FALSE, op.sourceOffset,
                  value ? 4 : 5, value ? "true" : "false"),
            LiteralValue{.boolean_value = value}));
        return;
    }
    case EXPR_GREATER:
    case EXPR_LESS: {
        if (l->expressionType() == EXPR_NUMBER &&
            r->expressionType() == EXPR_NUMBER) {
            double lLit =
                static_cast<const LiteralExpression &>(*l).value().number_value;
            double rLit =
                static_cast<const LiteralExpression &>(*r).value().number_value;
            bool value = expression.expressionType() == EXPR_GREATER
                             ? lLit > rLit
                             : lLit < rLit;

            workingStack.push(std::make_unique<LiteralExpression>(
                Token(value ? TokenType::TRUE : TokenType::FALSE,
                      op.sourceOffset, value ? 4 : 5, value ? "true" : "false"),
                LiteralValue{.boolean_value = value}));
            return;
        }
        break;
    }
    case EXPR_EXPONENT: {
        if (l->expressionType() == EXPR_NUMBER &&
            r->expressionType() == EXPR_NUMBER) {
            double value = std::pow(
                static_cast<LiteralExpression *>(l.get())->value().number_value,
                static_cast<LiteralExpression *>(r.get())
                    ->value()
                    .number_value);

            LiteralValue literalValue;
            literalValue.number_value = value;

            workingStack.push(std::make_unique<LiteralExpression>(
                Token(TokenType::NUMBER, op.sourceOffset, 1,
                      std::to_string(literalValue.number_value)),
                literalValue));
            return;
        }
        break;
    }
    case EXPR_LOGIC_AND: {
        if (l->expressionType() != EXPR_BOOLEAN ||
            r->expressionType() != EXPR_BOOLEAN) {
            throw TypeMismatch(
                l->tokenClone(),
                "Operand of Logic And not evaluated to boolean value");
        }
        assert(l->expressionType() == EXPR_BOOLEAN);
        assert(r->expressionType() == EXPR_BOOLEAN);
        const auto &lLit = static_cast<const LiteralExpression &>(*l);
        LiteralValue value;
        if (!lLit.value().boolean_value) {
            value.boolean_value = false;
            workingStack.push(std::make_unique<LiteralExpression>(
                Token(FALSE, 0, 5, "false"), value));
            return;
        }
        const auto &rLit = static_cast<const LiteralExpression &>(*r);
        if (!rLit.value().boolean_value) {
            value.boolean_value = false;
            workingStack.push(std::make_unique<LiteralExpression>(
                Token(FALSE, 0, 5, "false"), value));
            return;
        }

        // Both lhs and rhs are truthy
        value.boolean_value = true;
        workingStack.push(std::make_unique<LiteralExpression>(
            Token(TRUE, 0, 4, "true"), value));
        return;
    }
    case EXPR_LOGIC_OR: {
        if (l->expressionType() != EXPR_BOOLEAN ||
            r->expressionType() != EXPR_BOOLEAN) {
            throw TypeMismatch(
                l->tokenClone(),
                "Operand of Logic Or not evaluated to boolean value");
        }
        assert(l->expressionType() == EXPR_BOOLEAN);
        assert(r->expressionType() == EXPR_BOOLEAN);
        const auto &lLit = static_cast<const LiteralExpression &>(*l);
        LiteralValue value;
        if (lLit.value().boolean_value) {
            value.boolean_value = true;
            workingStack.push(std::make_unique<LiteralExpression>(
                Token(TRUE, 0, 4, "true"), value));
            return;
        }
        const auto &rLit = static_cast<const LiteralExpression &>(*r);
        if (rLit.value().boolean_value) {
            value.boolean_value = true;
            workingStack.push(std::make_unique<LiteralExpression>(
                Token(TRUE, 0, 4, "true"), value));
            return;
        }

        // Both lhs and rhs are falsy
        value.boolean_value = false;
        workingStack.push(std::make_unique<LiteralExpression>(
            Token(FALSE, 0, 5, "false"), value));
        return;
    }
    // De-sugar to call diff native proc
    case EXPR_DIFFERENTIATE: {
        const auto &lLit = static_cast<const LiteralExpression &>(*l);
        auto callLit = std::make_unique<LiteralExpression>(Token(STRING,0,4,"diff"), "diff");
        ParameterList args;
        args.push_back(r->clone());
        args.push_back(lLit.clone());
        auto call = std::make_unique<CallExpression>(std::move(callLit), std::move(args));
        call->accept(*this);
        return;
    }

    default:
        break;
    }

    // We cannot evaluate this binary thing, so we just create a new binary
    // thing with the evaluated left and right values :)
    workingStack.push(
        std::make_unique<BinaryExpression>(std::move(l), op, std::move(r)));
}

void Interpreter::addDefinition(const std::string &name, EnvironmentValue value,
                                bool local) {
    if (local) {
    define_local:
        environmentStack.back().addDefinition(name, std::move(value));
    } else {
        bool found_in_higher_scope = false;
        for (int i = environmentStack.size() - 1; i >= 0; i--) {
            auto definition = environmentStack[i].getDefinition(name);
            if (definition != environmentStack[i].definitionEnd()) {
                environmentStack[i].addDefinition(name, std::move(value));
                found_in_higher_scope = true;
                break;
            }
        }
        if (!found_in_higher_scope)
            goto define_local;
    }
}

std::optional<const EnvironmentValue *>
Interpreter::getDefinition(const std::string &name) const {
    assert(environmentStack.size() > 0);

    for (int i = environmentStack.size() - 1; i >= 0; i--) {
        auto definition = environmentStack[i].getDefinition(name);
        if (definition != environmentStack[i].definitionEnd())
            return &definition->second;
    }

    return std::nullopt;
}

void Interpreter::pushScope() {
    if (environmentStack.size() > MAX_STACK_SIZE)
        throw StackOverflow();

    environmentStack.push_back(Environment());
}

void Interpreter::popScope() { environmentStack.pop_back(); }

void Interpreter::resetEnvironment() {
    environmentStack.clear();
    environmentStack.push_back(Environment());
}
