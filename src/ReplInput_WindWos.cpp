#ifndef linux

#include "../include/ReplInput.h"
#include <iostream>
#include <string>

ReplInput::ReplInput(){}
ReplInput::~ReplInput(){}

std::string ReplInput::getInput() {
  std::cout << "> "  << std::endl;
  std::string line;
  std::getline(std::cin, line);
  return line;
}

void ReplInput::beginRawMode(){}
void ReplInput::endRawMode(){}
void ReplInput::historyOlder(){}
void ReplInput::historyNewer(){}
bool ReplInput::match(wchar_t input, wchar_t c, KeyModifier mod){ return false; }
wchar_t ReplInput::readKey(){ return '\0'; }

#endif
