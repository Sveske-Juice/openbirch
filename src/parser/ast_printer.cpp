#include "parser/ast_printer.h"
#include "lexer/token.h"
#include "parser/expression.h"

#include <cstddef>
#include <fmt/core.h>
#include <fmt/ranges.h>
#include <string>

AstPrinter::AstPrinter(int decimals_, bool spacing_)
    : decimals{decimals_}, spacing{spacing_} {}

std::string AstPrinter::print(const Expression &expression) {
    return print(const_cast<Expression &>(expression));
}

std::string AstPrinter::print(Expression &expression) {
    while (workingStack.size() > 0) {
        workingStack.pop();
    }
    expression.accept(*this);
    if (workingStack.empty())
        return "";

    return workingStack.top();
}

void AstPrinter::visitLiteralExpression(LiteralExpression &expression) {
    switch (expression.expressionType()) {
    case EXPR_NULL:
        workingStack.push("NIL");
        break;
    case EXPR_STRING:
        workingStack.push(
            fmt::format("{}", expression.value().string_value));
        break;
    case EXPR_IDENTIFIER:
        workingStack.push(expression.value().string_value);
        break;
    case EXPR_BOOLEAN:
        workingStack.push(expression.value().boolean_value ? "true" : "false");
        break;
    case EXPR_NUMBER:
        if (decimals == -1)
            workingStack.push(
                fmt::format("{}", expression.value().number_value));
        else
            workingStack.push(fmt::format(
                "{:.{}f}", expression.value().number_value, this->decimals));
        break;
    case EXPR_TYPE:
        workingStack.push(TokenTypesToString[expression.value().type_value]);
        break;
    default:
        throw std::logic_error("Illegal literal expression type");
    }
}

void AstPrinter::visitVectorExpression(VectorExpression &expression) {
    std::vector<std::string> values;
    for (size_t i = 0; i < expression.size(); i++) {
        expression.get(i).accept(*this);
        std::string e = workingStack.top();
        workingStack.pop();

        values.push_back(e);
    }
    workingStack.push(fmt::format("[{}]", fmt::join(values, ", ")));
}

void AstPrinter::visitMapExpression(MapExpression &expression) {
    std::string output = "{";
    for (const auto &[key, value] : expression.mapReference()) {
        // Get str of key
        key->accept(*this);
        std::string keyStr = workingStack.top();
        workingStack.pop();

        // Get str of value
        value->accept(*this);
        std::string valueStr = workingStack.top();
        workingStack.pop();

        output += fmt::format("{} : {},", keyStr, valueStr);
    }
    if (!expression.mapReference().empty())
        output.erase(output.size() - 1);
    output += "}";
    workingStack.push(output);
}

void AstPrinter::visitLambdaExpression(LambdaExpression &expression) {
    workingStack.push(fmt::format("({})", expression.toString()));
}

void AstPrinter::visitCallExpression(CallExpression &expression) {
    workingStack.push(fmt::format("{}", expression.toString()));
}

void AstPrinter::visitBinaryExpression(BinaryExpression &expression) {
    expression.left().accept(*this);
    std::string l = std::move(workingStack.top());
    workingStack.pop();

    expression.right().accept(*this);
    std::string r = std::move(workingStack.top());
    workingStack.pop();

    if (expression.expressionType() == EXPR_DIFFERENTIATE) {
        workingStack.push(fmt::format("d/d{} {}", l, r));
        return;
    }

    if (spacing)
        workingStack.push(
            fmt::format("({} {} {})", l, expression.op().literal, r));
    else
        workingStack.push(
            fmt::format("({}{}{})", l, expression.op().literal, r));
}

void AstPrinter::visitUnaryExpression(UnaryExpression &expression) {
    expression.operand().accept(*this);
    std::string o = workingStack.top();
    workingStack.pop();

    switch (expression.op().type) {
    case NOT:
        workingStack.push(fmt::format("{} {}", expression.op().literal, o));
        break;
    case MINUS:
        workingStack.push(fmt::format("{}{}", expression.op().literal, o));
        break;
    default:
        workingStack.push(fmt::format("{}{}", o, expression.op().literal));
        break;
    }
}
