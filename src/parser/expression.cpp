#include "parser/expression.h"
#include "lexer/token.h"

#include <unordered_map>

const char *ExpressionTypeToString[] = {
    "String",
    "Identifier",
    "Number",
    "Boolean",
    "Type",
    "Null",

    "Vector",
    "Map",
    "Lambda",
    "Call",

    "Plus",
    "Minus",
    "Multiplication",
    "Division",
    "Modulus",
    "Equals",
    "Not Equals",
    "Greater",
    "Less",
    "Exponent",
    "Differentiate",
    "And",
    "Or",

    "Negate",
    "Logic Negate",
};

const std::unordered_map<ExpressionType, TokenType> ExpressionTypeToBirchType = {
    {EXPR_NULL, TYPE_NULL},
    {EXPR_NUMBER, TYPE_NUMBER},
    {EXPR_STRING, TYPE_STRING},
    {EXPR_BOOLEAN, TYPE_BOOLEAN},
    {EXPR_CALL, TYPE_FUNCTION},
    {EXPR_LAMBDA, TYPE_FUNCTION},
    {EXPR_VECTOR, TYPE_VECTOR},
    {EXPR_MAP, TYPE_MAP},

    {EXPR_PLUS, TYPE_PLUS},
    {EXPR_MINUS, TYPE_MINUS},
    {EXPR_MULTIPLICATION, TYPE_MULTIPLICATION},
    {EXPR_DIVISION, TYPE_DIVISION},
    {EXPR_MODULUS, TYPE_MODULUS},
    {EXPR_DIFFERENTIATE, TYPE_DIFF},
    {EXPR_EXPONENT, TYPE_EXPONENT},

    {EXPR_NEGATE, TYPE_NEGATE},
};

const std::unordered_set<ExpressionType> binaryTokens = {
    EXPR_PLUS,
    EXPR_MINUS,
    EXPR_MULTIPLICATION,
    EXPR_DIVISION,
    EXPR_DIFFERENTIATE,
    EXPR_MODULUS,
    EXPR_EXPONENT,

    EXPR_LESS,
    EXPR_GREATER,
    EXPR_EQUALS,
    EXPR_NOT_EQUALS,

    EXPR_LOGIC_OR,
    EXPR_LOGIC_AND,
};
