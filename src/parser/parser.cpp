#include "parser/parser.h"
#include "lexer/lexer.h"
#include "lexer/token.h"
#include "parser/expression.h"
#include "parser/parser_errors.h"
#include "parser/statement.h"

#include <cstddef>
#include <initializer_list>
#include <iostream>
#include <memory>

std::vector<std::unique_ptr<Statement>> Parser::parse() {
    std::vector<std::unique_ptr<Statement>> statements;
    while (!isAtEnd()) {
        statements.push_back(command());
    }

    return statements;
}

std::unique_ptr<Statement> Parser::command() {
    switch (peek().type) {
    case IMPORT:
        consume();
        return importStatement();

    default: // Not a command, fall down to statement
        break;
    }

    return statement();
}

std::unique_ptr<Statement> Parser::importStatement() {
    Token import_token = previous();
    std::unique_ptr<Expression> expr = expression();

    if (!matchAny({TokenType::SEMICOLON}) && !isAtEnd())
        throw MissingTerminator(previous());

    return std::make_unique<ImportStatement>(import_token, std::move(expr));
}

std::unique_ptr<Statement> Parser::statement() {
    if (matchAny({PROCEDURE}))
        return procedureStatement();
    else if (matchAny({IF}))
        return ifStatement();
    else if (matchAny({RETURN}))
        return returnStatement();

    auto expr = expression();

    if (peek().type == TokenType::COLON_EQUAL)
        return declarationStatement(std::move(expr));
    else
        return expressionStatement(std::move(expr));
}

std::unique_ptr<Statement>
Parser::declarationStatement(std::unique_ptr<Expression> precalculated) {
    std::unique_ptr<Expression> expr =
        precalculated == nullptr ? expression() : std::move(precalculated);

    if (!matchAny({COLON_EQUAL}))
        throw UnexpectedToken(peek());

    std::string name;
    std::unique_ptr<Expression> value;

    switch (expr->expressionType()) {
    case EXPR_CALL: {
        CallExpression &callExpr = static_cast<CallExpression &>(*expr);

        name = callExpr.lambdaName().value().string_value;

        auto body = expression();

        std::vector<std::string> boundVariables;
        boundVariables.reserve(callExpr.parameters().size());
        for (size_t i = 0; i < callExpr.parameters().size(); i++) {
            boundVariables.push_back(callExpr.parameters()[i]->toString());
        }
        value =
            std::make_unique<LambdaExpression>(boundVariables, std::move(body));
        break;
    }
    case EXPR_IDENTIFIER: {
        auto &literalExpr = *static_cast<LiteralExpression *>(expr.get());
        name = literalExpr.toString();
        value = expression();
        break;
    }
    default:
        throw SyntaxError(previous(), "Cannot declare a variable with rhs of " +
                                          expr->toString());
    }

    if (!matchAny({TokenType::SEMICOLON}) && !isAtEnd()) {
        throw MissingTerminator(previous());
    }

    return std::make_unique<DeclarationStatement>(name, std::move(value));
}

std::unique_ptr<Statement>
Parser::expressionStatement(std::unique_ptr<Expression> precalculated) {
    std::unique_ptr<Expression> expr =
        precalculated == nullptr ? expression() : std::move(precalculated);

    if (!matchAny({TokenType::SEMICOLON}) && !isAtEnd()) {
        throw MissingTerminator(previous());
    }

    return std::make_unique<ExpressionStatement>(std::move(expr));
}

std::unique_ptr<Statement> Parser::procedureStatement() {
    Token token = previous();

    if (!matchAny({IDENTIFIER}))
        throw SyntaxError(peek(), "Expected an identifier here");
    Token name = previous();

    if (!matchAny({LPAREN}))
        throw SyntaxError(peek(), "Expected parenthesis here");

    std::vector<Token> arguments;
    if (matchAny({IDENTIFIER})) {
        arguments.push_back(previous());
        while (matchAny({TokenType::COMMA})) {
            if (!matchAny({IDENTIFIER}))
                throw SyntaxError(peek(), "Expected identifier here");

            arguments.push_back(previous());
        }
    }

    if (!matchAny({RPAREN}))
        throw SyntaxError(peek(), "Expected parenthesis here");

    CodeBlock body;
    while (!matchAny({END})) {
        body.push_back(statement());
        if (peek().type == END_OF_FILE)
            throw SyntaxError(token, "Unterminated procedure, expected \"end\" "
                                     "to mark end of procedure");
    }

    // Optional ; after end
    matchAny({SEMICOLON});

    return std::make_unique<ProcedureStatement>(token, name, arguments,
                                                std::move(body));
}

std::unique_ptr<Statement> Parser::ifStatement() {
    Token ifToken = previous();
    if (!matchAny({LPAREN}))
        throw SyntaxError(peek(), "Expected parenthesis after if statement");
    auto condition = expression();
    if (!matchAny({RPAREN}))
        throw SyntaxError(previous(), "If statement missing closing bracket");

    CodeBlock ifBlock;
    std::vector<ConditionalBlock> elifConditionalBlocks;
    CodeBlock elseBlock;

    while (!matchAny({ELSE_IF, ELSE, END})) {
        ifBlock.push_back(statement());
        if (peek().type == END_OF_FILE)
            throw SyntaxError(ifToken,
                              "Unterminated if statement, expected \"end\" "
                              "to mark end of statement");
    }
    if (previous().type == ELSE_IF) {
        while (true) {
            if (!matchAny({LPAREN}))
                throw SyntaxError(peek(),
                                  "Expected parenthesis after if statement");
            auto elifCondition = expression();
            if (!matchAny({RPAREN}))
                throw SyntaxError(previous(),
                                  "If statement missing closing bracket");

            CodeBlock elifBlock;
            while (!matchAny({ELSE_IF, ELSE, END})) {
                elifBlock.push_back(statement());
                if (peek().type == END_OF_FILE)
                    throw SyntaxError(
                        ifToken, "Unterminated if statement, expected \"end\" "
                                 "to mark end of statement");
            }
            elifConditionalBlocks.push_back(ConditionalBlock(
                std::move(elifCondition), std::move(elifBlock)));
            if (previous().type == END || previous().type == ELSE)
                break;
            assert(previous().type == ELSE_IF);
        }
    }
    if (previous().type == ELSE) {
        while (!matchAny({END})) {
            elseBlock.push_back(statement());
            if (peek().type == END_OF_FILE)
                throw SyntaxError(ifToken,
                                  "Unterminated if statement, expected \"end\" "
                                  "to mark end of statement");
        }
    }
    return std::make_unique<IfStatement>(
        ifToken, ConditionalBlock(std::move(condition), std::move(ifBlock)),
        std::move(elifConditionalBlocks), std::move(elseBlock));
}

std::unique_ptr<Statement> Parser::returnStatement() {
    Token token = previous();
    auto value = expression();

    if (!matchAny({TokenType::SEMICOLON}) && !isAtEnd()) {
        throw MissingTerminator(previous());
    }

    return std::make_unique<ReturnStatement>(token, std::move(value));
}

std::unique_ptr<Expression> Parser::expression() { return logic_or(); }

std::unique_ptr<Expression> Parser::logic_or() {
    auto expr = logic_and();
    while (matchAny({TokenType::OR})) {
        Token op = previous();
        auto right = logic_and();
        expr = std::make_unique<BinaryExpression>(std::move(expr), op, std::move(right));
    }

    return expr;
}

std::unique_ptr<Expression> Parser::logic_and() {
    auto expr = equality();
    while (matchAny({TokenType::AND})) {
        Token op = previous();
        auto right = equality();
        expr = std::make_unique<BinaryExpression>(std::move(expr), op, std::move(right));
    }

    return expr;
}

std::unique_ptr<Expression> Parser::equality() {
    std::unique_ptr<Expression> expression = comparison();
    while (matchAny({TokenType::BANG_EQUALS, TokenType::EQUALS})) {
        Token equialityOp = previous();
        std::unique_ptr<Expression> right = comparison();
        std::unique_ptr<Expression> binOp = std::make_unique<BinaryExpression>(
            std::move(expression), equialityOp, std::move(right));
        expression = std::move(binOp);
    }
    return expression;
}
std::unique_ptr<Expression> Parser::comparison() {
    std::unique_ptr<Expression> expression = term();
    while (matchAny({TokenType::GREATER, TokenType::GREATER_EQUALS,
                     TokenType::LESS, TokenType::LESS_EQUALS})) {
        Token comparisonOp = previous();
        std::unique_ptr<Expression> right = term();
        std::unique_ptr<Expression> binOp = std::make_unique<BinaryExpression>(
            std::move(expression), comparisonOp, std::move(right));
        expression = std::move(binOp);
    }
    return expression;
}
std::unique_ptr<Expression> Parser::term() {
    std::unique_ptr<Expression> expression = factor();
    while (matchAny({TokenType::PLUS, TokenType::MINUS})) {
        Token termOp = previous();
        std::unique_ptr<Expression> right = factor();
        std::unique_ptr<Expression> binOp = std::make_unique<BinaryExpression>(
            std::move(expression), termOp, std::move(right));
        expression = std::move(binOp);
    }
    return expression;
}
std::unique_ptr<Expression> Parser::factor() {
    std::unique_ptr<Expression> expression = unary();
    while (matchAny({TokenType::SLASH, TokenType::STAR})) {
        Token factorOp = previous();
        std::unique_ptr<Expression> right = unary();
        std::unique_ptr<Expression> binOp = std::make_unique<BinaryExpression>(
            std::move(expression), factorOp, std::move(right));
        expression = std::move(binOp);
    }
    return expression;
}

std::unique_ptr<Expression> Parser::unary() {
    if (matchAny({TokenType::NOT, TokenType::MINUS})) {
        Token unaryOp = previous();
        std::unique_ptr<Expression> operand = unary();
        return std::make_unique<UnaryExpression>(unaryOp, std::move(operand));
    }
    return exponent();
}

std::unique_ptr<Expression> Parser::exponent() {
    std::unique_ptr<Expression> expr = differentiation();

    auto seq = {
        TokenType::EXPONENT, // ^
    };

    if (matchAny(seq)) {
        Token op = previous();
        std::unique_ptr<Expression> right = unary();
        std::unique_ptr<Expression> binOp = std::make_unique<BinaryExpression>(
            std::move(expr), op, std::move(right));
        expr = std::move(binOp);
    }

    return expr;
}

// TODO: support for * between d and variable with respect to as in BNF
std::unique_ptr<Expression> Parser::differentiation() {
    if (peek().type != IDENTIFIER)
        return call();

    Token firstIdentifier = peek();
    if (firstIdentifier.literal != "d")
        return call();

    if (peek(1).type != SLASH)
        return call();

    if (peek(2).type != IDENTIFIER)
        return call();

    Token secondIdentifier = peek(2);
    if (secondIdentifier.literal.length() < 2)
        return call();

    if (!(secondIdentifier.literal[0] == 'd'))
        return call();

    matchAny({IDENTIFIER});
    matchAny({SLASH});
    matchAny({IDENTIFIER});
    auto expr = expression();
    std::string variableWithRespectTo = secondIdentifier.literal.substr(1);

    Token diffOp(D_OVER_D_IDENTIFER, secondIdentifier.sourceOffset,
                 3 + variableWithRespectTo.length(),
                 "d/d" + variableWithRespectTo);
    return std::make_unique<BinaryExpression>(
        std::make_unique<LiteralExpression>(
            Token(IDENTIFIER, secondIdentifier.sourceOffset, 1,
                  variableWithRespectTo),
            variableWithRespectTo),
        diffOp, std::move(expr));
}

std::unique_ptr<Expression> Parser::call() {
    auto name = primary();

    if (!matchAny({TokenType::LPAREN}))
        return name;

    std::vector<std::unique_ptr<Expression>> parameterList;

    // Allow 0 argument functions
    if (peek().type != RPAREN) {
        do {
            parameterList.push_back(expression());
        } while (matchAny({TokenType::COMMA}));
    }

    if (!matchAny({TokenType::RPAREN}))
        throw MissingClosingBracket(previous().sourceOffset,
                                    previous().length);

    if (name->expressionType() != EXPR_IDENTIFIER)
        throw SyntaxError(
            previous(),
            "Can only call functions, not constants and strings etc.");

    // Swap from abstract to literal expr
    assert(name->expressionType() == EXPR_IDENTIFIER);
    LiteralExpression *lamdaName =
        static_cast<LiteralExpression *>(name.release());

    return std::make_unique<CallExpression>(
        std::unique_ptr<LiteralExpression>(lamdaName),
        std::move(parameterList));
}

std::unique_ptr<Expression> Parser::primary() {
    if (matchAny({TokenType::STRING})) {
        Token tokenLiteral = previous();
        return std::make_unique<LiteralExpression>(tokenLiteral,
                                                   tokenLiteral.literal);
    }

    if (matchAny({TokenType::NUMBER})) {
        Token tokenLiteral = previous();
        LiteralValue value;
        value.number_value = std::stod(tokenLiteral.literal);
        return std::make_unique<LiteralExpression>(tokenLiteral, value);
    }

    if (matchAny({TokenType::LPAREN})) {
        std::unique_ptr<Expression> expr;
        if (peek().type == IDENTIFIER &&
            (peek(1).type == COMMA || peek(1).type == ARROW)) {
            std::vector<std::string> bound_variables;
            do {
                if (!matchAny({IDENTIFIER}))
                    throw SyntaxError(peek(), "Expected IDENTIFIER here");

                bound_variables.push_back(previous().literal);

            } while (matchAny({COMMA}));

            if (!matchAny({ARROW}))
                throw SyntaxError(peek(), "Expected ARROW here");

            std::unique_ptr<Expression> body = expression();

            expr = std::make_unique<LambdaExpression>(bound_variables,
                                                      std::move(body));

        } else
            expr = expression();

        uint16_t length{0};

        // Verify it wasn't a EOF token
        if (!matchAny({TokenType::RPAREN})) {
            throw MissingClosingBracket(peek().sourceOffset, length);
        }
        return expr;
    }

    if (matchAny({TokenType::LSQUARE})) {
        if (peek().type ==
            TokenType::RSQUARE) { // Empty vector initialization
            consume();
            // return std::make_unique<VectorExpression>(previous(), {});
            return std::make_unique<VectorExpression>(
                previous(), std::vector<std::unique_ptr<Expression>>());
        }

        Token start_token = previous();
        std::vector<std::unique_ptr<Expression>> values;
        do {
            values.push_back(expression());
        } while (matchAny({TokenType::COMMA}));

        Token token = peek();
        if (token.type != TokenType::RSQUARE) {
            throw MissingClosingBracket(peek().sourceOffset, token.length);
        }

        consume();
        return std::make_unique<VectorExpression>(previous(),
                                                  std::move(values));
    }

    if (matchAny({TokenType::LCURLY})) {
        Token lCurly = previous();
        HashMap hashMap;

        // Empty hashmap
        if (peek().type == RCURLY) {
            consume();
            return std::make_unique<MapExpression>(lCurly, std::move(hashMap));
        }

        do {
            auto key = expression();
            if (!matchAny({TokenType::COLON_EQUAL}))
                throw SyntaxError(key->tokenReference(), "Expected a ':=' after key in hashmap");
            auto value = expression();

            hashMap[std::move(key)] = std::move(value);
        } while (matchAny({TokenType::COMMA}));

        Token token = peek();
        if (token.type != TokenType::RCURLY) {
            throw MissingClosingBracket(peek().sourceOffset, token.length);
        }

        consume();
        return std::make_unique<MapExpression>(lCurly, std::move(hashMap));
    }

    if (matchAny({TokenType::TRUE, TokenType::FALSE})) {
        Token tokenLiteral = previous();
        LiteralValue value;
        value.boolean_value = tokenLiteral.type == TokenType::TRUE;

        return std::make_unique<LiteralExpression>(tokenLiteral, value);
    }

    if (matchAny({TokenType::IDENTIFIER})) {
        Token tokenIdentifier = previous();
        return std::make_unique<LiteralExpression>(tokenIdentifier,
                                                   tokenIdentifier.literal);
    }

    if (matchAny(typeTokens.cbegin(), typeTokens.cend())) {
        Token token = previous();
        return std::make_unique<LiteralExpression>(token, LiteralValue{.type_value=token.type});
    }

    throw UnexpectedToken(peek());
}

bool Parser::matchAny(std::initializer_list<TokenType> tokenTypes) {
    for (TokenType type : tokenTypes) {
        if (!isAtEnd() && peek().type == type) {
            consume();
            return true;
        }
    }
    return false;
}

template<typename Iterator>
bool Parser::matchAny(Iterator begin, Iterator end) {
    for (auto it = begin; it != end; it++) {
        if (!isAtEnd() && peek().type == *it) {
            consume();
            return true;
        }
    }
    return false;
}

inline void Parser::consume() { this->currentIndex++; }

inline bool Parser::isAtEnd() const {
    return lexemes[currentIndex].type == TokenType::END_OF_FILE;
}

inline Token Parser::previous() const {
    assert(currentIndex > 0 && currentIndex <= lexemes.size());
    return lexemes[currentIndex - 1];
}

inline Token Parser::peek() const {
    assert(currentIndex < lexemes.size());
    return lexemes[currentIndex];
}
inline Token Parser::peek(size_t amount) const {
    size_t idx = currentIndex + amount;

    // Return EOF if out of bounds
    if (idx >= lexemes.size())
        return lexemes.back();

    return lexemes[idx];
}

inline bool Parser::isOfType(TokenType typeToCheck,
                             std::initializer_list<TokenType> types) const {
    for (TokenType type : types) {
        if (typeToCheck == type)
            return true;
    }

    return false;
}
