#include "lexer/token.h"

const std::string TokenTypesToString[] = {
    "String", // String
    "Number", // Number

    "Identifier", // Identifier
    "Import",     // Import

    "Plus",     // Plus
    "Minus",    // Minus
    "Star",     // Star
    "Slash",    // Slash
    "Modulus",  // Modulus
    "Exponent", // Exponent
    "Leibniz Differentiate", // d/dx diff notation

    "Left Parenthesis",     // Left Parenthesis
    "Right Parenthesis",    // Right Parenthesis
    "Left Square bracket",  // Left Square bracket
    "Right Square bracket", // Right Square bracket
    "Left Curly bracket",
    "Right Curly bracket",

    "Equals",         // Equals
    "Assign",         // Assign
    "Less",           // Less
    "Greater",        // Greater
    "Equal Equals",   // Equal Equals
    "Less Equals",    // Less Equals
    "Greater Equals", // Greater Equals
    "Not Equals",     // Not Equals
    "Arrow",          // Arrow
    "Single Quote",   // Single Quote

    "Colon",     // Colon
    "Semicolon", // Semicolon
    "Comma",     // Comma
    "Dot",       // Dot
    "Bang",      // Bang
    "Ampersand", // Ampersand

    "Not",       // Not
    "And",       // And
    "Or",        // Or
    "If",        // If
    "Else If",   // Else If
    "Else",      // Else
    "Then",      // Then
    "While",     // While
    "For",       // For
    "True",      // True
    "False",     // False
    "Null",      // Null
    "End",

    "<NULL TYPE>",
    "<NUMBER TYPE>",
    "<STRING TYPE>",
    "<BOOLEAN TYPE>",
    "<IDENTIFER TYPE>",
    "<FUNCTION TYPE>",
    "<VECTOR TYPE>",
    "<MAP TYPE>",

    "<PLUS TYPE>",
    "<MINUS TYPE>",
    "<MULTIPLICATION TYPE>",
    "<DIVISION TYPE>",
    "<MODOLUS TYPE>",
    "<EXPONENT TYPE>",
    "<DIFF TYPE>",
    "<NEGATE TYPE>",

    "Procedure",
    "Return",
    "local",

    "End Of File" // End Of File
};
