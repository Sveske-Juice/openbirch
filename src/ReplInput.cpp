#ifdef linux

#include "../include/ReplInput.h"
#include "ansii.h"
#include <cctype>
#include <cstdio>
#include <stdexcept>
#include <string>
#include <unistd.h>

// constructor
ReplInput::ReplInput() { setlocale(LC_ALL, ""); }

// deconstructor
ReplInput::~ReplInput() {}

std::string ReplInput::getInput() {
  beginRawMode();

  currentText = "";
  cursorPos = 0;

  dprintf(STDOUT_FILENO, "%s>%s %s", ANSII_DIM, ANSII_DIM_RESET, currentText.c_str());

  // get input;

  while (1) {
    wchar_t c = readKey();

    if (c == 10) {
      break;
    }

    if (c == 127 && !currentText.empty()) { // Backspace
      if (cursorPos > 0) {
        currentText.erase(cursorPos - 1, 1);
        cursorPos--;
      }

    } else if (c == 126 && !currentText.empty()) { // Delete
      currentText.erase(cursorPos, 1);

      if (cursorPos > 0 && cursorPos > currentText.size())
        cursorPos--;
    } else if (c > -1) {
      if (cursorPos > 100000000) {
        cursorPos = 0;
      }

      if (std::isprint(c)) {
        currentText.insert(cursorPos, (char *)&c, sizeof(char));
        cursorPos++;
      }
    } else {
      switch (c) {
      case -1102: // right arrow
        if (cursorPos < currentText.size())
          cursorPos++;
        break;
      case -1101: // left arrow
        if (cursorPos > 0)
          cursorPos--;
        break;
      case -1104: // up arrow
          historyOlder();
          break;
      case -1103: // down arrow
          historyNewer();
          break;
      }
    }

    write(STDOUT_FILENO, "\33[2K\r", 5); // Clear line
    // write(STDOUT_FILENO, currentText.c_str(), currentText.size());
    dprintf(STDOUT_FILENO, "%s>%s %s", ANSII_DIM, ANSII_DIM_RESET, currentText.c_str());

    dprintf(STDOUT_FILENO, "\33[1C");

    // move cursor back to where it belongs
    dprintf(STDOUT_FILENO, "\33[%zuD", (currentText.length() - cursorPos) + 1);
  }

  printf("\n");

  history_index = 0;

  history.push_back(currentText);

  return currentText;

  endRawMode();
}

void ReplInput::historyNewer() {
  if (history.empty())
    return;

  if (history_index == 0) {
    currentTextBeforeHistory = currentText;
  }

  history_index = (history_index + 1 ) % (history.size() + 1);

  if (history_index == 0)
    currentText = currentTextBeforeHistory;
  else
    currentText = history.at(history_index-1);

  cursorPos = currentText.size();
}

void ReplInput::historyOlder() {
  if (history.empty())
    return;

  if (history_index == 0) {
    currentTextBeforeHistory = currentText;
    history_index = history.size()+1;
  }

  history_index--;

  if (history_index == 0)
    currentText = currentTextBeforeHistory;
  else
    currentText = history.at(history_index-1);

  cursorPos = currentText.size();
}

bool ReplInput::match(wchar_t input, wchar_t c, KeyModifier mod) {
  switch (mod) {
  case NOMODIFIER:
    return input == c;
  case CONTROL:
    return input == (c & 0x1b);
  default:
    throw std::runtime_error("Not implemented");
  }
}

void ReplInput::beginRawMode() {
  tcgetattr(0, &old);      /* grab old terminal i/o settings */
  new1 = old;              /* make new settings same as old settings */
  new1.c_lflag &= ~ICANON; /* disable buffered i/o */
  new1.c_lflag &= ~ECHO;   /* set echo mode */

  // Makes read() non-blocking by returning '\0' when nothing is pressed
  new1.c_cc[VMIN] = 0;
  new1.c_cc[VTIME] = 1;

  tcsetattr(0, TCSANOW, &new1); /* use these new terminal i/o settings now */

  // Create screen (clear everything but it can be brought back)
  // write(STDOUT_FILENO, "\x1b[?1049h", 8);
}

void ReplInput::endRawMode() {
  tcsetattr(0, TCSANOW, &old);
  // Restore screen
  // write(STDOUT_FILENO, "\x1b[?1049l", 8);
}

wchar_t ReplInput::readKey() {
  int nread;
  char c;

  // Read bytes for character
  while ((nread = read(STDIN_FILENO, &c, 1)) != 1) {
    if (nread == -1 && errno != EAGAIN)
      exit(1);
  }

  // printf("input: %d\n", c);

  // If the character is '\x1b' (27) its an escape sequence
  if (c == '\x1b') {
    char seq[5];

    // Try to read next byte into seq
    if (read(STDIN_FILENO, &seq[0], 1) != 1)
      // Read was empty, so no more bytes other than '\x1b' (27;esc)
      return '\x1b';

    // Try to read 3rd byte
    if (read(STDIN_FILENO, &seq[1], 1) != 1)
      // Result was empty, no control sequence is this short, (^[)
      // return '\x1b' (27;esc)
      return 0;

    if (seq[1] == '1') {
      if (read(STDIN_FILENO, &seq[2], 3) != 3)
        // Result was empty, no control sequence is this short, (^[)
        // return '\x1b' (27;esc)
        return '\x1b';
    }

    if (seq[0] == '[') {
      // Switch control sequence
      if (seq[1] == '1') {
        switch (seq[4]) {
        case 'A':       // ^[[A (Up arrow)
          return -1204; // Remap to k

        case 'B':       // ^[[B (Down arrow)
          return -1203; // Remap to j

        case 'C':       // ^[[C (Right arrow)
          return -1202; // Remap to l

        case 'D':       // ^[[D (Left arrow)
          return -1201; // Remap to h
        }
      } else {
        switch (seq[1]) {
        case 'A':       // ^[[A (Up arrow)
          return -1104; // Remap to k

        case 'B':       // ^[[B (Down arrow)
          return -1103; // Remap to j

        case 'C':       // ^[[C (Right arrow)
          return -1102; // Remap to l

        case 'D':       // ^[[D (Left arrow)
          return -1101; // Remap to h
        }
      }
    }

    return 0;

  } else
    return c;
}

#endif
