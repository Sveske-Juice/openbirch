#include "lexer/lexer.h"
#include "ansii.h"
#include "lexer/lexer_errors.h"
#include "lexer/token.h"
#include "parser/ast_printer.h"
#include "parser/parser.h"
#include "parser/parser_errors.h"
#include "runtime/expressionrewriter.h"
#include "runtime/interpreter.h"
#include "runtime/runtime_errors.h"
#include "parser/ast_printer.h"
#include "include/ReplInput.h"
#include "include/WasmInterface.h"

#include <fstream>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>

#define OUTPUT_PADDING ANSII_FOREGROUND_CYAN << ANSII_ITALIC << "\t# "

void execute_file(std::string);

void printResult(const Expression &result) {
  AstPrinter printer(-1, true);
  auto str = printer.print(result);
  if (str.empty())
      return;

  std::cout << OUTPUT_PADDING << str << ANSII_FOREGROUND_DEFAULT
            << ANSII_ITALIC_RESET << std::endl;
}

void print_error(int charNum, int length, std::string what, std::string error_type) {
  for (int i = 0; i < charNum; i++) {
    printf(" ");
  }

  printf("  %s", ANSII_FOREGROUND_YELLOW);

  for (int i = 0; i < length; i++) {
    printf("^"); 
  }

  printf("\n%s%s%s: %s\n", ANSII_FOREGROUND_RED,
         error_type.c_str(), ANSII_RESET, what.c_str());
}


int main(int argc, char *argv[]) {
  // Repl mode
  if (argc < 2) {
    Interpreter interpreter;
    ReplInput replInput;
    // std::cout << "\x1b[?1049h";
    while (true) {
      std::string input = replInput.getInput();

      if (input == "quit" || input == "q" || input == ":q" || input == "exit")
        break;

      try {
        // Lexing
        Lexer lexer{input};
        auto tokens = lexer.tokenize();

        // Parsing
        Parser parser{tokens};
        auto statements = parser.parse();

        // Interpret
        auto result = interpreter.interpret(std::move(statements));
        if (result.has_value())
          printResult(*result->get());
      } catch (const LexerException &le) {
        std::string_view sv{input.begin(), input.end()};
        print_error(le.sourceOffset_, le.length_, le.what(sv), "Lexer Error");
      } catch (const ParserException &pe) {
        std::string_view sv{input.begin(), input.end()};
        print_error(pe.sourceOffset_, pe.length_, pe.what(sv), "Parser Error");
      } catch (const RuntimeException &re) {
        std::string_view sv{input.begin(), input.end()};
        print_error(re.sourceOffset_, re.length_, re.what(sv), "Runtime Error");
      }
    }

    // std::cout << "\x1b[?1049l";

    return 0;
  }

  execute_file(argv[1]);

  return 0;
}

void execute_file(std::string path) {
  std::fstream ifs(path);
  // Read contents to string
  std::string content((std::istreambuf_iterator<char>(ifs)),
                      (std::istreambuf_iterator<char>()));

  // Lex and parse
  try {
    Lexer lexer(content);
    auto tokens = lexer.tokenize();
    Parser parser(tokens);
    std::vector<std::unique_ptr<Statement>> statements = parser.parse();

    Interpreter interpreter(std::move(statements));
    interpreter.interpret();
  } catch (const LexerException &le) {
    std::cerr << "Lexer Error: " << le.what(content) << std::endl;
  } catch (const ParserException &pe) {
    std::cerr << "Parser Error: " << pe.what(content) << std::endl;
  }
  catch (const RuntimeException &re) {
    std::cerr << "Runtime Error: " << re.what(content) << std::endl;
  }
}
