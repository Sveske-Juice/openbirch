set(CMAKE_SYSTEM_NAME Linux)
set(EMSCRIPTEN 1)
set(CMAKE_SYSTEM_VERSION 1)

# Specify the Emscripten compilers
set(CMAKE_C_COMPILER emcc)
set(CMAKE_CXX_COMPILER em++)

# Specify the archiver and ranlib
set(CMAKE_AR emar)
set(CMAKE_RANLIB emranlib)

# Set the linker
set(CMAKE_LINKER emcc)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

# Ensure CMake uses these tools
set(CMAKE_C_COMPILER_WORKS 1)
set(CMAKE_CXX_COMPILER_WORKS 1)

# Override the build system tools
set(CMAKE_CXX_COMPILER_AR emar)
set(CMAKE_CXX_COMPILER_RANLIB emranlib)
set(CMAKE_C_COMPILER_AR emar)
set(CMAKE_C_COMPILER_RANLIB emranlib)
