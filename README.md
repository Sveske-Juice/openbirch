## Features
- [ ] Algebra
  - [x] Functions
  - [x] Variables
  - [x] Evaluate expressions w/o variables
  - [ ] Isolate variables
  - [ ] Solve for unkown variable
  - [ ] Solve systems of equations
  - [x] Vectors
  - [ ] Matrices
  - [ ] Tensors

- [ ] Calculus
  - [ ] Single variable derivative
  - [ ] Partial derivatives
  - [ ] Vector derivatives
  - [ ] Ordinary differential equations (ODEs)
  - [ ] Partial differential equations (PDEs)
  - [ ] Integration
  - [ ] Numerical Intergration
  - [ ] Limits

- [ ] Other
  - [ ] Replacements in expressions
    - [ ] Replace built in function
    - [ ] Hashmaps for MatchMap
  - [x] Builtin/native functions (Interface to C++)
  - [ ] Check equality of expressions (canonical/normal form)
  - [ ] Complex numbers
  - [ ] Units
  - [x] Procedures
    - [x] Scopes
    - [x] Control flow
    - [x] Return statements

## Build
`make` to build main `OpenBirch` executable

`make tests` for unit tests

`make bench` for unit tests and benchmarks

`make docs` for Doxygen documentation

## BNF of language
See: https://en.wikipedia.org/wiki/Extended_Backus%E2%80%93Naur_form
```ebnf
<program>                     ::= <command> { <command> } EOF

<command>                     ::= ( <showCommand> | <applyCommand> | <defineCommand> | <statement> ) ";"
<showCommand>                 ::= "show" <expression>
<applyCommand>                ::= "apply" <identifier> {"," <identifier>} ":" <expression> 
<defineCommand>               ::= "define" [ "repeating" ] <identifier> ":" <expression> "->" <expression> [ "where" <constraint> [ "," <constraint> ] ] [ "evaluate" ] 
<defineSupersetCommand>       ::= "define" [ "repeating" ] <identifier> "->" <identifier> [ "," <identifier> ] [ "evaluate" ] 
<constraint>                  ::= <identifier> ":" ( "constant" | "variable" | "name" | "function" | "vector" )
 
<statement>                   ::= <declarationStatement> | <expressionStatement> | <procedureStatement> | <ifStatement>
<declarationStatement>        ::= <expression> ":=" <expression>
<expressionStatement>         ::= <expression>

<statementWithReturn>         ::= <returnStatement> | <statement>
<procedureStatement>          ::= "procedure" <identifier> "(" [ <identifier> ] { "," <identifier> } ")" { <statementWithReturn> } "end"
<returnStatement>             ::= "return" <expression>

<ifStatement>                 ::= "if" "(" <expression> ")" {statement}
                                  { "elif" "(" <expression> ")" {statement} }
                                  [ "else" {statement} ] "end"

<expression>                  ::= <logic_or>
<logic_or>                    ::= <logic_and> { "or" <logic_and> }
<logic_and>                   ::= <equality> { "and" <equality> }
<equality>                    ::= ( <comparison> ( "!=" | "==" ) <comparison> ) | <comparison>
<comparison>                  ::= ( <term> ( ">" | ">=" | "<" | "<=" ) <term> ) | <term>
<term>                        ::= ( <factor> ( "+" | "-" ) <factor> ) | <factor>
<factor>                      ::= ( <unary> ( "*" | "/" ) <unary> ) | <unary>
<unary>                       ::= ( [ "~" | "-" ] <unary> ) | <differentiation>
<exponent>                    ::= ( <differentiation> "^" <differentiation> ) | <differentiation>
<differentiation>             ::= ( "d" "/" "d" [ "*" ] <identifier> <expression> ) | <call>
<call>                        ::= ( <identifier> "(" [ <expression { "," <expression> } ] ")" ) | <lambda>
<lambda>                      ::= "(" <identifier> "->" <expression> | <primary> ")"
<primary>                     ::= <number> | <string> | <identifier> | false | true | <vector> | <parenthesis> | <lambda>
<parenthesis>                 ::= "(" <expression> ")"
<vector>                      ::= "[" [ <expression> { "," <expression> } ] "]"

<identifier>                  ::= ( "a"-"z" | "A"-"Z" | "_" ) { "a"-"z" | "A"-"Z" | "_" | "0"-"9" }
<number>                      ::= ( "0"-"9" )* [ "." { "1"-"9" } ]
<string>                      ::= """.*"""
```

## Example
See [example](example.obd)

## Resources
* https://craftinginterpreters.com/contents.html
* [Term Rewriting and All That](https://zubairabid.com/Semester7/subjects/PoPL/books/TRaAT.pdf)
* http://rewriting.loria.fr/
* https://en.wikipedia.org/wiki/Rewriting
* https://en.m.wikipedia.org/wiki/Lambda_calculus
* https://wwwbroy.in.tum.de/publ/papers/TUM-I9508.pdf
* https://who.rocq.inria.fr/Alejandro.Diaz-Caro/TheVectorialCalculus.pdf
* https://digital.wpi.edu/pdfviewer/4m90dw00r
* https://en.wikipedia.org/wiki/Binomial_theorem

## Cross compile

### From (Arch) Linux to Windows
Install MinGW
`mingw-w64-binutils mingw-w64-cmake mingw-w64-gcc`

#### Build static libraries to mingw environment
**fmt:**
```
git clone https://github.com/fmtlib/fmt.git
cd fmt
mkdir build/
cd build/
cmake .. -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=OFF -DCMAKE_INSTALL_PREFIX=/usr/x86_64-w64-mingw32
make
sudo make install
```
**GNU gmp ([based on](https://stackoverflow.com/questions/65471795/compiling-gmp-library-with-mingw-on-linux)):**
```
wget https://gmplib.org/download/gmp/gmp-6.3.0.tar.xz
tar xf gmp-6.3.0.tar.xz
cd gmp-6.3.0/
export CC=x86_64-w64-mingw32-gcc
export CC_FOR_BUILD=x86_64-linux-gnu-gcc
./configure --build=x86_64-linux-gnu --host=x86_64-w64-mingw32 --enable-cxx --prefix=/usr/x86_64-w64-mingw32
make
sudo make install
```

#### Build OpenBirch.exe
Start in project root.
```
rm -rf build/
cmake -B build -S . -DCMAKE_TOOLCHAIN_FILE=mingw-toolchain.cmake
cmake --build build -j 12 -- OpenBirch
```

.exe is located at build/OpenBirch.exe

### From (Arch) Linux to Webassembly (wasm)
Install `emscripten` (used to build)

#### Download & install sdk
```
git clone https://github.com/emscripten-core/emsdk.git
cd emsdk
./emsdk install latest
./emsdk activate latest
source ./emsdk_env.sh
```
The SDK environment variables are only saved in this terminal, so
run `source ./emsdk_env.sh` if opening a new terminal/rebooting etc.

#### Build libraries to wasm
Start in project root and setup:
```
mkdir -p lib/emscripten
cd lib/emscripten
```
**fmt:**
```
git clone https://github.com/fmtlib/fmt.git
cd fmt
mkdir build
cd build
emcmake cmake ..
emmake make
```
**gnu gmp:**
```
wget https://gmplib.org/download/gmp/gmp-6.3.0.tar.xz
tar xf gmp-6.3.0.tar.xz
cd gmp-6.3.0
emconfigure ./configure --disable-shared --enable-static --build=x86_64-linux-gnu --enable-cxx --disable-assembly --host none
emmake make
```

#### Build OpenBirch.wasm and OpenBirch.js
```
make web
```
or manually:
```
cd build/
cmake -DCMAKE_TOOLCHAIN_FILE=../Emscripten.cmake ..
cp -r std/ build/
cmake --build . -j 12 -- OpenBirch
```

### From Windows to Linux
idk man it's difficult enough to compile to windows on windows itself, good luck ig


