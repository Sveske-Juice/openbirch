{
  description = "Openbirch nixos flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  };

  outputs = { self, nixpkgs }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in
    {
      devShell.x86_64-linux = with pkgs; mkShell {
        buildInputs = with pkgs; [
          gnumake
          cmake
          ninja
          fmt
          gmp
          gdb
        ];

        shellHook = ''
          zsh
        '';
      };
    };
}
