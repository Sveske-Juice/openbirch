@echo on

:: Hacker mode enabled for better compile
color a

:: Remove the build directory if it exists
if exist build (
    rmdir /s /q build
)

:: Create the build directory
mkdir build

:: Configure the project using CMake with the vcpkg toolchain file
cmake -DCMAKE_TOOLCHAIN_FILE="C:/Users/BOTAlex/Desktop/vcpkg/scripts/buildsystems/vcpkg.cmake" -S . -B ./build

:: Build the project in parallel using 8 threads
cmake --build ./build --parallel 8

:: Indicate that the build is done
echo Build done!
