#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include "runtime/abstract_function.h"

#include <memory>
#include <string>
#include <unordered_map>

class Environment {
  private:
  std::unordered_map<std::string,
                     std::variant<std::unique_ptr<Expression>,
                                  std::unique_ptr<AbstractFunction>>>
      definitions;

  public:
  Environment() {}
  void addDefinition(const std::string &name,
                     std::variant<std::unique_ptr<Expression>,
                                  std::unique_ptr<AbstractFunction>>
                         function) {
    definitions[name] = std::move(function);
  }
  auto getDefinition(const std::string &name) const { return definitions.find(name); }
  auto definitionEnd() const { return definitions.end(); }

  void reset() {
      definitions.clear();
  }
};

#endif
