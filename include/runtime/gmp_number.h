#include <gmp.h>
#include <gmpxx.h>

class GMPNumber {
private:
    mpq_class value;

public:
    GMPNumber(double value_) : value(value_) {}
    GMPNumber(mpq_class value_) : value(value_) {}
    GMPNumber(long t, long b) : value(t, b) {}
    GMPNumber(std::string literal) : value(literal) {}
    GMPNumber(GMPNumber const &other) : value(other.value) {};
    GMPNumber() {
        value = mpq_class(0);
    }

    ~GMPNumber() {}

    // GMPNumber operator=(double value_) {
    //     return GMPNumber(value_);
    // }
    // GMPNumber operator=(std::string value_) {
    //     return GMPNumber(value_);
    // }

    GMPNumber operator+(GMPNumber other) {
        return GMPNumber(this->value + other.value);
    }
    GMPNumber operator-(GMPNumber other) {
        return GMPNumber(this->value - other.value);
    }
    GMPNumber operator/(GMPNumber other) {
        return GMPNumber(this->value / other.value);
    }
    GMPNumber operator*(GMPNumber other) {
        return GMPNumber(this->value * other.value);
    }

    std::string to_string() {
        return value.get_str();
    }
};
