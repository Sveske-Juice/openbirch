#ifndef INTERPRETER_H
#define INTERPRETER_H

#include "parser/iexpression_visitor.h"
#include "parser/istatement_visitor.h"
#include "parser/statement.h"
#include "runtime/environment.h"

#include <memory>
#include <optional>
#include <stack>
#include <vector>

#define MAX_STACK_SIZE 5000

using EnvironmentValue = std::variant<std::unique_ptr<Expression>,
                                      std::unique_ptr<AbstractFunction>>;

/// The \ref Interpreter is responsible for evaluating a tree of
/// \ref Statement "Statements".
class Interpreter : public IStatementVisitor, public IExpressionVisitor {
private:
    std::vector<std::unique_ptr<Statement>> statements;
    std::vector<std::string> autorun;
    bool applyingAutoRule = false;
    void setupEnvironment();
    void addNativeFunctions();
    void addConstants();
    bool importStandardLibrary();

    // Used for WASM version to show answers to user input while they're typing.
    // This shouldn't mutate the state, so we disable assignments (the only
    // thing that changes state)
    bool ignoreAssignments = false;

public:
    Interpreter();
    Interpreter(std::unique_ptr<Statement> _statement);
    Interpreter(std::vector<std::unique_ptr<Statement>> _statements);

    std::vector<Environment> environmentStack;
    std::stack<std::unique_ptr<Expression>> workingStack;
    std::optional<const EnvironmentValue *>
    getDefinition(const std::string &name) const;
    void addDefinition(const std::string &name, EnvironmentValue value,
                       bool local);
    void pushScope();
    void popScope();
    void resetEnvironment();

    std::optional<std::unique_ptr<Expression>> interpret();
    std::optional<std::unique_ptr<Expression>>
    interpret(std::vector<std::unique_ptr<Statement>> statements_);

    void setIgnoreAssignments(bool ignore);

    void CallCodeBlock(const CodeBlock& codeblock);
    std::unique_ptr<Expression> resolve(const Expression& expr) const;

    virtual void
    visitExpressionStatement(ExpressionStatement &statement) override;
    virtual void visitDeclaration(DeclarationStatement &statement) override;
    virtual void visitProcedure(ProcedureStatement &statement) override;
    virtual void visitIfStatement(IfStatement &statement) override;
    virtual void visitReturn(ReturnStatement &statement) override;
    virtual void visitImportStatement(ImportStatement &statement) override;
    virtual void visitLiteralExpression(LiteralExpression &expression) override;
    virtual void visitVectorExpression(VectorExpression &expression) override;
    virtual void visitMapExpression(MapExpression &expression) override;
    virtual void visitLambdaExpression(LambdaExpression &expression) override;
    virtual void visitCallExpression(CallExpression &expression) override;
    virtual void visitBinaryExpression(BinaryExpression &expression) override;
    virtual void visitUnaryExpression(UnaryExpression &expression) override;
};

#endif
