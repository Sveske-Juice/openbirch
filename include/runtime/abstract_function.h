#ifndef ABSTRACT_FUNCTION_H
#define ABSTRACT_FUNCTION_H

#include "parser/expression.h"

using ParameterList = std::vector<std::unique_ptr<Expression>>;
class Interpreter;

enum ReturnCode {
    SUCCESS = 0,
    FAIL = -1,
    AUTORUN_ADD = 2,
    AUTORUN_LIST = 3,
    AUTORUN_SET = 4,
};

struct FunctionReturn {
    ReturnCode rc;
    Expression *expr;
};

class CallContext {
public:
    const Expression& name;
    Interpreter& interpreter;
    ParameterList parameterList;

    CallContext(const Expression& _name, Interpreter& _interpreter, ParameterList _parameterList)
        : name(_name), interpreter(_interpreter), parameterList(std::move(_parameterList)) {}
};

enum AbstractFunctionType {
  NATIVE_FUNCTION,
  LAMBDA_FUNCTION,
  PROCEDURE_FUNCTION
};

class AbstractFunction {
  public:
      const bool evalAfter;
      const bool evalParams;
      AbstractFunction(bool evalAfter_, bool evalParams_) : evalAfter(evalAfter_), evalParams(evalParams_) {}
  virtual FunctionReturn
  call(CallContext& callCtx) = 0;

  virtual AbstractFunctionType type() = 0;
};

#endif
