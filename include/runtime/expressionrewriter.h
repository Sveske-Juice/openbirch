#ifndef EXPRESSIONREWRITER_H
#define EXPRESSIONREWRITER_H

#include "parser/expression.h"
#include "parser/iexpression_visitor.h"

#include <deque>
#include <memory>
#include <string>
#include <unordered_map>

class Interpreter;

using SymbolMap = std::unordered_map<std::string, const Expression *>;
using MatchMap = std::unordered_map<std::string, std::string>;
using ExpressionDeque = std::deque<Expression *>;

struct RewriteResult {
    bool matched{false};
    std::unique_ptr<Expression> rewrittenExpr;
};

class ExpressionRewriter {
private:
    static std::unique_ptr<Expression> resolveReplacement(Expression *node,
                                                          SymbolMap &symbolMap);

public:
    // Should be used as a static class
    ExpressionRewriter() = delete;

    static RewriteResult substitute(std::unique_ptr<Expression> rootSource,
                                    std::unique_ptr<Expression> pattern,
                                    std::unique_ptr<Expression> replacement_,
                                    bool substituteAll = true,
                                    bool recursiveSubstitution = false);
    static RewriteResult substitute(std::unique_ptr<Expression> rootSource,
                                    std::unique_ptr<Expression> pattern,
                                    std::unique_ptr<Expression> replacement_,
                                    const MatchMap &matchMap,
                                    Interpreter &interpreter,
                                    bool substituteAll = true,
                                    bool recursiveSubstitution = false);

    // TODO: Add doc comments explaining the use for these functions
    static Expression *checkForMatch(ExpressionDeque &patternPostfix,
                                     ExpressionDeque &sourcePostfix,
                                     SymbolMap &symbolMap,
                                     const MatchMap &matchMap,
                                     Interpreter &interpreter);

    static int matchPart(const Expression &sourceExpr,
                         ExpressionDeque &sourcePostfix,
                         const MatchMap &matchMap, SymbolMap &symbolMap,
                         const Expression &patternExpr,
                         Interpreter &interpreter);

    static std::unique_ptr<Expression>
    replaceNodeInAst(Expression &node, std::unique_ptr<Expression> replacement,
                     SymbolMap *symbolMap = nullptr);
};

class RPNExpressionBuilder : public IExpressionVisitor {
private:
    ExpressionDeque rpn;

public:
    ExpressionDeque buildRPNQueue(Expression &root);
    virtual void visitLiteralExpression(LiteralExpression &expression) override;
    virtual void visitVectorExpression(VectorExpression &expression) override;
    virtual void visitMapExpression(MapExpression &expression) override;
    virtual void visitLambdaExpression(LambdaExpression &expression) override;
    virtual void visitCallExpression(CallExpression &expression) override;
    virtual void visitBinaryExpression(BinaryExpression &expression) override;
    virtual void visitUnaryExpression(UnaryExpression &expression) override;
};

#endif
