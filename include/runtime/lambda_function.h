#ifndef LAMBDA_FUNCTION_H
#define LAMBDA_FUNCTION_H

#include "runtime/abstract_function.h"

class LambdaFunction : public AbstractFunction {
private:
    std::unique_ptr<LambdaExpression> lambdaExpression_;

public:
    LambdaFunction(std::unique_ptr<LambdaExpression> expression,
                   bool evalAfter_ = false, bool evalParams_ = true)
        : AbstractFunction(evalAfter_, evalParams_),
          lambdaExpression_(std::move(expression)) {}

    virtual FunctionReturn
    call(CallContext& callCtx) override;

    virtual AbstractFunctionType type() override { return LAMBDA_FUNCTION; }

    const LambdaExpression &lambdaExpression() const { return *lambdaExpression_; }
};

#endif
