#ifndef RUNTIME_ERRORS_H
#define RUNTIME_ERRORS_H

#include "carl_constants.h"
#include "lexer/token.h"
#include <cassert>
#include <cstddef>
#include <fmt/core.h>
#include <format>
#include <memory>
#include <string_view>
#include <tuple>

class Expression;

class RuntimeException {
public:
    const size_t sourceOffset_;
    const size_t length_;
    static inline std::tuple<size_t, size_t>
    calculateLineNumber(const std::string_view source, const size_t sourceOffset) {
        /* assert((source.size() >= sourceOffset) && */
        /*        "Could not calculate line number since the offset exceeds the " */
        /*        "bounds of the source string given"); */

        int lineNumber{0};
        int offsetToLineNumber{0};
        for (size_t offset = 0; offset != sourceOffset; offset++) {
            if (source[offset] == '\n') {
                lineNumber++;
                offsetToLineNumber = offset;
            }
        }
        return std::make_tuple(lineNumber, sourceOffset - offsetToLineNumber);
    }
    std::string locationPrefix(std::string_view source) const {
        int startLineNumber;
        int startCharacterIndex;
        std::tie(startLineNumber, startCharacterIndex) =
            RuntimeException::calculateLineNumber(source, sourceOffset_);

        int endLineNumber;
        int endCharacterIndex;
        std::tie(endLineNumber, endCharacterIndex) =
            RuntimeException::calculateLineNumber(source,
                                                  sourceOffset_ + length_);
        return std::format("{}:{} - {}:{}", startLineNumber,
                           startCharacterIndex, endLineNumber,
                           endCharacterIndex);
    }

public:
    RuntimeException(const size_t sourceOffset, const size_t length)
        : sourceOffset_{sourceOffset}, length_{length} {}
    RuntimeException(const Token &token)
        : sourceOffset_{token.sourceOffset}, length_{token.length} {}

    /// Reports the error and where it happened
    virtual std::string what(std::string_view source) const noexcept = 0;

    /// Used for when the source is unavailable or the source location is unkown
    virtual std::string what_anonymous() const noexcept = 0;

    virtual RuntimeError error_code() const noexcept = 0;
};

class TypeMismatch : public RuntimeException {
private:
    const std::string reason_;

public:
    TypeMismatch(const int sourceOffset, const int length)
        : RuntimeException(sourceOffset, length) {}
    TypeMismatch(const Token &token, std::string reason)
        : RuntimeException(token), reason_{reason} {}

    virtual std::string
    what(std::string_view source) const noexcept override {
        return fmt::format("{} {}", locationPrefix(source), reason_);
    }

    virtual std::string what_anonymous() const noexcept override {
        return reason_;
    }

    virtual RuntimeError error_code() const noexcept override {
        return RuntimeError::TYPE_MISMATCH;
    }
};

class StackOverflow : public RuntimeException {
private:
    const std::string reason_;

public:
    StackOverflow(const int sourceOffset, const int length)
        : RuntimeException(sourceOffset, length) {}
    StackOverflow(const Token &token, std::string reason)
        : RuntimeException(token), reason_{reason} {}
    StackOverflow() : RuntimeException(0,0) {}

    virtual std::string
    what(std::string_view source) const noexcept override {
        return fmt::format("{} {}", locationPrefix(source), reason_);
    }

    virtual std::string what_anonymous() const noexcept override {
        return reason_;
    }

    virtual RuntimeError error_code() const noexcept override {
        return RuntimeError::TYPE_MISMATCH;
    }
};

class NotDefined : public RuntimeException {
private:
    const std::string reason_;

public:
    NotDefined(const int sourceOffset, const int length)
        : RuntimeException(sourceOffset, length) {}
    NotDefined(const Token &token, std::string reason)
        : RuntimeException(token), reason_{reason} {}

    virtual std::string
    what(std::string_view source) const noexcept override {
        return fmt::format("{} {}", locationPrefix(source), reason_);
    }
    virtual std::string what_anonymous() const noexcept override {
        return reason_;
    }
    virtual RuntimeError error_code() const noexcept override {
        return RuntimeError::NOT_DEFINED;
    }
};

class UnimplementedLogic : public RuntimeException {
private:
    const std::string reason_;

public:
    UnimplementedLogic(const int sourceOffset, const int length,
                       std::string reason)
        : RuntimeException(sourceOffset, length), reason_(reason) {}
    UnimplementedLogic(const Token &token, std::string reason)
        : RuntimeException(token), reason_{reason} {}

    virtual std::string
    what(std::string_view source) const noexcept override {
        return fmt::format("{} {}", locationPrefix(source), reason_);
    }
    virtual std::string what_anonymous() const noexcept override {
        return reason_;
    }
    virtual RuntimeError error_code() const noexcept override {
        return RuntimeError::UNIMPLEMENTED_INTERPRETER;
    }
};

class CallArity : public RuntimeException {
private:
    const std::string reason_;

public:
    CallArity(const int sourceOffset, const int length, std::string reason)
        : RuntimeException(sourceOffset, length), reason_(reason) {}
    CallArity(const Token &token, std::string reason)
        : RuntimeException(token), reason_{reason} {}

    virtual std::string
    what(std::string_view source) const noexcept override {
        return fmt::format("{} {}", locationPrefix(source), reason_);
    }
    virtual std::string what_anonymous() const noexcept override {
        return reason_;
    }
    virtual RuntimeError error_code() const noexcept override {
        return RuntimeError::CALL_ARITY;
    }
};

class ImportError : public RuntimeException {
private:
    const std::string reason_;

public:
    ImportError(const int sourceOffset, const int length, std::string reason)
        : RuntimeException(sourceOffset, length), reason_(reason) {}
    ImportError(const Token &token, std::string reason)
        : RuntimeException(token), reason_{reason} {}

    virtual std::string
    what(std::string_view source) const noexcept override {
        return fmt::format("{} {}", locationPrefix(source), reason_);
    }
    virtual std::string what_anonymous() const noexcept override {
        return reason_;
    }
    virtual RuntimeError error_code() const noexcept override {
        return RuntimeError::IMPORT_ERROR;
    }
};


class ReturnValue {
public:
  std::unique_ptr<Expression> value;

  ReturnValue(std::unique_ptr<Expression> value_) : value(std::move(value_)) {}
};

#endif
