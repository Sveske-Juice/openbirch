#ifndef PROCEDURE_FUNCTION_H
#define PROCEDURE_FUNCTION_H

#include "parser/statement.h"
#include "runtime/abstract_function.h"
#include "runtime/interpreter.h"
#include <string>

class ProcedureFunction : public AbstractFunction {
private:
    std::string name_;
    std::vector<std::string> arguments_;
    std::vector<std::unique_ptr<Statement>> body_;

public:
    ProcedureFunction(std::string name, std::vector<std::string> arguments,
                      std::vector<std::unique_ptr<Statement>> body)
        : AbstractFunction(false, true), name_(name), arguments_(arguments),
          body_(std::move(body)) {}

    std::string name() { return name_; }
    std::vector<std::unique_ptr<Statement>> &body() { return body_; }

    FunctionReturn
    call(CallContext& callCtx) override {
        if (arguments_.size() != callCtx.parameterList.size())
            throw CallArity(callCtx.name.tokenClone(),
                            fmt::format("The procedure '{}' takes {} "
                                        "arguments, but {} where supplied",
                                        name_,
                                        arguments_.size(),
                                        callCtx.parameterList.size()));

        // Create new scope
        callCtx.interpreter.pushScope();

        // Define arguments in scope
        for (size_t i = 0; i < callCtx.parameterList.size(); i++) {
            callCtx.interpreter.addDefinition(arguments_[i],
                                      std::move(callCtx.parameterList[i]), true);
        }

        try {
            try {
                for (auto &statement : body_) {
                    statement->accept(callCtx.interpreter);
                }
            } catch (const StackOverflow &e) {
                callCtx.interpreter.popScope();
                throw StackOverflow(
                    callCtx.name.tokenClone(),
                    fmt::format("Stack overflow in procedure '{}'",
                                name_));
            }
        } catch (ReturnValue &r) {
            callCtx.interpreter.popScope();
            return {SUCCESS, r.value.release()};
        }

        // Remove scope when we're done
        callCtx.interpreter.popScope();

        return {SUCCESS, nullptr};
    }

    virtual AbstractFunctionType type() override { return PROCEDURE_FUNCTION; }
};

#endif
