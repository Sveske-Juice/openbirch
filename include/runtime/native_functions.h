#ifndef NATIVE_FUNCTIONS_H
#define NATIVE_FUNCTIONS_H

#include "lexer/token.h"
#include "parser/ast_printer.h"
#include "parser/expression.h"
#include "runtime/abstract_function.h"
#include "runtime/expressionrewriter.h"
#include "runtime/interpreter.h"
#include "runtime/lambda_function.h"
#include "runtime/runtime_errors.h"

#include <cmath>
#include <fmt/core.h>
#include <iostream>
#include <memory>
#include <variant>

/* UTILS */
static FunctionReturn __native_type(CallContext &callCtx) {
    const ExpressionType parameterType =
        callCtx.parameterList[0]->expressionType();
    LiteralValue value;
    if (parameterType == EXPR_TYPE) {
        value.type_value =
            static_cast<const LiteralExpression &>(*callCtx.parameterList[0])
                .value()
                .type_value;
        return {SUCCESS, new LiteralExpression(
                             Token(value.type_value, 0, 0,
                                   TokenTypesToString[value.type_value]),
                             value)};
    } else if (parameterType == EXPR_IDENTIFIER) {
        const auto &literal =
            static_cast<const LiteralExpression &>(*callCtx.parameterList[0]);
        auto definition =
            callCtx.interpreter.getDefinition(literal.value().string_value);
        if (!definition.has_value()) {
            value.type_value = TYPE_IDENTIFIER;
        } else if (std::holds_alternative<std::unique_ptr<Expression>>(
                       *definition.value())) {
            value.type_value = TYPE_IDENTIFIER;
        } else {
            value.type_value = TYPE_FUNCTION;
        }
        return {SUCCESS, new LiteralExpression(
                             Token(value.type_value, 0, 0,
                                   TokenTypesToString[value.type_value]),
                             value)};
    }
    if (auto it = ExpressionTypeToBirchType.find(parameterType);
        it != ExpressionTypeToBirchType.cend()) {
        value.type_value = it->second;
        std::string tokenStr = TokenTypesToString[value.type_value];
        return {SUCCESS,
                new LiteralExpression(
                    Token(value.type_value, 0, tokenStr.length(), tokenStr),
                    value)};
    }
    throw UnimplementedLogic(
        callCtx.parameterList[0]->tokenClone(),
        fmt::format("No map implemented from '{}' to a BirchType",
                    ExpressionTypeToString[parameterType]));
}

static bool __native_helper_node_match(const Expression &n1,
                                       const Expression &n2) {
    if (n2.expressionType() == EXPR_TYPE) {
        if (auto it = ExpressionTypeToBirchType.find(n1.expressionType());
            it != ExpressionTypeToBirchType.cend()) {
            return it->second == n2.tokenClone().type;
        }
        return false;
    }

    if (n1.expressionType() != n2.expressionType())
        return false;

    switch (n1.expressionType()) {
    case EXPR_NUMBER:
        return AS_LITERALEXPR(n1).value().number_value ==
               AS_LITERALEXPR(n2).value().number_value;
    case EXPR_BOOLEAN:
        return AS_LITERALEXPR(n1).value().boolean_value ==
               AS_LITERALEXPR(n2).value().boolean_value;
    case EXPR_IDENTIFIER:
    case EXPR_STRING:
        return strcmp(AS_LITERALEXPR(n1).value().string_value,
                      AS_LITERALEXPR(n2).value().string_value) == 0;
    default:
        throw UnimplementedLogic(
            n1.tokenClone(),
            "Unimplemented switch for native node match helper");
    }
}

static FunctionReturn __native_contains(CallContext &callCtx) {
    Expression &expression = *callCtx.parameterList[0];
    Expression &key = *callCtx.parameterList[1];
    RPNExpressionBuilder rpnBuilder;
    const auto &rpnStack = rpnBuilder.buildRPNQueue(expression);

    for (const auto node : rpnStack) {
        if (__native_helper_node_match(*node, key)) {
            return {SUCCESS, MAKE_TRUE};
        }
    }
    return {SUCCESS, MAKE_FALSE};
}

static FunctionReturn __native_lhs(CallContext &callCtx) {
    const auto &parameter = callCtx.parameterList[0];
    if (auto it = binaryTokens.find(parameter->expressionType());
        it == binaryTokens.cend()) {
        throw TypeMismatch(
            parameter->tokenClone(),
            fmt::format("Native LHS procedure expects binary, but received: {}",
                        ExpressionTypeToString[parameter->expressionType()]));
    }

    const auto &binary = static_cast<const BinaryExpression &>(*parameter);
    return {SUCCESS, binary.left().clone().release()};
}

// get operand of unary
static FunctionReturn __native_operand(CallContext &callCtx) {
    const auto &parameter = callCtx.parameterList[0];
    if (parameter->expressionType() != EXPR_NEGATE && parameter->expressionType() != EXPR_LOGIC_NEGATE) {
        throw TypeMismatch(
            parameter->tokenClone(),
            fmt::format("Native operand procedure expects unary, but received: {}",
                        ExpressionTypeToString[parameter->expressionType()]));
    }

    const auto &unary = static_cast<const UnaryExpression &>(*parameter);
    return {SUCCESS, unary.operand().clone().release() };
}

static FunctionReturn __native_rhs(CallContext &callCtx) {
    const auto &parameter = callCtx.parameterList[0];
    if (auto it = binaryTokens.find(parameter->expressionType());
        it == binaryTokens.cend()) {
        throw TypeMismatch(
            parameter->tokenClone(),
            fmt::format("Native RHS procedure expects binary, but received: {}",
                        ExpressionTypeToString[parameter->expressionType()]));
    }

    const auto &binary = static_cast<const BinaryExpression &>(*parameter);
    return {SUCCESS, binary.right().clone().release()};
}

static FunctionReturn
__native_replace(CallContext &callCtx) {
    const auto &source = *callCtx.parameterList[0];
    const auto &pattern = *callCtx.parameterList[1];
    const auto &replacement = *callCtx.parameterList[2];

    RewriteResult res = ExpressionRewriter::substitute(source.clone(), pattern.clone(), replacement.clone(), true);
    if (res.matched)
        return { SUCCESS, res.rewrittenExpr->clone().release() };
    return { SUCCESS, source.clone().release() };
}

static FunctionReturn
__native_eval(CallContext &callCtx) {
    auto &parameter = *callCtx.parameterList[0];
    parameter.accept(callCtx.interpreter);
    auto res = std::move(callCtx.interpreter.workingStack.top());
    callCtx.interpreter.workingStack.pop();

    return { SUCCESS, res.release() };
}

static FunctionReturn __native_index(CallContext &callCtx) {
    const auto &collection = *callCtx.parameterList[0];
    if (collection.expressionType() == EXPR_VECTOR) {
        const auto &index = *callCtx.parameterList[1];
        if (index.expressionType() != EXPR_NUMBER) {
            throw TypeMismatch(index.tokenClone(), "Expected second parameter of index to be a non floating point number");
        }
        const auto& indexLit = AS_LITERALEXPR(index);

        // Must not be a decimal value
        if (std::trunc(indexLit.value().number_value) != indexLit.value().number_value) {
            throw TypeMismatch(index.tokenClone(), "Expected second parameter of index to be a non floating point number");
        }

        const auto &vec = AS_VECTOREXPR(collection);
        const auto &element = vec.get_const(static_cast<size_t>(indexLit.value().number_value));
        return { SUCCESS, element.clone().release() };
    } else if (collection.expressionType() == EXPR_MAP) {
        const auto &key = *callCtx.parameterList[1];

        const auto &map = AS_MAPEXPR(collection);
        if (auto it = map.mapReference().find(key.clone()); it != map.mapReference().cend()) {
            return { SUCCESS, it->second->clone().release() };
        }
        else {
            return { SUCCESS, MAKE_NIL };
        }

    } else {
        throw TypeMismatch(
            collection.tokenClone(),
            fmt::format("Can not index type: {}",
                        ExpressionTypeToString[collection.expressionType()]));
    }
}

static FunctionReturn
__native_insert_map(CallContext &callCtx) {
    auto &collection = *callCtx.parameterList[0];
    if (collection.expressionType() != EXPR_MAP) {
        throw TypeMismatch(collection.tokenClone(), "Expected first argument to be a map");
    }
    const auto &key = *callCtx.parameterList[1];
    const auto &value = *callCtx.parameterList[2];

    auto &map = static_cast<MapExpression &>(collection);
    map.insert(key.clone(), value.clone());
    return { SUCCESS, map.clone().release() };
}

// Get function from call
static FunctionReturn
__native_get_func(CallContext &callCtx) {
    const auto &parameter = *callCtx.parameterList[0];
    if (parameter.expressionType() != EXPR_CALL) {
        throw TypeMismatch(parameter.tokenClone(), "Expected call as parameter");
    }
    const auto &call = static_cast<const CallExpression &>(parameter);
    std::string funcName = call.lambdaName().value().string_value;
    return { SUCCESS, new LiteralExpression(Token(IDENTIFIER, 0, funcName.length(), funcName), funcName) };
}

static FunctionReturn
__native_get_params(CallContext &callCtx) {
    const auto &parameter = *callCtx.parameterList[0];
    const auto &index = *callCtx.parameterList[1];
    if (parameter.expressionType() != EXPR_CALL) {
        throw TypeMismatch(parameter.tokenClone(), "Expected call as parameter");
    }
    if (index.expressionType() != EXPR_NUMBER) {
        throw TypeMismatch(index.tokenClone(), "Expected second parameter of params to be a non floating point number");
    }
    const auto &indexLit = static_cast<const LiteralExpression &>(index);

    // Must not be a decimal value
    if (std::trunc(indexLit.value().number_value) != indexLit.value().number_value) {
        throw TypeMismatch(index.tokenClone(), "Expected second parameter of params to be a non floating point number");
    }

    const auto &call = static_cast<const CallExpression &>(parameter);
    size_t idx = static_cast<size_t>(indexLit.value().number_value);

    // TODO: change err type
    if (idx >= call.parameters().size())
        throw TypeMismatch(index.tokenClone(), fmt::format("Illegal index, call expression doesn't have a parameter at index: {}", idx));
    auto result = call.parameters()[idx]->clone();
    return { SUCCESS, result.release() };
}

/* I/O */
static FunctionReturn __native_print(CallContext &callCtx) {
    AstPrinter printer;
    std::cout << printer.print(*callCtx.parameterList[0].get()) << std::endl;
    return {SUCCESS, nullptr};
}

static FunctionReturn __native_autorun_add(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_STRING &&
        callCtx.parameterList[0]->expressionType() != EXPR_IDENTIFIER)
        throw TypeMismatch(callCtx.parameterList[0]->tokenClone(),
                           "Autorun argument must be either a string or the "
                           "name of a procedure");

    return {AUTORUN_ADD, callCtx.parameterList[0].release()};
}

static FunctionReturn __native_autorun_list(__attribute__((unused))
                                            CallContext &callCtx) {
    return {AUTORUN_LIST, nullptr};
}

static FunctionReturn __native_autorun_set(CallContext &callCtx) {

    if (callCtx.parameterList[0]->expressionType() != EXPR_VECTOR)
        throw TypeMismatch(callCtx.parameterList[0]->tokenClone(),
                           "Expected a vector here");

    return {AUTORUN_SET, callCtx.parameterList[0].release()};
}

/* TRIG FUNCTIONS */
static FunctionReturn __native_sin(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_NUMBER) {
        return {FAIL, new CallExpression(std::make_unique<LiteralExpression>(
                                             callCtx.name.tokenClone(), "sin"),
                                         std::move(callCtx.parameterList))};
    }

    const LiteralExpression &number =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[0].get());

    auto calculated_value = LiteralValue(sin(number.value().number_value));

    return {SUCCESS, new LiteralExpression(
                         Token(NUMBER, 0, 0,
                               std::to_string(calculated_value.number_value)),
                         calculated_value)};
}

static FunctionReturn __native_cos(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_NUMBER) {
        return {FAIL, new CallExpression(std::make_unique<LiteralExpression>(
                                             callCtx.name.tokenClone(), "cos"),
                                         std::move(callCtx.parameterList))};
    }

    const LiteralExpression &number =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[0].get());

    auto calculated_value = LiteralValue(cos(number.value().number_value));

    return {SUCCESS, new LiteralExpression(
                         Token(NUMBER, 0, 0,
                               std::to_string(calculated_value.number_value)),
                         calculated_value)};
}

static FunctionReturn __native_tan(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_NUMBER) {
        return {FAIL, new CallExpression(std::make_unique<LiteralExpression>(
                                             callCtx.name.tokenClone(), "tan"),
                                         std::move(callCtx.parameterList))};
    }

    const LiteralExpression &number =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[0].get());

    auto calculated_value = LiteralValue(tan(number.value().number_value));

    return {SUCCESS, new LiteralExpression(
                         Token(NUMBER, 0, 0,
                               std::to_string(calculated_value.number_value)),
                         calculated_value)};
}

static FunctionReturn __native_asin(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_NUMBER) {
        return {FAIL, new CallExpression(std::make_unique<LiteralExpression>(
                                             callCtx.name.tokenClone(), "asin"),
                                         std::move(callCtx.parameterList))};
    }

    const LiteralExpression &number =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[0].get());

    auto calculated_value = LiteralValue(asin(number.value().number_value));

    return {SUCCESS, new LiteralExpression(
                         Token(NUMBER, 0, 0,
                               std::to_string(calculated_value.number_value)),
                         calculated_value)};
}

static FunctionReturn __native_acos(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_NUMBER) {
        return {FAIL, new CallExpression(std::make_unique<LiteralExpression>(
                                             callCtx.name.tokenClone(), "acos"),
                                         std::move(callCtx.parameterList))};
    }

    const LiteralExpression &number =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[0].get());

    auto calculated_value = LiteralValue(acos(number.value().number_value));

    return {SUCCESS, new LiteralExpression(
                         Token(NUMBER, 0, 0,
                               std::to_string(calculated_value.number_value)),
                         calculated_value)};
}

static FunctionReturn __native_atan(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_NUMBER) {
        return {FAIL, new CallExpression(std::make_unique<LiteralExpression>(
                                             callCtx.name.tokenClone(), "atan"),
                                         std::move(callCtx.parameterList))};
    }

    const LiteralExpression &number =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[0].get());

    auto calculated_value = LiteralValue(atan(number.value().number_value));

    return {SUCCESS, new LiteralExpression(
                         Token(NUMBER, 0, 0,
                               std::to_string(calculated_value.number_value)),
                         calculated_value)};
}

// Calculated as cos(x)/sin(x)
static FunctionReturn __native_cot(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_NUMBER) {
        return {FAIL, new CallExpression(std::make_unique<LiteralExpression>(
                                             callCtx.name.tokenClone(), "cot"),
                                         std::move(callCtx.parameterList))};
    }

    const LiteralExpression &number =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[0].get());

    auto calculated_value = LiteralValue(cos(number.value().number_value) /
                                         sin(number.value().number_value));

    return {SUCCESS, new LiteralExpression(
                         Token(NUMBER, 0, 0,
                               std::to_string(calculated_value.number_value)),
                         calculated_value)};
}

// Calculated as 1/cos(x)
static FunctionReturn __native_sec(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_NUMBER) {
        return {FAIL, new CallExpression(std::make_unique<LiteralExpression>(
                                             callCtx.name.tokenClone(), "sec"),
                                         std::move(callCtx.parameterList))};
    }

    const LiteralExpression &number =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[0].get());

    auto calculated_value =
        LiteralValue(1.0 / cos(number.value().number_value));

    return {SUCCESS, new LiteralExpression(
                         Token(NUMBER, 0, 0,
                               std::to_string(calculated_value.number_value)),
                         calculated_value)};
}

// Calculated as 1/sin(x)
static FunctionReturn __native_csc(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_NUMBER) {
        return {FAIL, new CallExpression(std::make_unique<LiteralExpression>(
                                             callCtx.name.tokenClone(), "csc"),
                                         std::move(callCtx.parameterList))};
    }

    const LiteralExpression &number =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[0].get());

    auto calculated_value =
        LiteralValue(1.0 / sin(number.value().number_value));

    return {SUCCESS, new LiteralExpression(
                         Token(NUMBER, 0, 0,
                               std::to_string(calculated_value.number_value)),
                         calculated_value)};
}

/* GENERAL MATH */
static FunctionReturn __native_sqrt(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_NUMBER) {
        return {FAIL, new CallExpression(std::make_unique<LiteralExpression>(
                                             callCtx.name.tokenClone(), "sqrt"),
                                         std::move(callCtx.parameterList))};
    }

    const LiteralExpression &number =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[0].get());

    auto calculated_value = LiteralValue(sqrt(number.value().number_value));

    return {SUCCESS, new LiteralExpression(
                         Token(NUMBER, 0, 0,
                               std::to_string(calculated_value.number_value)),
                         calculated_value)};
}

static FunctionReturn __native_cbrt(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_NUMBER) {
        return {FAIL, new CallExpression(std::make_unique<LiteralExpression>(
                                             callCtx.name.tokenClone(), "cbrt"),
                                         std::move(callCtx.parameterList))};
    }

    const LiteralExpression &number =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[0].get());

    auto calculated_value = LiteralValue(cbrt(number.value().number_value));

    return {SUCCESS, new LiteralExpression(
                         Token(NUMBER, 0, 0,
                               std::to_string(calculated_value.number_value)),
                         calculated_value)};
}

// Calculated as pow(number, 1/root)
static FunctionReturn __native_nroot(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_NUMBER ||
        callCtx.parameterList[1]->expressionType() != EXPR_NUMBER) {
        return {FAIL,
                new CallExpression(std::make_unique<LiteralExpression>(
                                       callCtx.name.tokenClone(), "nroot"),
                                   std::move(callCtx.parameterList))};
    }

    const LiteralExpression &number =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[0].get());
    const LiteralExpression &root =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[1].get());

    auto calculated_value = LiteralValue(
        pow(number.value().number_value, 1.0 / root.value().number_value));

    return {SUCCESS, new LiteralExpression(
                         Token(NUMBER, 0, 0,
                               std::to_string(calculated_value.number_value)),
                         calculated_value)};
}

static FunctionReturn __native_log(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_NUMBER ||
        callCtx.parameterList[1]->expressionType() != EXPR_NUMBER) {
        return {FAIL, new CallExpression(std::make_unique<LiteralExpression>(
                                             callCtx.name.tokenClone(), "log"),
                                         std::move(callCtx.parameterList))};
    }

    const LiteralExpression &number =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[0].get());

    const LiteralExpression &power =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[1].get());

    auto calculated_value = LiteralValue(std::log(number.value().number_value) /
                                         std::log(power.value().number_value));
    return {SUCCESS, new LiteralExpression(
                         Token(NUMBER, 0, 0,
                               std::to_string(calculated_value.number_value)),
                         calculated_value)};
}

static FunctionReturn __native_natural_log(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_NUMBER) {
        return {FAIL, new CallExpression(std::make_unique<LiteralExpression>(
                                             callCtx.name.tokenClone(), "ln"),
                                         std::move(callCtx.parameterList))};
    }

    const LiteralExpression &number =
        static_cast<const LiteralExpression &>(*callCtx.parameterList[0].get());

    auto calculated_value = LiteralValue(std::log(number.value().number_value));

    return {SUCCESS, new LiteralExpression(
                         Token(NUMBER, 0, 0,
                               std::to_string(calculated_value.number_value)),
                         calculated_value)};
}

/* VECTORS */
static FunctionReturn __native_vector_dotproduct(CallContext &callCtx) {
    if (callCtx.parameterList[0]->expressionType() != EXPR_VECTOR ||
        callCtx.parameterList[1]->expressionType() != EXPR_VECTOR) {
        return {FAIL, new CallExpression(std::make_unique<LiteralExpression>(
                                             callCtx.name.tokenClone(), "dot"),
                                         std::move(callCtx.parameterList))};
    }

    auto lVec = static_cast<VectorExpression *>(callCtx.parameterList[0].get());
    auto rVec = static_cast<VectorExpression *>(callCtx.parameterList[1].get());

    if (lVec->size() != rVec->size()) {
        return {FAIL, new CallExpression(std::make_unique<LiteralExpression>(
                                             callCtx.name.tokenClone(), "dot"),
                                         std::move(callCtx.parameterList))};
    }

    auto result = std::make_unique<BinaryExpression>(
        std::unique_ptr<Expression>(lVec->obtain(0)), Token(STAR, 0, 1, "*"),
        std::unique_ptr<Expression>(rVec->obtain(0)));
    for (size_t i = 1; i < lVec->size(); i++) {
        auto ith = std::make_unique<BinaryExpression>(
            std::unique_ptr<Expression>(lVec->obtain(i)),
            Token(STAR, 0, 1, "*"),
            std::unique_ptr<Expression>(rVec->obtain(i)));
        result = std::make_unique<BinaryExpression>(
            std::move(result), Token(PLUS, 0, 1, "+"), std::move(ith));
    }
    return {SUCCESS, result.release()};
}

static size_t __internal_isolate_find_depth_of_token(std::string token,
                                                     Expression &tree);
static FunctionReturn __native_isolate(CallContext &callCtx) {

    if (callCtx.parameterList[0]->expressionType() != EXPR_IDENTIFIER)
        throw TypeMismatch(callCtx.parameterList[0]->tokenClone(),
                           "Can only isolate for variables.");

    if (callCtx.parameterList[1]->expressionType() != EXPR_EQUALS)
        throw TypeMismatch(
            callCtx.parameterList[1]->tokenClone(),
            "Can only isolate for tokens that are a part of an equation");

    std::string variable_to_isolate_for =
        static_cast<LiteralExpression *>(callCtx.parameterList[0].get())
            ->toString();

    // Find the side of the equation that has the shallowest instance of the
    //
    // token. We do this with breadth first recursive descent, keeping track of
    // the amount of nodes we've descended, and returning the minimum.

    auto expression =
        static_cast<BinaryExpression *>(callCtx.parameterList[1].get());

    size_t left_depth = __internal_isolate_find_depth_of_token(
        variable_to_isolate_for, expression->left());

    size_t right_depth = __internal_isolate_find_depth_of_token(
        variable_to_isolate_for, expression->right());

    if (left_depth == 0 && right_depth == 0)
        throw TypeMismatch(
            callCtx.name.tokenClone(),
            fmt::format("Given expression does not contain the token '{}'",
                        variable_to_isolate_for));

    // printf("left depth: %zu, right depth: %zu", left_depth, right_depth);

    // Swap so the thing is on the left side. Yuh
    if ((right_depth < left_depth || left_depth == 0) && right_depth != 0)
        expression = new BinaryExpression(expression->right().clone(),
                                          expression->tokenClone(),
                                          expression->left().clone());
    else
        expression = new BinaryExpression(expression->left().clone(),
                                          expression->tokenClone(),
                                          expression->right().clone());

    // This is where the magic happens

    // while (true) {
    //
    // }

    std::vector<std::unique_ptr<Expression>> new_parameterList;
    new_parameterList.reserve(2);

    new_parameterList.push_back(callCtx.parameterList[0]->clone());
    new_parameterList.push_back(expression->clone());

    auto identifier_literal =
        static_cast<LiteralExpression *>(callCtx.name.clone().release());

    auto final_expression = new CallExpression(
        std::unique_ptr<LiteralExpression>(identifier_literal),
        std::move(new_parameterList));

    return {SUCCESS, final_expression};
}

/// Searches an expression for literals that match the given token.
/// Returns the depth of the token in the tree, or 0 if it wasn't found.
static size_t __internal_isolate_find_depth_of_token(std::string token,
                                                     Expression &tree) {
    std::stack<Expression *> queue;

    size_t depth = 0;

    queue.push(tree.clone().release());

    while (!queue.empty()) {
        Expression *value = queue.top();
        queue.pop();
        // Not really depth, more like total nodes visited. But it works since
        // breadth first search goes from shallowest to deepest. Only side
        // affect is that results are also dependant on the order of operations,
        // IE depth(3/x) > depth(x/3)
        depth++;

        if (value->expressionType() == EXPR_IDENTIFIER &&
            value->toString() == token) {
            return depth;
        }

        switch (value->expressionType()) {

        case EXPR_EQUALS:
            throw TypeMismatch(value->tokenClone(),
                               "Cannot isolate expressions that contain "
                               "multiple equals signs");

        case EXPR_STRING:
        case EXPR_IDENTIFIER:
        case EXPR_NUMBER:
        case EXPR_BOOLEAN:
            continue;

        case EXPR_VECTOR: {
            auto vector = static_cast<VectorExpression *>(value);

            for (size_t i = 0; i < vector->size(); i++) {
                auto item = vector->obtain(i);
                queue.push(item);
            }

            break;
        }

        case EXPR_LAMBDA:
            continue;

        case EXPR_CALL: {
            auto call = static_cast<CallExpression *>(value);

            for (auto &parameter : call->parameters()) {
                queue.push(parameter->clone().get());
            }

            break;
        }

        case EXPR_PLUS:
        case EXPR_MINUS:
        case EXPR_MULTIPLICATION:
        case EXPR_DIVISION:
        case EXPR_MODULUS:
        case EXPR_NOT_EQUALS:
        case EXPR_EXPONENT:
        case EXPR_DIFFERENTIATE: {
            auto binary = static_cast<BinaryExpression *>(value);

            queue.push(binary->left().clone().release());
            queue.push(binary->left().clone().release());

            break;
        }

        case EXPR_NEGATE:
        case EXPR_LOGIC_NEGATE: {
            auto unary = static_cast<UnaryExpression *>(value);

            queue.push(unary->operand().clone().get());

            break;
        }

        default:
            throw UnimplementedLogic(
                Token(TokenType::NUMBER, 0, 0, ""),
                fmt::format("Unimplemented expression type '{}' in {}. Send "
                            "this message to the developers",
                            TokenTypesToString[value->tokenClone().type],
                            __func__));
        }
    }

    // 0 indicates not found
    return 0;
}

// int sort_order(Expression& expr) {
//
// }
//
// static Expression*
// __native_sort(std::vector<std::unique_ptr<Expression>> parameterList) {
//
// }

#endif
