#ifndef NATIVE_FUNCTION_H
#define NATIVE_FUNCTION_H

#include "lexer/token.h"
#include "parser/expression.h"
#include "runtime/runtime_errors.h"
#include "runtime/abstract_function.h"

#include <fmt/core.h>
#include <functional>

using NativeFn =
    std::function<FunctionReturn(CallContext& callCtx)>;

class NativeFunction : public AbstractFunction {
  const size_t argCount;
  NativeFn nativeFunction;

  public:
  NativeFunction(size_t _argCount, NativeFn _nativeFunction, bool evalAfter_ = false, bool evalParams_ = true)
      : AbstractFunction(evalAfter_, evalParams_), argCount(_argCount), nativeFunction{_nativeFunction} {}

  virtual FunctionReturn
  call(CallContext& callCtx) override {
    if (argCount != callCtx.parameterList.size())
      throw CallArity(
          callCtx.name.tokenClone(),
          fmt::format("Called native function '{}' with {} argument(s), "
                      "but expected {} argument(s)",
                      callCtx.name.tokenClone().literal, callCtx.parameterList.size(), argCount));

    return nativeFunction(callCtx);
  }

  virtual AbstractFunctionType type() override { return NATIVE_FUNCTION; }
};

#endif
