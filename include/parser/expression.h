#ifndef EXPRESSION_H
#define EXPRESSION_H

#include "lexer/lexer.h"
#include "lexer/token.h"
#include "parser/iexpression_visitor.h"
#include "runtime/gmp_number.h"
#include "runtime/runtime_errors.h"

#include <cstddef>
#include <fmt/core.h>
#include <fmt/ranges.h>
#include <functional>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

#define AS_LITERALEXPR(expr) static_cast<const LiteralExpression &>(expr)
#define AS_BINARYEXPR(expr) static_cast<const BinaryExpression &>(expr)
#define AS_UNARYEXPR(expr) static_cast<const UnaryExpression &>(expr)
#define AS_VECTOREXPR(expr) static_cast<const VectorExpression &>(expr)
#define AS_MAPEXPR(expr) static_cast<const MapExpression &>(expr)

#define MAKE_TRUE new LiteralExpression(Token(TRUE,0,4,"true"),LiteralValue{.boolean_value=true})
#define MAKE_FALSE new LiteralExpression(Token(FALSE,0,5,"false"),LiteralValue{.boolean_value=false})
#define MAKE_NIL new LiteralExpression(Token(VOID,0,0,""),LiteralValue(0))

struct ExpressionHash;
struct ExpressionEqual;

using HashMap = std::unordered_map<std::unique_ptr<Expression>, std::unique_ptr<Expression>, ExpressionHash, ExpressionEqual>;

/// Descibes the type of expression.
/// Used to check for different types.
enum ExpressionType {
    // Literals
    EXPR_STRING,
    EXPR_IDENTIFIER,
    EXPR_NUMBER,
    EXPR_BOOLEAN,
    EXPR_TYPE,
    EXPR_NULL,

    // Unique
    EXPR_VECTOR,
    EXPR_MAP,
    EXPR_LAMBDA,
    EXPR_CALL,

    // Binary
    EXPR_PLUS,
    EXPR_MINUS,
    EXPR_MULTIPLICATION,
    EXPR_DIVISION,
    EXPR_MODULUS,
    EXPR_EQUALS,
    EXPR_NOT_EQUALS,
    EXPR_GREATER,
    EXPR_LESS,
    EXPR_EXPONENT,
    EXPR_DIFFERENTIATE,
    EXPR_LOGIC_AND,
    EXPR_LOGIC_OR,

    // Unary
    EXPR_NEGATE,
    EXPR_LOGIC_NEGATE
};

extern const std::unordered_map<ExpressionType, TokenType> ExpressionTypeToBirchType;
extern const std::unordered_set<ExpressionType> binaryTokens;

/// Used to translate an \ref ExpressionType into a string for debugging or
/// error logging.
extern const char *ExpressionTypeToString[];

// https://stackoverflow.com/questions/2590677/how-do-i-combine-hash-values-in-c0x
inline std::size_t hash_combine(std::size_t seed, std::size_t hash) {
    return seed ^ (hash + 0x9e3779b9 + (seed << 6) + (seed >> 2));
}

/// Abstract base class for all expressions.
class Expression {
public:
    virtual ~Expression() {}

    /// The parent node in the expression tree.
    /// \remarks If the \ref Expression is a parameter
    /// in a \ref CallExpression or a value in a \ref VectorExpression,
    /// then the parent is set to that \ref Expression. This is
    /// done so the \ref ExpressionRewriter can substitute and replace
    /// nodes correctly.
    ///
    /// If this is a nullptr then the node is a root node.
    Expression *parent{nullptr};

    /// Returns the type of expression this is.
    ///
    /// Each class that inherits from \ref Expression will have to implement
    /// this and have it return the appropriate type
    virtual ExpressionType expressionType() const = 0;

    virtual Token &tokenReference() = 0;
    virtual Token tokenClone() const = 0;

    /// [Visitor pattern](https://en.wikipedia.org/wiki/Visitor_pattern)
    /// implementation.
    ///
    /// Calls the appropriate function in the passed visitor.
    /// @param visitor The visitor that should evaluate this \ref Expression
    virtual void accept(IExpressionVisitor &visitor) = 0;

    /// Returns the string representation for this \ref Expression
    virtual std::string toString() const = 0;

    /// Returns a deep clone of the \ref Expression.
    virtual std::unique_ptr<Expression> clone() const = 0;

    /// Checks if another \ref Expression is identical to this one.
    /// Meaning their respective tree structure is identical.
    ///
    /// Equality recursively descends, which means all children must be in the
    /// same order, for the equality to match.
    /// If you want to check if 2 expressions are mathmatically equal, they
    /// must first be
    /// [normalized](https://en.wikipedia.org/wiki/Beta_normal_form)
    virtual bool equal(const Expression &other) const = 0;

    virtual std::size_t hash() const = 0;

    /// Returns the amount of direct descendant nodes this \ref Expression has.
    /// Including itself
    virtual int nodeCount() const = 0;
};

/// Contains the actual data of a \ref LiteralExpression.
union LiteralValue {
    double number_value; /// If the \ref LiteralExpression is a number then this
                         /// field should be used

    bool boolean_value; /// If the \ref LiteralValue is a boolean then this
                        /// field should be used

    const char *string_value; /// If the \ref LiteralValue is a string then this
                              /// field should be used

    TokenType type_value; /// If the \ref LiteralValue is a birch type then this
                          /// field stores the type
};

/// Contains concrete data, such as numbers or strings.
///
/// LiteralExpressions are the leaf nodes in any \ref Expression tree.
class LiteralExpression : public Expression {
private:
    Token literalType_; /// Can be NUMBER, STRING, TRUE, FALSE or any birch type
    const LiteralValue
        value_; /// The actual data contained in the \ref LiteralExpression

public:
    LiteralExpression(Token literalType, LiteralValue value)
        : literalType_(literalType), value_(value) {}

    LiteralExpression(Token literalType, const std::string &str)
        : literalType_{literalType}, value_{stringToLiteral(str)} {}

    virtual ~LiteralExpression() {
        if (this->literalType_.type == STRING ||
            this->literalType_.type == IDENTIFIER) {
            delete this->value_.string_value;
        }
    }

    virtual Token tokenClone() const override { return literalType_; }
    virtual Token &tokenReference() override { return literalType_; }

    /// Helper function that constructs a \ref LiteralValue from a string
    ///
    /// @param str The string to turn into a LiteralValue
    static LiteralValue stringToLiteral(const std::string &str) {
        LiteralValue literal;
        literal.string_value = new char[str.size() + 1];
        strcpy(const_cast<char *>(literal.string_value), str.c_str());
        return literal;
    }

    /// Helper function that constructs a \ref LiteralValue from a string
    ///
    /// @param str The string to turn into a LiteralValue
    static LiteralValue *stringToLiteralHeap(const std::string &str) {
        LiteralValue *literal = new LiteralValue();
        literal->string_value = new char[str.size() + 1];
        strcpy(const_cast<char *>(literal->string_value), str.c_str());
        return literal;
    }

    /// Returns a reference to this expressions value
    const LiteralValue &value() const { return value_; }

    void accept(IExpressionVisitor &visitor) override {
        visitor.visitLiteralExpression(*this);
    }

    virtual ExpressionType expressionType() const override {
        if (typeTokens.contains(literalType_.type))
            return EXPR_TYPE;

        switch (literalType_.type) {
        case IDENTIFIER:
            return EXPR_IDENTIFIER;
        case STRING:
            return EXPR_STRING;
        case NUMBER:
            return EXPR_NUMBER;
        case TRUE:
        case FALSE:
            return EXPR_BOOLEAN;
        case VOID:
            return EXPR_NULL;
        default:
            throw UnimplementedLogic(
                0, 0, fmt::format("expressionType not implemented for {}", TokenTypesToString[literalType_.type]));
        }
    }
  
    virtual std::unique_ptr<Expression> clone() const override {
        // Copy string
        if (this->literalType_.type == STRING ||
            this->literalType_.type == IDENTIFIER) {
            std::string strCopy{this->value_.string_value};
            return std::make_unique<LiteralExpression>(this->literalType_,
                                                       strCopy);
        }
        return std::make_unique<LiteralExpression>(*this);
    }
    virtual bool equal(const Expression &other) const override {
        /* std::cout << "Both null? " << (expressionType() == EXPR_NULL && other.expressionType() == EXPR_NULL) << std::endl; */
        if (other.expressionType() != this->expressionType())
            return false;

        auto otherLiteral = static_cast<const LiteralExpression *>(&other);
        switch (this->expressionType()) {
        case EXPR_NULL:
            return other.expressionType() == EXPR_NULL;
        case EXPR_IDENTIFIER:
        case EXPR_STRING:
            return strcmp(this->value_.string_value,
                          otherLiteral->value().string_value) == 0;
        case EXPR_NUMBER:
            return this->value_.number_value ==
                   otherLiteral->value().number_value;
        case EXPR_BOOLEAN:
            return this->value_.boolean_value ==
                   otherLiteral->value().boolean_value;
        case EXPR_TYPE:
            return this->value_.type_value ==
                   otherLiteral->value().type_value;
        default:
            throw std::logic_error("Literal expression is an illegal type");
        }
    }

    virtual std::size_t hash() const override {
        switch (literalType_.type) {
            case IDENTIFIER:
            case STRING:
                return std::hash<std::string>{}(value_.string_value);
            case NUMBER:
                return std::hash<double>{}(value_.number_value);
            case TRUE:
            case FALSE:
                return std::hash<bool>{}(value_.boolean_value);
            case VOID:
                return std::hash<std::nullptr_t>{}(nullptr);
            default:
                throw UnimplementedLogic(
                        0, 0, "expressionType not implemented for literal");
        }
    }

    TokenType type() const { return literalType_.type; }
    virtual std::string toString() const override {
        switch (this->expressionType()) {
        case EXPR_NULL:
            return "nulllll";
        case EXPR_IDENTIFIER:
        case EXPR_STRING:
            return this->value_.string_value;
        case EXPR_NUMBER:
            return fmt::format("{}", this->value_.number_value);
        case EXPR_BOOLEAN:
            return this->value_.boolean_value ? "true" : "false";
        case EXPR_TYPE:
            return TokenTypesToString[this->value_.type_value];
        default:
            throw std::logic_error("Literal expression is an illegal type");
        }
    }
    virtual int nodeCount() const override { return 1; }
};

class CallExpression : public Expression {
private:
    std::unique_ptr<LiteralExpression> lambdaName_;
    std::vector<std::unique_ptr<Expression>> parameterList_;

public:
    CallExpression(std::unique_ptr<LiteralExpression> lambda_name_,
                   std::vector<std::unique_ptr<Expression>> parameters_)
        : lambdaName_(std::move(lambda_name_)),
          parameterList_(std::move(parameters_)) {
        setParents();
    }

    void setParents() {
        for (size_t i = 0; i < parameterList_.size(); i++)
            parameterList_[i]->parent = this;
    }

    std::vector<std::unique_ptr<Expression>> obtainParameters() {
        return std::move(parameterList_);
    }
    std::unique_ptr<LiteralExpression> obtainLambdaName() {
        return std::move(lambdaName_);
    }
    const std::vector<std::unique_ptr<Expression>> &parameters() const {
        return parameterList_;
    }
    LiteralExpression &lambdaName() const { return *lambdaName_; }

    virtual ExpressionType expressionType() const override {
        return EXPR_CALL;
    };

    virtual Token tokenClone() const override { return lambdaName_->tokenClone(); }
    virtual Token &tokenReference() override { return lambdaName_->tokenReference(); }

    virtual void accept(IExpressionVisitor &visitor) override {
        visitor.visitCallExpression(*this);
    };

    virtual std::string toString() const override {
        std::string arguments;
        if (parameterList_.size() > 0) {
            for (size_t i = 0; i < (parameterList_.size() - 1); i++) {
                arguments += parameterList_[i]->toString() + ", ";
            }
            arguments += parameterList_.back()->toString();
        }

        return fmt::format("{}({})", lambdaName_->toString(), arguments);
    };

    virtual std::unique_ptr<Expression> clone() const override {
        auto tmp = lambdaName_->clone();
        auto identifier_clone = static_cast<LiteralExpression *>(tmp.release());
        auto ptr = std::unique_ptr<LiteralExpression>(identifier_clone);

        std::vector<std::unique_ptr<Expression>> clonedParameters;
        clonedParameters.reserve(parameterList_.size());
        for (size_t i = 0; i < parameterList_.size(); i++) {
            clonedParameters.push_back(parameterList_[i]->clone());
        }

        return std::make_unique<CallExpression>(std::move(ptr),
                                                std::move(clonedParameters));
    };

    virtual bool equal(const Expression &other) const override {
        if (other.expressionType() != this->expressionType())
            return false;

        const CallExpression &other_ =
            static_cast<const CallExpression &>(other);

        if (strcmp(other_.lambdaName_->value().string_value,
                   lambdaName_->value().string_value) != 0)
            return false;

        // Should have same argument count
        if (other_.parameters().size() != parameterList_.size())
            return false;

        // All arguments should match
        for (size_t i = 0; i < parameterList_.size(); i++) {
            if (!other_.parameters()[i]->equal(*parameterList_[i].get()))
                return false;
        }

        return true;
    };

    virtual std::size_t hash() const override {
        size_t seed{lambdaName_->hash()};
        for (const auto &param : parameterList_) {
            seed = hash_combine(seed, param->hash());
        }
        return seed;
    }

    void setLambdaName(Token newName) {
        lambdaName_ =
            std::make_unique<LiteralExpression>(newName, newName.literal);
    }

    virtual int nodeCount() const override {
        int parametersNodeCountSum{0};
        for (size_t i = 0; i < parameterList_.size(); i++)
            parametersNodeCountSum += parameterList_[i]->nodeCount();

        return 1 + parametersNodeCountSum;
    };

    void setParameter(size_t idx, std::unique_ptr<Expression> newParam) {
        assert(idx < parameterList_.size());
        parameterList_[idx] = std::move(newParam);
    }

    void setLambdaName(std::string newName) {
        lambdaName_ = std::make_unique<LiteralExpression>(
            Token(TokenType::IDENTIFIER, 0, 0, newName), newName);
    }
};



class LambdaExpression : public Expression {
private:
    std::vector<std::string> boundVariables_;
    std::unique_ptr<Expression> body_;

public:
    LambdaExpression(std::vector<std::string> boundVariables,
                     std::unique_ptr<Expression> body)
        : boundVariables_(boundVariables), body_(std::move(body)){};

    std::vector<std::string> &boundVariables() { return boundVariables_; }
    Expression &body() { return *body_.get(); }
    virtual ExpressionType expressionType() const override {
        return EXPR_LAMBDA;
    }

    virtual Token tokenClone() const override { return body_->tokenClone(); }
    virtual Token &tokenReference() override { return body_->tokenReference(); }

    virtual void accept(IExpressionVisitor &visitor) override {
        visitor.visitLambdaExpression(*this);
    }

    virtual std::string toString() const override {
        return fmt::format("{} -> {}", fmt::join(boundVariables_, ", "),
                           body_->toString());
    }

    virtual std::unique_ptr<Expression> clone() const override {
        return std::make_unique<LambdaExpression>(boundVariables_,
                                                  body_->clone());
    }

    virtual bool equal(const Expression &other) const override {
        if (other.expressionType() != this->expressionType())
            return false;

        const LambdaExpression &other_ =
            static_cast<const LambdaExpression &>(other);

        return other_.boundVariables_ == boundVariables_ &&
               other_.body_->equal(*body_);
    }

    virtual std::size_t hash() const override {
        size_t seed{body_->hash()};
        for (const auto &var : boundVariables_) {
            seed = hash_combine(seed, std::hash<std::string>{}(var));
        }
        return seed;
    }

    virtual int nodeCount() const override { return 1; }
};

class VectorExpression : public Expression {
    Token token_;
    std::vector<std::unique_ptr<Expression>> values_;

public:
    VectorExpression(Token start,
                     std::vector<std::unique_ptr<Expression>> values)
        : token_(start), values_(std::move(values)) {
        for (size_t i = 0; i < values_.size(); i++) {
            values_[i]->parent = this;
        }
    }

    inline size_t size() const { return values_.size(); }
    virtual ExpressionType expressionType() const override {
        return EXPR_VECTOR;
    }

    virtual Token tokenClone() const override { return token_; }
    virtual Token &tokenReference() override { return token_; }
    std::vector<std::unique_ptr<Expression>> cloneVector() const {
        std::vector<std::unique_ptr<Expression>> clonedValues;
        clonedValues.reserve(size());
        for (size_t i = 0; i < size(); i++) {
            clonedValues.push_back(values_[i]->clone());
        }
        return clonedValues;
    }

    inline Expression &get(size_t index) {
        assert(index < size() && "Indexing vector out of range");
        return *(values_.at(index));
    }

    inline Expression *obtain(size_t index) {
        assert(index < size() && "Indexing vector out of range");
        return values_.at(index).release();
    }

    inline const Expression &get_const(size_t index) const {
        assert(index < size() && "Indexing vector out of range");
        return *(values_.at(index));
    }

    inline void set(size_t index, std::unique_ptr<Expression> value) {
        assert(index < size() && "Indexing vector out of range");
        value->parent = this;
        values_[index] = std::move(value);
    }

    virtual std::unique_ptr<Expression> clone() const override {
        std::vector<std::unique_ptr<Expression>> clonedValues;
        clonedValues.reserve(size());
        for (size_t i = 0; i < size(); i++) {
            clonedValues.push_back(values_[i]->clone());
        }
        return std::make_unique<VectorExpression>(token_,
                                                  std::move(clonedValues));
    }

    virtual bool equal(const Expression &other) const override {
        if (other.expressionType() != this->expressionType())
            return false;

        auto vector = static_cast<const VectorExpression *>(&other);

        if (vector->size() != this->size())
            return false;

        for (size_t i = 0; i < size(); i++) {
            if (!vector->get_const(i).equal(get_const(i)))
                return false;
        }

        return true;
    }

    virtual std::size_t hash() const override {
        size_t seed{0};
        for (const auto &element : values_) {
            seed = hash_combine(seed, element->hash());
        }
        return seed;
    }

    void accept(IExpressionVisitor &visitor) override {
        visitor.visitVectorExpression(*this);
    }

    virtual std::string toString() const override {
        std::vector<std::string> values;
        values.reserve(size());

        for (size_t i = 0; i < size(); i++) {
            const Expression &e = get_const(i);
            values.push_back(e.toString());
        }
        return fmt::format("[{}]", fmt::join(values, ", "));
    }

    virtual int nodeCount() const override {
        int valuesNodeCountSum{0};
        for (size_t i = 0; i < size(); i++)
            valuesNodeCountSum += values_[i]->nodeCount();
        return 1 + valuesNodeCountSum;
    }
};

struct ExpressionHash {
    std::size_t operator()(const std::unique_ptr<Expression>& expr) const {
        return expr->hash();
    }
};

struct ExpressionEqual {
    bool operator()(const std::unique_ptr<Expression>& lhs, const std::unique_ptr<Expression>& rhs) const {
        return lhs->equal(*rhs);
    }
};

class MapExpression : public Expression {
private:
    Token token;
    HashMap hashMap;

public:
    MapExpression(Token token_, HashMap hashMap_) : token(token_), hashMap(std::move(hashMap_)) {}

    virtual Token tokenClone() const override { return token; }
    virtual Token &tokenReference() override { return token; }

    virtual ExpressionType expressionType() const override {
        return EXPR_MAP;
    }

    virtual std::unique_ptr<Expression> clone() const override {
        HashMap cloned;
        cloned.reserve(hashMap.size());
        for (const auto &[key, value] : hashMap) {
            cloned[key->clone()] = value->clone();
        }

        return std::make_unique<MapExpression>(token, std::move(cloned));
    }

    virtual bool equal(const Expression &other) const override {
        if (this->expressionType() != other.expressionType())
            return false;

        const auto &otherMap = static_cast<const MapExpression &>(other);
        if (this->mapReference().size() != otherMap.mapReference().size())
            return false;

        // Make sure each key and value exists and are equal
        for (const auto &[key, value] : hashMap) {
            if (auto it = otherMap.mapReference().find(key); it != otherMap.mapReference().cend()) {
                if (!value->equal(*it->second)) {
                    return false;
                }
            } else {
                // Key not found in other
                return false;
            }
        }

        return true;
    }

    virtual std::size_t hash() const override {
        size_t seed{0};
        for (const auto &[key, value] : hashMap) {
            seed = hash_combine(seed, key->hash());
            seed = hash_combine(seed, value->hash());
        }
        return seed;
    }

    virtual void accept(IExpressionVisitor &visitor) override {
        visitor.visitMapExpression(*this);
    }

    virtual std::string toString() const override {
        return "map";
    }

    virtual int nodeCount() const override {
        return 1;
    }

    const HashMap &mapReference() const { return hashMap; }
    void insert(std::unique_ptr<Expression> key, std::unique_ptr<Expression> value) {
        hashMap[std::move(key)] = std::move(value);
    }
};

/// \ref Expression for any Binary operation (plus, minus, multiplication etc.)
/// \ref BinaryExpression is also used for differentiation, where
/// the variable with respect to differentiation is stored as a
/// \ref LiteralExpression on the lhs (\ref BinaryExpression::left_) and
/// the \ref Expression to differentiate is stored on the rhs (\ref
/// BinaryExpression::right_)
class BinaryExpression : public Expression {
private:
    std::unique_ptr<Expression> left_;
    Token op_;
    std::unique_ptr<Expression> right_;

public:
    BinaryExpression(std::unique_ptr<Expression> left, Token token,
                     std::unique_ptr<Expression> right)
        : left_(std::move(left)), op_(std::move(token)),
          right_(std::move(right)) {
        left_->parent = this;
        right_->parent = this;
    }
    virtual std::string toString() const override {
        switch (op_.type) {
        case D_OVER_D_IDENTIFER:
            return fmt::format("d/d{} {}", left_->toString(),
                               right_->toString());
        default:
            return "(" + left_->toString() + op_.literal +
                   right_->toString() + ")";
        }
    }

    virtual Token tokenClone() const override { return op_; }
    virtual Token &tokenReference() override { return op_; }

    void accept(IExpressionVisitor &visitor) override {
        visitor.visitBinaryExpression(*this);
    }

    virtual ExpressionType expressionType() const override {
        switch (op_.type) {
        case PLUS:
            return EXPR_PLUS;
        case MINUS:
            return EXPR_MINUS;
        case STAR:
            return EXPR_MULTIPLICATION;
        case SLASH:
            return EXPR_DIVISION;
        case MODULUS:
            return EXPR_MODULUS;
        case EQUALS:
            return EXPR_EQUALS;
        case BANG_EQUALS:
            return EXPR_NOT_EQUALS;
        case GREATER:
            return EXPR_GREATER;
        case LESS:
            return EXPR_LESS;
        case EXPONENT:
            return EXPR_EXPONENT;
        case D_OVER_D_IDENTIFER:
            return EXPR_DIFFERENTIATE;
        case AND:
            return EXPR_LOGIC_AND;
        case OR:
            return EXPR_LOGIC_OR;
        default:
            throw UnimplementedLogic(op_, "can't find expr type");
        }
    }

    virtual bool equal(const Expression &other) const override {
        if (other.expressionType() != this->expressionType())
            return false;

        auto binary = static_cast<const BinaryExpression *>(&other);
        if (binary->op().type != this->op().type)
            return false;
        return binary->left().equal(this->left()) &&
               binary->right().equal(this->right());
    }

    virtual std::size_t hash() const override {
        size_t seed{std::hash<int>{}(static_cast<int>(op_.type))};
        seed = hash_combine(seed, left_->hash());
        seed = hash_combine(seed, right_->hash());
        return seed;
    }

    virtual std::unique_ptr<Expression> clone() const override {
        auto leftCopy = left_->clone();
        auto rightCopy = right_->clone();
        return std::make_unique<BinaryExpression>(std::move(leftCopy), op_,
                                                  std::move(rightCopy));
    }

    virtual int nodeCount() const override {
        if (op_.type == D_OVER_D_IDENTIFER)
            return 1 + right_->nodeCount();

        return 1 + left_->nodeCount() + right_->nodeCount();
    }

    Expression &left() const { return *left_; }
    const Token &op() const { return op_; }
    Expression &right() const { return *right_; }

    void setLeft(std::unique_ptr<Expression> value) {
        left_ = std::move(value);
        left_->parent = this;
    }
    void setRight(std::unique_ptr<Expression> value) {
        right_ = std::move(value);
        right_->parent = this;
    }
};

class UnaryExpression : public Expression {
private:
    Token op_;
    std::unique_ptr<Expression> operand_;

public:
    UnaryExpression(Token op, std::unique_ptr<Expression> right)
        : op_(op), operand_(std::move(right)) {
        operand_->parent = this;
    }

    void accept(IExpressionVisitor &visitor) override {
        visitor.visitUnaryExpression(*this);
    }

    virtual ExpressionType expressionType() const override {
        switch (op_.type) {
        case MINUS:
            return EXPR_NEGATE;
        case NOT:
            return EXPR_LOGIC_NEGATE;
        default:
            throw UnimplementedLogic(op_, "Unhandled unary expressiontype");
        }
    }

    virtual Token tokenClone() const override { return op_; }
    virtual Token &tokenReference() override { return op_; }

    virtual bool equal(const Expression &other) const override {
        if (other.expressionType() != this->expressionType())
            return false;

        auto unary = static_cast<const UnaryExpression *>(&other);
        if (unary->op().type != this->op().type)
            return false;
        return this->operand_->equal(unary->operand());
    }

    virtual std::size_t hash() const override {
        size_t seed{std::hash<int>{}(static_cast<int>(op_.type))};
        seed = hash_combine(seed, operand_->hash());
        return seed;
    }

    virtual std::unique_ptr<Expression> clone() const override {
        auto operandCopy = operand_->clone();
        return std::make_unique<UnaryExpression>(op_, std::move(operandCopy));
    }

    const Token &op() const { return op_; }
    Expression &operand() const { return *operand_; }
    void setOperand(std::unique_ptr<Expression> value) {
        operand_ = std::move(value);
        operand_->parent = this;
    }

    virtual std::string toString() const override {
        switch (op_.type) {
        case MINUS:
        case NOT:
            return op_.literal + operand_->toString();

        // Default formatting
        default:
            return operand_->toString() + op_.literal;
        }
    }

    virtual int nodeCount() const override { return 1 + operand_->nodeCount(); }
};

#endif
