#ifndef PARSER_ERRORS
#define PARSER_ERRORS

#include "carl_constants.h"
#include "lexer/token.h"

#include <cassert>
#include <fmt/core.h>
#include <format>
#include <string>
#include <tuple>

// Base Lexer exception
class ParserException {
    public:
        const size_t sourceOffset_;
        const size_t length_;
        std::tuple<size_t, size_t> calculateLineNumber(const std::string_view source, const size_t sourceOffset) const {
            assert((source.size() >= sourceOffset) && "Could not calculate line number since the offset exceeds the bounds of the source string given");

            size_t lineNumber{0};
            size_t offsetToLineNumber{0};
            for (size_t offset = 0; offset != sourceOffset; offset++) {
                if (source[offset] == '\n') {
                    lineNumber++;
                    offsetToLineNumber = offset;
                }
            }
            return std::make_tuple(lineNumber, sourceOffset - offsetToLineNumber);
        }
        std::string locationPrefix(std::string_view source) const {
            size_t startLineNumber;
            size_t startCharacterIndex;
            std::tie(startLineNumber, startCharacterIndex) = calculateLineNumber(source, sourceOffset_);

            size_t endLineNumber;
            size_t endCharacterIndex;
            std::tie(endLineNumber, endCharacterIndex) = ParserException::calculateLineNumber(source, sourceOffset_ + length_);
            return std::format("{}:{} - {}:{}", startLineNumber, startCharacterIndex, endLineNumber, endCharacterIndex);
        }
    public:
        ParserException(const size_t sourceOffset, const size_t length) : sourceOffset_{sourceOffset}, length_{length} {}

        virtual std::string what(std::string_view source) const noexcept = 0;
        virtual ParserError error_code() const noexcept = 0;
};

class SyntaxError : public ParserException {
    private:
        const Token token_;
        const std::string msg_;
    public:
        SyntaxError(const Token token) : ParserException(token.sourceOffset, token.length), token_{token} {}
        SyntaxError(const Token token, std::string msg) : ParserException(token.sourceOffset, token.length), token_{token}, msg_{msg} {}

        ParserError error_code() const noexcept override {
            return ParserError::SYNTAX_ERROR;
        }

        std::string what(std::string_view source) const noexcept override {
            return std::string{locationPrefix(source) + " Syntax Error at '" + token_.literal + "' " + msg_};
        }
};

class UnexpectedToken : public ParserException {
    private:
        const Token token_;
    public:
        UnexpectedToken(const Token token) : ParserException(token.sourceOffset, token.length), token_{token} {}

        ParserError error_code() const noexcept override {
            return ParserError::UNEXPECTED_TOKEN;
        }

        std::string what(std::string_view source) const noexcept override {
            return std::string{locationPrefix(source) + " Unexpected Token '" + token_.literal + "'"};
        }
};

class MissingClosingBracket : public ParserException {
    public:
        MissingClosingBracket(const size_t sourceOffset, const size_t length) : ParserException(sourceOffset, length) {}

        ParserError error_code() const noexcept override {
            return ParserError::MISSING_CLOSING_BRACKET;
        }

        std::string what(std::string_view source) const noexcept override {
            return std::string{"Missing Closing Bracket at: " + locationPrefix(source)};
        }
};

class MissingTerminator : public ParserException {
    private:
        const Token token_;
    public:
        MissingTerminator(const Token token) : ParserException(token.sourceOffset, token.length), token_{token} {}

        ParserError error_code() const noexcept override {
            return ParserError::MISSING_TERMINATOR;
        }

        std::string what(std::string_view source) const noexcept override {
            return std::string{locationPrefix(source) + " Missing terminator after '" + token_.literal + "'"};
        }
};

class UnimplementedParser : public ParserException {
    private:
        const Token token_;
    public:
        UnimplementedParser(const Token token) : ParserException(token.sourceOffset, token.length), token_{token} {}

        ParserError error_code() const noexcept override {
            return ParserError::UNIMPLEMENTED_PARSER;
        }

        std::string what(std::string_view source) const noexcept override {
            return std::string{locationPrefix(source) + " Unimplemented parser logic for '" + token_.literal + "'"};
        }
};
#endif
