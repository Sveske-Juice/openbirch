#ifndef STATEMENT_H
#define STATEMENT_H

#include "parser/expression.h"
#include "parser/istatement_visitor.h"
#include <memory>

/// A piece of birch code that can either evaluate to an \ref Expression
/// or have a side effect - change the state of the program.
class Statement {
public:
    virtual void accept(IStatementVisitor &visitor) = 0;
};

using CodeBlock = std::vector<std::unique_ptr<Statement>>;

/// Wrapper around an \ref Expression.
class ExpressionStatement : public Statement {
private:
    std::unique_ptr<Expression> expression_;

public:
    ExpressionStatement(std::unique_ptr<Expression> expression)
        : expression_{std::move(expression)} {}
    virtual void accept(IStatementVisitor &visitor) override {
        visitor.visitExpressionStatement(*this);
    }
    Expression &expression() { return *expression_; }
    std::unique_ptr<Expression> obtainExpression() {
        return std::move(expression_);
    }
};

/// Declaring a function or a variable
class DeclarationStatement : public Statement {
private:
    std::string variableName_;
    std::unique_ptr<Expression> value_;

public:
    DeclarationStatement(std::string variableName,
                         std::unique_ptr<Expression> value)
        : variableName_{variableName}, value_{std::move(value)} {}

    virtual void accept(IStatementVisitor &visitor) override {
        visitor.visitDeclaration(*this);
    }

    std::string variableName() const { return variableName_; }
    std::unique_ptr<Expression> move_value() { return std::move(value_); }
    Expression &value() { return *value_; }
};

class ProcedureStatement : public Statement {
private:
    Token token_;
    Token name_;
    std::vector<Token> arguments_;
    std::vector<std::unique_ptr<Statement>> body_;

public:
    ProcedureStatement(Token token, Token name, std::vector<Token> arguments,
                       std::vector<std::unique_ptr<Statement>> body)
        : token_(token), name_(name), arguments_(arguments),
          body_(std::move(body)) {}

    virtual void accept(IStatementVisitor &visitor) override {
        visitor.visitProcedure(*this);
    }

    const Token &token() { return token_; }
    const Token &name() { return name_; }
    const std::vector<Token> &arguments() { return arguments_; }

    std::vector<std::unique_ptr<Statement>> &body() { return body_; }
    std::vector<std::unique_ptr<Statement>> move_body() {
        return std::move(body_);
    }
};

class ReturnStatement : public Statement {
private:
    Token token_;
    std::unique_ptr<Expression> returnValue_;

public:
    ReturnStatement(Token token, std::unique_ptr<Expression> returnValue)
        : token_(token), returnValue_(std::move(returnValue)) {}

    virtual void accept(IStatementVisitor &visitor) override {
        visitor.visitReturn(*this);
    }

    const Token &token() { return token_; }

    Expression &returnValue() { return *returnValue_; }
    std::unique_ptr<Expression> move_returnValue() {
        return std::move(returnValue_);
    }
};

using ConditionalBlock = std::pair<std::unique_ptr<Expression>, CodeBlock>;

class IfStatement : public Statement {
private:
    Token token_;

    ConditionalBlock ifBranch_;
    std::vector<ConditionalBlock> elifBranches_;
    CodeBlock elseBranch_;

public:
    IfStatement(Token token, ConditionalBlock ifBranch,
                std::vector<ConditionalBlock> elifBranches = {},
                CodeBlock elseBranch = {})
        : token_(token), ifBranch_(std::move(ifBranch)),
          elifBranches_(std::move(elifBranches)),
          elseBranch_(std::move(elseBranch)) {}

    virtual void accept(IStatementVisitor &visitor) override {
        visitor.visitIfStatement(*this);
    }

    const Token &token() { return token_; }
    auto ifCondition() { return std::get<0>(ifBranch_)->clone(); }
    const CodeBlock &ifBranch() { return std::get<1>(ifBranch_); }
    const CodeBlock &elseBranch() { return elseBranch_; }
    const std::vector<ConditionalBlock> &elifBranches() {
        return elifBranches_;
    }
};

class ImportStatement : public Statement {
private:
    Token import_token;
    std::unique_ptr<Expression> expression_;

public:
    ImportStatement(Token &import_token_,
                    std::unique_ptr<Expression> expression)
        : import_token(import_token_), expression_{std::move(expression)} {}

    virtual void accept(IStatementVisitor &visitor) override {
        visitor.visitImportStatement(*this);
    }

    Expression &expression() const { return *expression_.get(); }

    const Token &token() const { return import_token; }
};

#endif
