#ifndef IEXPRESSION_VISITOR_H
#define IEXPRESSION_VISITOR_H

class Expression;
class BinaryExpression;
class LiteralExpression;
class VectorExpression;
class MapExpression;
class LambdaExpression;
class CallExpression;
class UnaryExpression;

class IExpressionVisitor {
    public:
        virtual ~IExpressionVisitor() {}

        virtual void visitLiteralExpression(LiteralExpression& expression) = 0;
        virtual void visitVectorExpression(VectorExpression& expression) = 0;
        virtual void visitMapExpression(MapExpression& expression) = 0;
        virtual void visitLambdaExpression(LambdaExpression& expression) = 0;
        virtual void visitCallExpression(CallExpression& expression) = 0;
        virtual void visitBinaryExpression(BinaryExpression& expression) = 0;
        virtual void visitUnaryExpression(UnaryExpression& expression) = 0;
};

#endif
