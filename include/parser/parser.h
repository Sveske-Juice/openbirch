#ifndef PARSER_H
#define PARSER_H

#include "lexer/token.h"
#include "parser/expression.h"
#include "parser/statement.h"

#include <cstddef>
#include <memory>
#include <vector>

/// The \ref Parser is responsible for taking the list of \ref Token "Tokens"
/// and producing a tree structure of expressions.
class Parser {
    private:
        const std::vector<Token> lexemes;
        size_t currentIndex{0};
        void consume();
        bool matchAny(std::initializer_list<TokenType> tokenTypes);

        template<typename Iterator>
        bool matchAny(Iterator begin, Iterator end);
        bool isOfType(TokenType typeToCheck, std::initializer_list<TokenType> types) const;
        bool isAtEnd() const;
        Token previous() const;
        Token peek() const;
        Token peek(size_t amount) const;

        std::unique_ptr<Statement> command();
        std::unique_ptr<Statement> importStatement();

        std::unique_ptr<Statement> statement();
        std::unique_ptr<Statement> ifStatement();
        std::unique_ptr<Statement> declarationStatement(std::unique_ptr<Expression> precalculated = nullptr);
        std::unique_ptr<Statement> expressionStatement(std::unique_ptr<Expression> precalculated = nullptr);
        std::unique_ptr<Statement> procedureStatement();
        std::unique_ptr<Statement> returnStatement();

        std::unique_ptr<Expression> expression();
        std::unique_ptr<Expression> logic_or();
        std::unique_ptr<Expression> logic_and();
        std::unique_ptr<Expression> equality();
        std::unique_ptr<Expression> comparison();
        std::unique_ptr<Expression> term();
        std::unique_ptr<Expression> factor();
        std::unique_ptr<Expression> unary();
        std::unique_ptr<Expression> exponent();
        std::unique_ptr<Expression> differentiation();
        std::unique_ptr<Expression> call();
        std::unique_ptr<Expression> lambda();
        std::unique_ptr<Expression> primary();

    public:
        Parser(const std::vector<Token> _lexemes) : lexemes{std::move(_lexemes)} {}
        std::vector<std::unique_ptr<Statement>> parse();
};

#endif
