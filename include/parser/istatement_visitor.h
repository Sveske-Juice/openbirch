#ifndef ISTATEMENT_VISITOR_H
#define ISTATEMENT_VISITOR_H

class ExpressionStatement;
class DefineStatement;
class DefineSupersetStatement;
class DefineIfRuleStatement;
class ApplyStatement;
class ShowStatement;
class ImportStatement;
class DeclarationStatement;
class ProcedureStatement;
class IfStatement;
class ReturnStatement;

class IStatementVisitor {
    public:
        virtual ~IStatementVisitor() {}

        virtual void visitExpressionStatement(ExpressionStatement& statement) = 0;
        virtual void visitDeclaration(DeclarationStatement& statement) = 0;
        virtual void visitProcedure(ProcedureStatement& statement) = 0;
        virtual void visitIfStatement(IfStatement& statement) = 0;
        virtual void visitReturn(ReturnStatement& statement) = 0;
        virtual void visitImportStatement(ImportStatement& statement) = 0;
};

#endif
