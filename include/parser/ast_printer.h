#ifndef AST_PRINTER_H
#define AST_PRINTER_H

#include "parser/iexpression_visitor.h"

#include <stack>
#include <string>

class AstPrinter : public IExpressionVisitor {
private:
    /// If -1 then print as many digits as needed, otherwise print the exact amount of decimals.
    const int decimals;
    const bool spacing;
    std::stack<std::string> workingStack;

public:
    AstPrinter(int decimals_ = -1, bool spacing_ = false);
    std::string print(const Expression &expression);
    std::string print(Expression &expression);
    void visitLiteralExpression(LiteralExpression &expression) override;
    void visitVectorExpression(VectorExpression &expression) override;
    void visitMapExpression(MapExpression &expression) override;
    void visitLambdaExpression(LambdaExpression &expression) override;
    void visitCallExpression(CallExpression &expression) override;
    void visitBinaryExpression(BinaryExpression &expression) override;
    void visitUnaryExpression(UnaryExpression &expression) override;
};

#endif
