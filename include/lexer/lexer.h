#ifndef LEXER_H
#define LEXER_H

#include "token.h"

#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using TokenMap = std::unordered_map<std::string, TokenType>;

extern const std::unordered_set<TokenType> typeTokens;
extern const TokenMap keywords;

/// The lexer is responsible for taking the source code string and transforming
/// it into a list of tokens for the parser to work with.
class Lexer {
public:
    Lexer(std::string source);

    /// Transform the source into a list of tokens.
    std::vector<Token> tokenize();

    /// Turns a list of tokens back into a string.
    static std::string tokensToString(const std::vector<Token>& tokens);

private:
    /// The source code the lexer will turn into tokens
    const std::string source;

    /// The start of the current token
    size_t start{0};
    /// The current character being processed for tokenization.
    size_t currentCharacterIndex{0};

    /// Checks whether a character is in the alphabet or other variable legal letters
    bool isAlpha(char c) const;
    /// Checks whether a number is in the alphabet or is a number
    bool isAlphaNumeric(char c) const;
    /// Checks whether or not the character set has been exhausted
    bool isAtEndOfFile() const;
    /// Checks what character lies ahead without consuming them
    /// @param ahead how many characters to look ahead
    char peek(size_t ahead = 0) const;
    /// Sets start to currentCharacterIndex + 1. Effectively moving on to the next character
    char consume();
    /// Check if the next character matches expected, and then consumes it.
    /// @param expected the character to look for
    bool match(char expected);
};

#endif
