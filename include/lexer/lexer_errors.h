#ifndef LEXER_ERRORS_H
#define LEXER_ERRORS_H

#include "carl_constants.h"
#include "lexer/token.h"

#include <cstddef>
#include <string_view>
#include <cassert>
#include <format>
#include <tuple>

// Base Lexer exception
class LexerException {
    public:
        const size_t sourceOffset_;
        const size_t length_;
        static inline std::tuple<size_t, int> calculateLineNumber(const std::string_view source, const size_t sourceOffset) {
            // assert((source.size() >= sourceOffset) && "Could not calculate line number since the offset exceeds the bounds of the source string given");

            size_t lineNumber{0};
            size_t offsetToLineNumber{0};
            for (size_t offset = 0; offset != sourceOffset; offset++) {
                if (source[offset] == '\n') {
                    lineNumber++;
                    offsetToLineNumber = offset;
                }
            }
            return std::make_tuple(lineNumber, sourceOffset - offsetToLineNumber);
        }
        std::string locationPrefix(std::string_view source) const {
            size_t startLineNumber;
            size_t startCharacterIndex;
            std::tie(startLineNumber, startCharacterIndex) = LexerException::calculateLineNumber(source, sourceOffset_);

            size_t endLineNumber;
            size_t endCharacterIndex;
            std::tie(endLineNumber, endCharacterIndex) = LexerException::calculateLineNumber(source, sourceOffset_ + length_);
            return std::format("{}:{} - {}:{}", startLineNumber, startCharacterIndex, endLineNumber, endCharacterIndex);
        }
    public:
        LexerException(const size_t sourceOffset, const size_t length) : sourceOffset_{sourceOffset}, length_{length} {}
        LexerException(const Token& token) : sourceOffset_{token.sourceOffset}, length_{token.length} {}

        virtual std::string what(std::string_view source) const noexcept = 0;
        virtual LexerError error_code() const noexcept = 0;
};


class UnterminatedString : public LexerException {
    public:
        UnterminatedString(const int sourceOffset, const int length) : LexerException(sourceOffset, length) {}
        UnterminatedString(const Token& token) : LexerException(token) {}

        LexerError error_code() const noexcept override {
            return LexerError::UNTERMINATED_STRING;
        }

        std::string what(std::string_view source) const noexcept override {
            return std::string{locationPrefix(source) + " Unterminated String"};
        }
};

class IncompleteDelimiter : public LexerException {
    public:
        IncompleteDelimiter(const int sourceOffset, const int length) : LexerException(sourceOffset, length) {}
        IncompleteDelimiter(const Token& token) : LexerException(token) {}

        LexerError error_code() const noexcept override {
            return LexerError::INCOMPLETE_DELIMITER;
        }

        std::string what(std::string_view source) const noexcept override {
            return std::string{locationPrefix(source) + " Unfinished delimiter"};
        }
};

class UnrecognizedCharacter : public LexerException {
    public:
        UnrecognizedCharacter(const int sourceOffset, const int length) : LexerException(sourceOffset, length) {}
        UnrecognizedCharacter(const Token& token) : LexerException(token) {}

        LexerError error_code() const noexcept override {
            return LexerError::UNRECOGNIZED_CHARACTER;
        }

        std::string what(std::string_view source) const noexcept override {
            return std::string{locationPrefix(source) + " Unrecognized Character '" + source[sourceOffset_] + "'"};
        }
};

#endif
