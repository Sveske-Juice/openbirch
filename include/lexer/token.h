#ifndef TOKEN_H
#define TOKEN_H

#include <cstddef>
#include <string>

/// Since Enums can't be turned into strings, all values for the enum are
/// encoded as strings here
extern const std::string TokenTypesToString[];

/// Defines the type of token.
enum TokenType {
    STRING, // "stuff enclosed in quotes"
    NUMBER, // numbers

    IDENTIFIER, // variable, function, etc.
    IMPORT,     // import another file containing valid openbirch statements

    // Operators
    PLUS,               // +
    MINUS,              // -
    STAR,               // *
    SLASH,              // /
    MODULUS,            // %
    EXPONENT,           // ^
    D_OVER_D_IDENTIFER, // d/dx where x is some identifier (handled in parser)

    // Brackets
    LPAREN,  // (
    RPAREN,  // )
    LSQUARE, // [
    RSQUARE, // ]
    LCURLY,  // {
    RCURLY,  // }

    EQUALS,         // =
    COLON_EQUAL,    // :=
    LESS,           // <
    GREATER,        // >
    EQUALS_EQUALS,  // ==
    LESS_EQUALS,    // <=
    GREATER_EQUALS, // >=
    BANG_EQUALS,    // !=
    ARROW,          // ->
    SINGLE_QUOTE,   // '

    // Characters
    COLON,      // :
    SEMICOLON,  // ;
    COMMA,      // ,
    DOT,        // .
    BANG,       // !
    AMPER_SAND, // &

    // Token Keywords
    NOT,       // not
    AND,       // and
    OR,        // or
    IF,        // if
    ELSE_IF,   // else if
    ELSE,      // else
    THEN,      // then
    WHILE,     // while
    FOR,       // for
    TRUE,      // true
    FALSE,     // false
    VOID,      // Null (NULL is reserved in C/C++)
    END,       // Used to end statements like procedures, if statements etc.
    
    // Types of expressions in birch
    TYPE_NULL,
    TYPE_NUMBER,
    TYPE_STRING,
    TYPE_BOOLEAN,
    TYPE_IDENTIFIER, // Variable or undefined identifer
    TYPE_FUNCTION,  // Defined identifier to be a function
    TYPE_VECTOR,
    TYPE_MAP,

    // Expr types
    TYPE_PLUS,
    TYPE_MINUS,
    TYPE_MULTIPLICATION,
    TYPE_DIVISION,
    TYPE_MODULUS,
    TYPE_EXPONENT,
    TYPE_DIFF,
    TYPE_NEGATE,

    // Procedure keywords
    PROCEDURE, // Procedure type
    RETURN,    // Used for return statements
    LOCAL, // Defines a variable in local scope, even if one exists in higher
           // scope

    // Control Characters
    END_OF_FILE
};

/// A token represents a piece of the source code. Special characters like * and
/// + are tokens, but "entire strings" and numbers are too.
class Token {
public:
    Token(TokenType _type, size_t _sourceOffset, size_t _length, std::string _literal)
        : sourceOffset(_sourceOffset), length(_length), type(_type),
          literal(_literal) {}

    /// Where in the source code is this token found
    /// If it is equal to -1, then there is no location
    /// information. This is probably because it was
    /// created in an intermediate state.
    size_t sourceOffset{};

    /// How many characters long is this token
    const size_t length;

    /// The type of token this is
    const TokenType type;

    /// Represents the piece of source code used to create this token
    const std::string literal;

    /// Returns the position the token ends in the source code
    size_t sourceEnd() const { return sourceOffset + length; }
};

#endif
