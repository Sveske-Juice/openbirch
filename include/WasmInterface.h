// Compiles if building for WebAssembly
#ifdef __EMSCRIPTEN__
#include "runtime/interpreter.h"

#include <emscripten/bind.h>

Interpreter interpreter;

// Interface to C++
extern "C" {
    char* eval(const char* input, bool ignoreAssignments = false) {
        std::string inputStr{input};

        interpreter.setIgnoreAssignments(ignoreAssignments);

        Lexer lexer{inputStr};
        Parser parser{lexer.tokenize()};
        auto statements = parser.parse();
        auto result = interpreter.interpret(std::move(statements));
        if (!result.has_value())
            return nullptr;

        std::string out = result.value()->toString();
        size_t len = out.length();
        char* output = static_cast<char*>(malloc(len + 1)); // Allocate memory for result
        if (result) {
            strcpy(output, out.c_str()); // Copy input to result
        }
        return output;
    }
}

extern "C" {
    void resetState() {
        interpreter.resetEnvironment();
    }
}

#endif
