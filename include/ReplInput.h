#ifndef REPLINPUT_H
#define REPLINPUT_H

#include <string>
#ifdef linux
#include <termios.h>
#endif
#include <stdio.h>
#include <vector>

enum KeyModifier {
  CONTROL,
  SHIFT,
  ALT,
  NOMODIFIER,
};

class ReplInput {
  private:
  std::vector<std::string> history;
  size_t history_index = 0;
  std::string currentTextBeforeHistory = "";

  public:
  ReplInput();
  ~ReplInput();
  std::string getInput();

  private:
  void beginRawMode();
  void endRawMode();

  void historyOlder();
  void historyNewer();

  bool match(wchar_t input, wchar_t c, KeyModifier mod = NOMODIFIER);

  wchar_t readKey();
  size_t cursorPos = 0;
  std::string currentText;

#ifdef linux
  termios old, new1;
#endif
};

#endif
