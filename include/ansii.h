#ifndef ANSII_H
#define ANSII_H

#define ANSII_RESET "\x1b[0m"

#define ANSII_BOLD "\x1b[1m"
#define ANSII_BOLD_RESET "\x1b[22m"

#define ANSII_DIM "\x1b[2m"
#define ANSII_DIM_RESET "\x1b[22m"

#define ANSII_ITALIC "\x1b[3m"
#define ANSII_ITALIC_RESET "\x1b[23m"

#define ANSII_UNDERLINE "\x1b[4m"
#define ANSII_UNDERLINE_RESET "\x1b[24m"

#define ANSII_BLINKING "\x1b[5m"
#define ANSII_BLINKING_RESET "\x1b[25m"

#define ANSII_STRIKETHROUGH "\x1b[9m"
#define ANSII_STRIKETHROUGH_RESET "\x1b[29m"

#define ANSII_FOREGROUND_BLACK "\x1b[30m"
#define ANSII_FOREGROUND_RED "\x1b[31m"
#define ANSII_FOREGROUND_GREEN "\x1b[32m"
#define ANSII_FOREGROUND_YELLOW "\x1b[33m"
#define ANSII_FOREGROUND_BLUE "\x1b[34m"
#define ANSII_FOREGROUND_MAGENTA "\x1b[35m"
#define ANSII_FOREGROUND_CYAN "\x1b[36m"
#define ANSII_FOREGROUND_WHITE "\x1b[37m"
#define ANSII_FOREGROUND_DEFAULT "\x1b[39m"

#define ANSII_BACKGROUND_BLACK "\x1b[40m"
#define ANSII_BACKGROUND_RED "\x1b[41m"
#define ANSII_BACKGROUND_GREEN "\x1b[42m"
#define ANSII_BACKGROUND_YELLOW "\x1b[43m"
#define ANSII_BACKGROUND_BLUE "\x1b[44m"
#define ANSII_BACKGROUND_MAGENTA "\x1b[45m"
#define ANSII_BACKGROUND_CYAN "\x1b[46m"
#define ANSII_BACKGROUND_WHITE "\x1b[47m"
#define ANSII_BACKGROUND_DEFAULT "\x1b[49m"

#endif
